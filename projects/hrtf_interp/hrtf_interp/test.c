#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>

#include "../../../scripts/interpolation_set/final/hrtf_db.h"

int main(void)
{
  double la, le, ra, re;
  getRaySphereIntersectionAngles(0, 0, 0.1, 1, 0.1449/2.0, &la, &le, &ra, &re);
  
  printf("%f %f %f %f\n", la, le, ra, re);
  
  //hrtf_db128_t* db128 = loadHRTFdb128_fromFiles(10,
  //  "../../../scripts/interpolation_set/final/scut_linear128_r0.2.dat",
  //  "../../../scripts/interpolation_set/final/scut_linear128_r0.25.dat",
  //  "../../../scripts/interpolation_set/final/scut_linear128_r0.3.dat",
  //  "../../../scripts/interpolation_set/final/scut_linear128_r0.4.dat",
  //  "../../../scripts/interpolation_set/final/scut_linear128_r0.5.dat",
  //  "../../../scripts/interpolation_set/final/scut_linear128_r0.6.dat",
  //  "../../../scripts/interpolation_set/final/scut_linear128_r0.7.dat",
  //  "../../../scripts/interpolation_set/final/scut_linear128_r0.8.dat",
  //  "../../../scripts/interpolation_set/final/scut_linear128_r0.9.dat",
  //  "../../../scripts/interpolation_set/final/scut_linear128_r1.dat"
  //);
  //
  //if (NULL != db128)
  //{
  //  float left[128];
  //  float right[128];

  //  getHRTF128_angle(db128, 0, 0, 0.225, left, right);
  //  destroyHRTFdb128(db128);
  //}

  return 0;
}
