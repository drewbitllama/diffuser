function [ out ] = ambi_decode( b, spkr_pos, xover_fc, fs )

% normalize coordinates
for i=1:size(spkr_pos,1)
    v = spkr_pos(i,:);
    v = v ./ sqrt(sum(v.^2));
    spkr_pos(i,:) = v;    
end

% generate FOA decoder matrix
K = ones(size(spkr_pos,1),4) ./ sqrt(2);
for i=1:size(spkr_pos,1)
    K(i,2:4) = [-spkr_pos(i,3) spkr_pos(i,1) spkr_pos(i,2)]; % front-back, left-right, up-down
end
M = pinv(K)';

% generate crossover filters
[ b_lp, a_lp, b_hp, a_hp ] = crossover_filter( xover_fc / fs );

% split input via crossover
w = b(:,1);
x = b(:,2);
y = b(:,3);
z = b(:,4);

w_lp = filter(b_lp, a_lp, w);
x_lp = filter(b_lp, a_lp, x);
y_lp = filter(b_lp, a_lp, y);
z_lp = filter(b_lp, a_lp, z);

w_hp = filter(b_hp, a_hp, w);
x_hp = filter(b_hp, a_hp, x);
y_hp = filter(b_hp, a_hp, y);
z_hp = filter(b_hp, a_hp, z);

w_weight = sqrt(2);
xyz_weight = sqrt(2/3);

w = w_lp - w_weight * w_hp;
x = x_lp - xyz_weight * x_hp;
y = y_lp - xyz_weight * y_hp;
z = z_lp - xyz_weight * z_hp;

out = (M * [w x y z]')';

end

function [ b_lp, a_lp, b_hp, a_hp ] = crossover_filter( fc )

k = tan(pi*fc);

b0_lp = k^2 / (k^2 + 2*k + 1);
b1_lp = 2*b0_lp;
b2_lp = b0_lp;

a0 = 1;
a1 = 2*(k^2 - 1) / (k^2 + 2*k + 1);
a2 = (k^2 - 2*k + 1) / (k^2 + 2*k + 1);

b0_hp = 1 / (k^2 + 2*k + 1);
b1_hp = -2*b0_hp;
b2_hp = b0_hp;

b_lp = [b0_lp b1_lp b2_lp];
a_lp = [a0 a1 a2];

b_hp = [b0_hp b1_hp b2_hp];
a_hp = [a0 a1 a2];

end
