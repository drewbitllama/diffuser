function [ b ] = ambi_encode( s, azi, ele, dist )

s = s(:);
theta = azi(:)/180*pi;
phi = ele(:)/180*pi;

% b-format encoding of input
w = s / sqrt(2);
x = s .* cos(theta) .* cos(phi);
y = s .* sin(theta) .* cos(phi);
z = s .* sin(phi);

b = [w x y z];

end