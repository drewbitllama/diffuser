clearvars;
clc;

fs = 44100;
len = round(fs*0.1)
ticks = 8;
in = (mod(0:len, round(len / ticks)) == 0) .* 1;

figure(1);
plot(in);

azi_start = 0;
azi_step = 360/(1*len); % degrees per second
azi = mod((0:(length(in)-1)) .* azi_step + azi_start, 360);

ele_start = 0;
ele_step = 0/len;
ele = mod((0:(length(in)-1)) .* ele_step + ele_start, 360);

b = ambi_encode(in, azi, ele, 1);

% spkr_pos = [1 1 1; -1 1 1; 1 1 -1; -1 1 -1; 1 -1 1; -1 -1 1; 1 -1 -1; -1 -1 -1];
spkrs = 16;
spkr_azi = linspace(0,-360,spkrs+1) - 45;
spkr_azi = spkr_azi(1:end-1);
spkr_pos = [];
for i=1:length(spkr_azi)
    spkr_pos = [spkr_pos; cos(spkr_azi(i)/180*pi) 0 sin(spkr_azi(i)/180*pi)];
end
% spkr_pos = [-1 0 1; 1 0 1; -1 0 -1; 1 0 -1; 0 0 1; 0 0 -1; 1 0 0; -1 0 0];
out = ambi_decode(b, spkr_pos, 700*fs/44100, fs);


audiowrite('quad_test.wav',out,fs);
