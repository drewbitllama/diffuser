function [ M ] = axis_rotate( M, a, b, angle )

if (size(M,1) ~= size(M,2))
    return;
end

R = eye(size(M,1));

theta = angle / 180 * pi;
R(a,a) = cos(theta);
R(a,b) = -sin(theta);
R(b,a) = sin(theta);
R(b,b) = cos(theta);

M = R*M;

end

