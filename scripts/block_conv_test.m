clearvars;
clc;

subject = 3;
load(sprintf('./CIPIC_hrtf_database/standard_hrir_database/subject_%03d/hrir_final.mat',subject));
x = audioread('chickenleg.wav');
x = x(22000:end);

blocksize = 256;
hopsize = blocksize / 2;
block_count = ceil(length(x)/blocksize);
out = zeros(block_count*blocksize,2);
out_smooth = zeros(block_count*blocksize,2);

x = [x(:); zeros(size(out,1) - length(x), 1)];
sinewave = cos(2*pi*440*(0:length(x)-1)/44100).';
x = x + sinewave;

ele = 16;
l_cipic = zeros(50,min(200,hopsize));
r_cipic = zeros(50,min(200,hopsize));
for i=1:25
    l_cipic(i,:) = squeeze(hrir_l(i,ele,1:min(200,hopsize)));
    r_cipic(i,:) = squeeze(hrir_r(i,ele,1:min(200,hopsize)));
    l_cipic(50-i+1,:) = squeeze(hrir_l(i,ele+32,1:min(200,hopsize)));
    r_cipic(50-i+1,:) = squeeze(hrir_r(i,ele+32,1:min(200,hopsize)));
end

azis = [-80 -65 -55 -45:5:45 55 65 80];
azis = [azis(:); azis(:) + 180];

ring_l_h = zeros(blocksize,360);
ring_r_h = zeros(blocksize,360);
idx = 1;
for i=1:50
    if i==50
        in = 1;
        step = 20;
    else
        in = i+1;
        step = azis(i+1) - azis(i);
    end
    for j=0:step-1
        alpha = j / step;
        l = (1 - alpha) * l_cipic(i,:) + alpha * l_cipic(in,:);
        r = (1 - alpha) * r_cipic(i,:) + alpha * r_cipic(in,:);
        l = [l(:); zeros(blocksize - length(l), 1)];
        r = [r(:); zeros(blocksize - length(r), 1)];
        ring_l_h(:,idx) = fft(l);
        ring_r_h(:,idx) = fft(r);
        idx = idx + 1;
    end
end

idx = 1;
for i=1:(block_count-1)*2+1
    
    idx_x_end = idx + hopsize - 1;
    idx_end = idx + blocksize - 1;
    
    block_in = x(idx:idx_x_end);
    block_in = [block_in(:); zeros(blocksize - length(block_in), 1)];
    block_h = fft(block_in);
    
    fft_idx = round(mod(round(idx*0.0015),360)+1);
    lh = block_h .* ring_l_h(:,fft_idx);
    rh = block_h .* ring_r_h(:,fft_idx);
    
    l_out = ifft(lh);
    r_out = ifft(rh);
    
    out(idx:idx_end,1) = out(idx:idx_end,1) + l_out;
    out(idx:idx_end,2) = out(idx:idx_end,2) + r_out;
    
    idx = idx + hopsize;
end

idx = 1;
for i=1:(block_count-1)*4+1
    
    idx_x_end = idx + hopsize - 1;
    idx_end = idx + blocksize - 1;
    
    block_in = x(idx:idx_x_end) .* hanning(hopsize);
    block_in = [block_in(:); zeros(blocksize - length(block_in), 1)];
    block_h = fft(block_in);
    
    fft_idx = round(mod(round(idx*0.0015),360)+1);
    lh = block_h .* ring_l_h(:,fft_idx);
    rh = block_h .* ring_r_h(:,fft_idx);
    
    l_out = ifft(lh);
    r_out = ifft(rh);
    
    out_smooth(idx:idx_end,1) = out_smooth(idx:idx_end,1) + l_out;
    out_smooth(idx:idx_end,2) = out_smooth(idx:idx_end,2) + r_out;
    
    idx = idx + hopsize / 2;
end

figure(1);
subplot(2,1,1);
plot(out(:,1));
subplot(2,1,2);
plot(out(:,2));

normalized = max(max(max(out)),max(max(out_smooth)));
out = out ./ normalized;
out_smooth = out_smooth ./ normalized;

fs = 44100;
% playblocking(audioplayer(out,fs));
playblocking(audioplayer(out_smooth,fs));

% audiowrite(sprintf('output_clicks_%d.wav',blocksize),out,fs);
% audiowrite(sprintf('output_smooth_%d.wav',blocksize),out_smooth,fs);
