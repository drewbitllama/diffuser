function [ p, q ] = brandhauen( input_response, desired_order, iterations )
%   Brandenstein-Unbehauen's Method for
%   Least-Squares Approximation of FIR by IIR Digital Filters
%
%   DESCRIPTION
%   An iterative procedure that requires solution of 
%   overdetermined set of liner equations and some digital 
%   filtering operations.
%
%   RETURNS
%   p       numerator coefficients of N+1 length
%   q       denominator coefficients of N+1 length

    input_response = input_response(:);
    norm_input_response = max(abs(input_response));
    input_response = input_response ./ norm_input_response;
    
    L = length(input_response);
    x1_h = input_response(L:-1:1);
    A = zeros(L, desired_order);
    q = [1 zeros(1 ,desired_order)];
    e2 = zeros(iterations, 1);
    q_out = zeros(iterations, desired_order + 1);
    p_out = zeros(iterations, desired_order + 1);
    
    for i=1:iterations
        
        x1 = filter(1, q, x1_h);
        b = -[zeros(desired_order, 1); x1(1:L-desired_order)];
        for j=1:desired_order
          A(:, j) = [zeros(j-1, 1); x1(1:L-j+1)];
        end
        
        A_inv = pinv(A);
        q_rev = [(A_inv * b)' 1];
        
        q = q_rev(desired_order + 1:-1:1);
        u = filter(q_rev, q, x1_h);
        r = u(L:-1:1);
        e2(i) = sqrt(sum(r.^2));
        q_out(i,:) = q(:);

        p = conv(input_response', q) - conv(q_rev, r');
        p = p(1:desired_order + 1);        
        p_out(i,:) = p(:);
    end
    
    [~, idx] = min(e2);
    p(:) = p_out(idx,:) .* norm_input_response;
    q(:) = q_out(idx,:);
end

