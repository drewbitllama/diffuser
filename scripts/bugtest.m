clearvars;
clc;

x = rand(100,1)*2-1;

X = fft(x);

storage_ok = zeros(100,1);
storage_bad = zeros(1,100);

storage_ok(:,1) = X;
storage_bad(1,:) = X;

err = sum(abs(storage_ok(:,1).' - storage_bad(1,:)));
fprintf('error is %f, but should be zero.\n', err);
