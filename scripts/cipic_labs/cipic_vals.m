clearvars;
clc;

load hrir_final.mat

fs = 44100;
c = 340;
radius = 1;
head_width = 0.1449;
dropped_samples = 100;

azi = [-80 -65 -55 -45:5:45 55 65 80];
ele = -45+5.625*(0:49);

le_dist = zeros(25,50);
re_dist = zeros(25,50);

le = [-head_width/2 0 0];
re = [head_width/2 0 0];

points = [];
for i=1:25
    for j=1:50
        v = cipic2cart(azi(i),ele(j)) * radius;
        [led,red] = get_samples_from_ears(v, head_width, c, fs, dropped_samples);
        le_dist(i,j) = led;
        re_dist(i,j) = red;
        points = [points; v];
    end
end

figure(1);
plot3(points(:,1),points(:,3),points(:,2),'.');
hold on;
plot3([le(1) re(1)],[le(3) re(3)],[le(2) re(2)],'.');
hold off;

for i=1:25
    for j=1:50
        figure(2);
        plot(squeeze(hrir_l(i,j,:)));
        hold on;
        plot(le_dist(i,j),hrir_l(i,j,round(le_dist(i,j))),'x');
        hold off;
        ylim([-1 1]);
        xlim([1 100]);
        pause(0.01);
    end
end


