function [ c ] = corrinterp( a, b, alpha )

xc = xcorr(a,b);
[~,idx] = max(xc);
shift = length(a) - idx;

figure(2);
plot(intshift(b,shift));
hold on;
plot(a);
hold off;


c = (1-alpha)*a+alpha*intshift(b,shift);
c = fracshift(c,-alpha*shift);

end
