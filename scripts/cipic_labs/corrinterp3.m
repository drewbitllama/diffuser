function [ lN ] = corrinterp3( l0, l1, l2, w0, w1 )

l0_hcc = conj(fft(l0,length(l0)*2));
l1_h = fft(l1,length(l1)*2);
l2_h = fft(l2,length(l2)*2);

xc01 = ifftshift(ifft(l1_h .* l0_hcc));
xc02 = ifftshift(ifft(l2_h .* l0_hcc));
xc01 = xc01(2:end);
xc02 = xc02(2:end);

[~,idx01] = max(xc01);
[~,idx02] = max(xc02);

shift1 = idx01 - length(l0);
shift2 = idx02 - length(l0);

t1 = intshift(l1,shift1);
t2 = intshift(l2,shift2);

lN = w0*l0 + w1*t1 + (1-w0-w1)*t2;
lN = fracshift(lN, -w1*shift1 - w0*shift2);

end

