function [ y ] = fracshift( x, frac_shift )

y = zeros(length(x),1);

for i=1:length(x)
    for j=1:length(x)
        y(i) = y(i) + sinc(i - j + frac_shift) .* x(j);
    end
end

end

