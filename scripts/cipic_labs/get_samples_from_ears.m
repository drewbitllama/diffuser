function [ left_del, right_del ] = get_samples_from_ears( v, head_width, c, fs, dropped_samples )

le = [-head_width/2 0 0];
re = [head_width/2 0 0];

left_del = sqrt(sum((v - le).^2)) / c * fs - dropped_samples;
right_del = sqrt(sum((v - re).^2)) / c * fs - dropped_samples;

end

