clearvars;
clc;

headwidth = 0.1449;
c = 340;
fs = 44100;
dropped_samples = 110;

load hrir_final.mat;

azi = [-80 -65 -55 -45:5:45 55 65 80];
ele = -45 + 5.625*(0:49);

azi0 = 1;
ele0 = 1;

azi1 = 2;
ele1 = 9;

azi2 = 1;
ele2 = 10;

l0 = squeeze(hrir_l(azi0,ele0,:));
l1 = squeeze(hrir_l(azi1,ele1,:));
l2 = squeeze(hrir_l(azi2,ele2,:));

[la,ra] = get_samples_from_ears(cipic2cart(azi(azi0),ele(ele0)),headwidth,c,fs,dropped_samples);
[lb,rb] = get_samples_from_ears(cipic2cart(azi(azi1),ele(ele1)),headwidth,c,fs,dropped_samples);
[lc,rc] = get_samples_from_ears(cipic2cart(azi(azi2),ele(ele2)),headwidth,c,fs,dropped_samples);

fa = fracshift(l0,la);
fb = fracshift(l1,lb);
fc = fracshift(l2,lc);

fN = corrinterp3(fa,fb,fc,1/3,1/3);

figure(1);
plot(fa);
hold on;
plot(fb);
plot(fc);
plot(fN);
hold off;
xlim([1 50]);

% fa_rs = resample(fa,8,1);
% fb_rs = resample(fb,8,1);
% fc_rs = resample(fc,8,1);
% 
% for alpha=0:0.01:1
%     c = corrinterp(fa,fb,alpha);
%     c_rs = resample(c,8,1);
%     
%     figure(1);
%     plot(fa_rs);
%     hold on;
%     plot(fb_rs);
%     plot(c_rs);
%     hold off;
%     xlim([1 400]);
%     pause(0.01);
% end