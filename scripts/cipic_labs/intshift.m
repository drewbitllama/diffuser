function [ y ] = intshift( x, int_shift )

x = x(:);
if (int_shift >= 0)    
    y = [x(1+int_shift:end); zeros(int_shift,1)];
else
    y = [zeros(abs(int_shift),1); x(1:end+int_shift)];
end

end

