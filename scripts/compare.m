clearvars;
clc;

fs = 44100;
t60 = 1;

len = round(fs * t60);

n = rand(len, 1) * 2 - 1;
n = n .* 10.^(linspace(0,-60,len)'/20);

fid = fopen('test.dat');
x = fread(fid,Inf,'double');
fclose(fid);

figure(1);
plot(1:length(n),20*log10(abs(n)),1:length(x),20*log10(abs(x)));
ylim([-60 0]);

% playblocking(audioplayer(n,fs));
% playblocking(audioplayer(x,fs));

[in,fs] = audioread('chickenleg.wav');
in = in(1:fs*2);

y1 = conv(in,n);
y2 = conv(in,x);

y1 = y1 ./ max(abs(y1));
y2 = y2 ./ max(abs(y2));

playblocking(audioplayer(y1, fs));
playblocking(audioplayer(y2, fs));

% audiowrite('ideal.wav',y1,fs);
% audiowrite('approx_low.wav',y2,fs);
% audiowrite('miller1.wav',y2,fs);

