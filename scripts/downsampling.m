clearvars;
clc;

x = rand(1024,1)*2-1;
x = x .* linspace(1,0,length(x))';

y = x(1:2:end);
y2 = resample(x,1,2,200);

h = fft(x);
g = fft(y);
z = resample(y,2,1,200);
k = fft(z);
l = fft(y2);

figure(1);
subplot(3,1,1);
plot(20*log10(abs(h(1:length(h)/2+1))));
subplot(3,1,2);
plot(20*log10(abs(g(1:length(g)/2+1))));
hold on;
plot(20*log10(abs(l(1:length(l)/2+1))));
hold off;
subplot(3,1,3);
plot(20*log10(abs(k(1:length(k)/2+1))));

