clearvars;
clc;

t60_dc = 0.2;
t60_ny = 0.9;

dels = primes(3000);
for i=1:length(dels)
    if dels(i) > 100
        dels = dels(i:end);
        break;
    end
end
fs = 44100;

alpha = t60_ny / t60_dc;

nfft = 512;
sum_h = zeros(nfft,1);

poles = zeros(length(dels),1);
t60_dc_nn = zeros(length(dels), 1);
t60_ny_nn = zeros(length(dels), 1);
t60_shape = zeros(nfft,1);
figure(1);
subplot(1,2,1);
for i=1:length(dels)
    Kp = -60 * dels(i) / fs / t60_dc;
    Gp = -60 * dels(i) / fs / t60_ny;
        
    kp = 10^(Kp / 20);
    gp = 10^(Gp / 20);
    
    bp = (kp - gp) / (kp + gp);
    alpha = t60_ny / t60_dc;
%     bp = Kp * log(10) / 80 * (1 - 1 / alpha^2);
    
    hpz_b = kp * (1 - bp);
    hpz_a = [1 -bp];
    poles(i) = roots(hpz_a);
    [hpz_h,hpz_w] = freqz(hpz_b,hpz_a,nfft);
    
    % verify I get back what I wanted
    t60_dc_nn(i) = -60 * dels(i) / fs / (20*log10(abs(hpz_h(1))));
    t60_ny_nn(i) = -60 * dels(i) / fs / (20*log10(abs(hpz_h(end))));
    t60_shape = t60_shape + (-60 .* dels(i) ./ fs ./ (20.*log10(abs(hpz_h))));
    
    sum_h = sum_h + hpz_h;
    plot(hpz_w,(abs(hpz_h)));
    if (i == 1)
        hold on;
    end
end
hold off;
title('Delay Filters');
xlim([0 pi]);

t60_shape = t60_shape ./ length(dels);

b = (1 - alpha) / (1 + alpha);
tz = [1 -b] / (1 - b);
[tz_h,tz_w] = freqz(tz,1,nfft);

subplot(1,2,2);
plot(tz_w,(abs(tz_h)));
title('Tone Corrector');
xlim([0 pi]);

% figure(2);
% plot(poles);

figure(3);
plot(t60_dc_nn);
title('T60(dc) realities');

figure(4);
plot(t60_ny_nn);
title('T60(ny) realities');

figure(5);
plot(t60_shape);
ylim([0 1]);
title('T60 shape');


