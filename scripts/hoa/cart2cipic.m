function [ azi, ele ] = cart2cipic( v )

azi = atan2(v(1),sqrt(v(2)^2+v(3)^2))*180/pi;
ele = atan2(v(2),v(3))*180/pi;

if (ele < -90)
   ele = ele + 360;
end

if (azi < -90 || azi > 90)
    
    if (azi < -90)
        azi = 180+azi;
    elseif (azi > 90)
        azi = 90-azi;
    end
    
    ele = ele + 180;
    if (ele > 270)
        ele = ele - 360;
    end
end

end

