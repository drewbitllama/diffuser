clearvars;
clc;

azi = [-80 -65 -55 -45:5:45 55 65 80];
ele = -45 + 5.625*(0:49);

pts = [];
pts_guess = [];
azi_guess = [];
ele_guess = [];
for i=1:length(azi)
    for j=1:length(ele)
        pts = [pts; cipic2cart(azi(i),ele(j))];
        [a,e] = cart2cipic(pts(end,:));
        azi_guess = [azi_guess; a];
        ele_guess = [ele_guess; e];
        pts_guess = [pts_guess; cipic2cart(a,e)];
        if (sqrt(sum((pts-pts_guess).^2)) > 1e-14)
            disp('!');
        end
    end
end

figure(1);
scatter3(pts(:,1),pts(:,3),pts(:,2),'filled');
hold on;
scatter3(pts_guess(:,1),pts_guess(:,3),pts_guess(:,2));
hold off;
xlim([-1 1]);
ylim([-1 1]);
zlim([-1 1]);
