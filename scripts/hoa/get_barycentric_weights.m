function [ w ] = get_barycentric_weights( v, v1, v2, v3 )

T = [v1 - v3; v2 - v3]';
w = pinv(T) * (v - v3)';
w(3) = 1 - sum(w);

end

