function [ coords ] = get_cipic_cartcoords( )

azi = [-80 -65 -55 -45:5:45 55 65 80];
ele = -45 + 5.625*(0:49);
coords = [];

% convert CIPIC angles to cartesian coordinates
for i=1:length(azi)
    for j=1:length(ele)
        coords = [coords; cipic2cart(azi(i),ele(j))];
    end
end

end
