clearvars;
clc;

cipic_azi = [-80 -65 -55 -45:5:45 55 65 80];
cipic_ele = -45 + 5.625*(0:49);

g = (1 + sqrt(5)) / 2;
rg = 1/g;
sphere = [...
    1,  1,  1; ...
   -1,  1,  1; ...
    1, -1,  1; ...
   -1, -1,  1; ...
    1,  1, -1; ...
   -1,  1, -1; ...
    1, -1, -1; ...
   -1, -1, -1; ...
    0, rg,  g; ...
    0,-rg,  g; ...
    0, rg, -g; ...
    0,-rg, -g; ...
   rg,  g,  0; ...
  -rg,  g,  0; ...
   rg, -g,  0; ...
  -rg, -g,  0; ...
    g,  0, rg; ...
   -g,  0, rg; ...
    g,  0,-rg; ...
   -g,  0,-rg; ...
   ];

% normalize dodecahedron & determine angles
angles = zeros(20,2);
for i=1:20
    v = sphere(i,:);
    v = v ./ sqrt(sum(v.^2));
    sphere(i,:) = v;
    
    theta = atan2(v(2),v(1));
    phi = asin(v(3));
    angles(i,:) = [theta phi];
end

% SN3D definitions
% W = 1
% X = cos(T)cos(R)
% Y = sin(T)cos(R)
% Z = sin(R)
% R = (3sin^2(R) - 1)/2
% S = sqrt(3)/2 * cos(T)sin(2R)
% T = sqrt(3)/2 * sin(T)sin(2R)
% U = sqrt(3)/2 * cos(2T)cos^2(R)
% V = sqrt(3)/2 * sin(2T)cos^2(R)
% K = sin(R) * (5sin^2(R)-3)/2
% L = sqrt(3/8) * cos(T)cos(R) * (5sin^2(R)-1)
% M = sqrt(3/8) * sin(T)cos(R) * (5sin^2(R)-1)
% N = sqrt(15)/2 * cos(2T)sin(R)cos^2(R)
% O = sqrt(15)/2 * sin(2T)sin(R)cos^2(R)
% P = sqrt(5/8) * cos(3T)cos^3(R)
% Q = sqrt(5/8) * sin(3T)cos^3(R)

% FUMA weights
% W = 1/sqrt(2)
% X = 1
% Y = 1
% Z = 1
% R = 1
% S = 2/sqrt(3)
% T = 2/sqrt(3)
% U = 2/sqrt(3)
% V = 2/sqrt(3)
% K = 1
% L = sqrt(45/32)
% M = sqrt(45/32)
% N = 3/sqrt(5)
% O = 3/sqrt(5)
% P = sqrt(8/5)
% Q = sqrt(8/5)

fuma = [1/sqrt(2) ...
    1 1 1 ...
    1 2/sqrt(3) 2/sqrt(3) 2/sqrt(3) 2/sqrt(3) ...
    1 sqrt(45/32) sqrt(45/32) 3/sqrt(5) 3/sqrt(5) sqrt(8/5) sqrt(8/5) ...
    ];

M = zeros(20,length(fuma));
row = zeros(1,length(fuma));
for i=1:20
    
    t = angles(i,1);
    r = angles(i,2);
    
    row(1) = 1;
    
    row(2) = cos(t)*cos(r);
    row(3) = sin(t)*cos(r);
    row(4) = sin(r);
    
    row(5) = (3*sin(r)^2-1)/2;
    row(6) = sqrt(3)/2*cos(t)*sin(2*r);
    row(7) = sqrt(3)/2*sin(t)*sin(2*r);
    row(8) = sqrt(3)/2*cos(2*t)*cos(r)^2;
    row(9) = sqrt(3)/2*sin(2*t)*cos(r)^2;
    
    row(10) = sin(r)*(5*sin(r)^2-3)/2;
    row(11) = sqrt(3/8)*cos(t)*cos(r)*(5*sin(r)^2-1);
    row(12) = sqrt(3/8)*sin(t)*cos(r)*(5*sin(r)^2-1);
    row(13) = sqrt(15)/2*cos(2*t)*sin(r)*cos(r)^2;
    row(14) = sqrt(15)/2*sin(2*t)*sin(r)*cos(r)^2;
    row(15) = sqrt(5/8)*cos(3*t)*cos(r)^3;
    row(16) = sqrt(5/8)*sin(3*t)*cos(r)^3;
    
    M(i,:) = fuma .* row;
    
end

D = pinv(M)';

cipic_coords = get_cipic_cartcoords();

% convert dodecahedron to cipic coords, find quad
sphere_azi = [];
sphere_ele = [];
for i=1:size(sphere,1)
   [a,e] = cart2cipic(sphere(i,:));
   sphere_azi = [sphere_azi; a];
   sphere_ele = [sphere_ele; e];
end

sphere_quads = zeros(length(sphere_azi),4);
for i=1:length(sphere_azi)
    
    azi_left = -1;
    azi_right = -1;
    ele_down = -1;
    ele_up = -1;
    
    for j=1:length(cipic_azi)
        if (sphere_azi(i) < cipic_azi(j))
            azi_right = cipic_azi(j);
            if (j > 1)
                azi_left = cipic_azi(j-1);
            end
            break;
        end
    end
    for k=1:length(cipic_ele)
        if (sphere_ele(i) < cipic_ele(k))
            ele_up = cipic_ele(k);
            if (k > 1)
                ele_down = cipic_ele(k-1);
            end
            break;
        end
    end
    
    % if any indices are -1, need to come up with another strategy....
    
    sphere_quads(i,1) = azi_left;
    sphere_quads(i,2) = ele_down;    
    sphere_quads(i,3) = azi_right;
    sphere_quads(i,4) = ele_up;
    
end

figure(1);
scatter3(cipic_coords(:,1),cipic_coords(:,3),cipic_coords(:,2),'.');
hold on;
scatter3(sphere(:,1),sphere(:,3),sphere(:,2),'filled');
hold off;


