function [ y ] = arc_interp( x0, x1, x2, w0, w1, w2 )

h0 = fft(x0);
h1 = fft(x1);
h2 = fft(x2);

w = 1:length(h0)/2+1;
lm0 = 20*log10(h0(w));
lm1 = 20*log10(h1(w));
lm2 = 20*log10(h2(w));

p0 = angle(h0(w));
p1 = angle(h1(w));
p2 = angle(h2(w));

for k=2:length(w)-1
    
    d01 = abs(p0(k) - p1(k));
    d02 = abs(p0(k) - p2(k));
    d12 = abs(p1(k) - p2(k));
    d_max = max([d01 d02 d12]);
    
    d01a0 = abs((p0(k) + 2*pi) - p1(k));
    d02a0 = abs((p0(k) + 2*pi) - p2(k));
    d12a0 = abs(p1(k) - p2(k));
    da0_max = max([d01a0 d02a0 d12a0]);
    
    d01s0 = abs((p0(k) - 2*pi) - p1(k));
    d02s0 = abs((p0(k) - 2*pi) - p2(k));
    d12s0 = abs(p1(k) - p2(k));
    ds0_max = max([d01s0 d02s0 d12s0]);

    d01a1 = abs(p0(k) - (p1(k) + 2*pi));
    d02a1 = abs(p0(k) - p2(k));
    d12a1 = abs((p1(k) + 2*pi) - p2(k));
    da1_max = max([d01a1 d02a1 d12a1]);

    d01s1 = abs(p0(k) - (p1(k) - 2*pi));
    d02s1 = abs(p0(k) - p2(k));
    d12s1 = abs((p1(k) - 2*pi) - p2(k));
    ds1_max = max([d01s1 d02s1 d12s1]);

    d01a2 = abs(p0(k) - p1(k));
    d02a2 = abs(p0(k) - (p2(k) + 2*pi));
    d12a2 = abs(p1(k) - (p2(k) + 2*pi));
    da2_max = max([d01a2 d02a2 d12a2]);

    d01s2 = abs(p0(k) - p1(k));
    d02s2 = abs(p0(k) - (p2(k) - 2*pi));
    d12s2 = abs(p1(k) - (p2(k) - 2*pi));
    ds2_max = max([d01s2 d02s2 d12s2]);

    d_min = min([d_max da0_max ds0_max da1_max ds1_max da2_max ds2_max]);
    
    if ( d_max == d_min ) % no oddball
        % do nothing
    elseif ( da0_max == d_min ) % add 2pi to p0
        p0(k) = p0(k) + pi;
    elseif ( ds0_max == d_min ) % sub 2pi to p0
        p0(k) = p0(k) - pi;
    elseif ( da1_max == d_min ) % add 2pi to p1
        p1(k) = p1(k) + pi;
    elseif ( ds1_max == d_min ) % sub 2pi to p1
        p1(k) = p1(k) - pi;
    elseif ( da2_max == d_min ) % add 2pi to p2
        p2(k) = p2(k) + pi;
    elseif ( ds2_max == d_min ) % sub 2pi to p2
        p2(k) = p2(k) - pi;
    else
        [p0(k) p1(k) p2(k)]
        errorlist = [d01 d02 d12; d01a0 d02a0 d12a0; d01s0 d02s0 d12s0; d01a1 d02a1 d12a1; d01s1 d02s1 d12s1; d01a2 d02a2 d12a2; d01s2 d02s2 d12s2]
        error('bad!');
    end
end

lmN = w0 * lm0 + w1 * lm1 + w2 * lm2;
pN = w0 * p0 + w1 * p1 + w2 * p2;
fN = 10.^(lmN./20).*(cos(pN)+1i.*sin(pN));
hN = [fN(:); conj(fN(end-1:-1:2))];
y = ifft(hN,'symmetric');

end