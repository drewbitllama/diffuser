function [ ] = display_triangle_mesh( tri_coord )

for i=1:size(tri_coord,1)
    tri = squeeze(tri_coord(i,1:3,:));
    fill3(tri(:,1),tri(:,3),tri(:,2),[1 1 1]);
    if (i == 1)
        hold on;
    end
end
hold off;
xlim([-1 1]);
ylim([-1 1]);
zlim([-1 1]);
axis equal;

end

