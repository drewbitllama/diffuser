function [ y ] = fracshift( x, frac_shift )

x = x(:);
y = zeros(length(x),1);
list = (1:length(x))' + frac_shift;
for i=1:length(x)
    y(i) = sum(sinc(i - list) .* x);
end

end
