function [ hd_n90, hd_n75, hd_n60, hd_n45, hd_0, hd_45, hd_60, hd_75, hd_90 ] = ...
    get_hrtf_data_from_responses( left, right, points_angle_indices )

len = size(left,2);

hd_n90 = zeros(len,2);
hd_n75 = zeros(len,15,45,2);
hd_n60 = zeros(len,15,90,2);
hd_n45 = zeros(len,15,180,2);
hd_0 = zeros(len,89,360,2);
hd_45 = zeros(len,15,180,2);
hd_60 = zeros(len,15,90,2);
hd_75 = zeros(len,15,45,2);
hd_90 = zeros(len,2);

for i=1:size(points_angle_indices,1)
    azi = points_angle_indices(i,1);
    ele = points_angle_indices(i,2);

    l = squeeze(left(i,:));
    r = squeeze(right(i,:));

    if ele == 1
        hd_n90(:,1) = l;
        hd_n90(:,2) = r;
    elseif ele >= 2 && ele <= 16
        hd_n75(:,ele-2+1,azi,1) = l;
        hd_n75(:,ele-2+1,azi,2) = r;
    elseif ele >= 17 && ele <= 31
        hd_n60(:,ele-17+1,azi,1) = l;
        hd_n60(:,ele-17+1,azi,2) = r;
    elseif ele >= 32 && ele <= 46
        hd_n45(:,ele-32+1,azi,1) = l;
        hd_n45(:,ele-32+1,azi,2) = r;
    elseif ele >= 47 && ele <= 135
        hd_0(:,ele-47+1,azi,1) = l;
        hd_0(:,ele-47+1,azi,2) = r;
    elseif ele >= 136 && ele <= 150
        hd_45(:,ele-136+1,azi,1) = l;
        hd_45(:,ele-136+1,azi,2) = r;
    elseif ele >= 151 && ele <= 165
        hd_60(:,ele-151+1,azi,1) = l;
        hd_60(:,ele-151+1,azi,2) = r;
    elseif ele >= 166 && ele <= 180
        hd_75(:,ele-166+1,azi,1) = l;
        hd_75(:,ele-166+1,azi,2) = r;
    elseif ele == 181
        hd_90(:,1) = l;
        hd_90(:,2) = r;
    else
        error('ele not found: %d', ele);
    end
end

end

