function [ fir_left, fir_right ] = get_interp_fir_from_weights( weights, triangle_indices, tri_left, tri_right, interp_mode )

fir_left = zeros(size(triangle_indices,1), size(tri_left,3));
fir_right = zeros(size(triangle_indices,1), size(tri_left,3));

parfor i=1:size(triangle_indices,1)
    
    % grab IRs
    l0 = squeeze(tri_left(triangle_indices(i),1,:));
    l1 = squeeze(tri_left(triangle_indices(i),2,:));
    l2 = squeeze(tri_left(triangle_indices(i),3,:));
    r0 = squeeze(tri_right(triangle_indices(i),1,:));
    r1 = squeeze(tri_right(triangle_indices(i),2,:));
    r2 = squeeze(tri_right(triangle_indices(i),3,:));
    
    % compute interpolations
    if strcmp(interp_mode,'linear')
        l = weights(i,1) * l0 + weights(i,2) * l1 + weights(i,3) * l2;
        r = weights(i,1) * r0 + weights(i,2) * r1 + weights(i,3) * r2;        
    elseif strcmp(interp_mode,'minphase')
        % compute minphase representations
        l0_minphase = minphase(l0);
        l1_minphase = minphase(l1);
        l2_minphase = minphase(l2);

        r0_minphase = minphase(r0);
        r1_minphase = minphase(r1);
        r2_minphase = minphase(r2);
        
        l = weights(i,1) * l0_minphase + weights(i,2) * l1_minphase + weights(i,3) * l2_minphase;
        r = weights(i,1) * r0_minphase + weights(i,2) * r1_minphase + weights(i,3) * r2_minphase;
    elseif strcmp(interp_mode,'arc')
        l = arc_interp(l0, l1, l2, weights(i,1), weights(i,2), weights(i,3));
        r = arc_interp(r0, r1, r2, weights(i,1), weights(i,2), weights(i,3));        
    else
        l = l0*0;
        r = l0*0;
    end
    
    % put into array structs
    fir_left(i,:) = l;
    fir_right(i,:) = r;
end

end

