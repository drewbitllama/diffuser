function [ fft_left, fft_right ] = get_mayerfft_from_firs( fir_left, ...
    fir_right, fft_size, prefade, postfade )

fft_left = zeros(size(fir_left,1),fft_size);
fft_right = zeros(size(fir_left,1),fft_size);
for i=1:size(fir_left,1)

    half_fft_size = fft_size/2;
    l = fir_left(i,:);
    r = fir_right(i,:);
    
    l = l(1:min(half_fft_size, length(l)));
    r = r(1:min(half_fft_size, length(r)));
    l = l(:);
    r = r(:);
    
    l(1:prefade) = linspace(0,1,prefade)' .* l(1:prefade);
    l(end-postfade+1:end) = linspace(1,0,postfade)' .* l(end-postfade+1:end);
    
    r(1:prefade) = linspace(0,1,prefade)' .* r(1:prefade);
    r(end-postfade+1:end) = linspace(1,0,postfade)' .* r(end-postfade+1:end);
    
    lh = fft(l,fft_size);
    rh = fft(r,fft_size);
    
    idx = 1;
    for j=1:fft_size/2+1
        fft_left(i,idx) = real(lh(j));
        fft_right(i,idx) = real(rh(j));
        idx = idx + 1;
    end
    for j=fft_size/2+2:fft_size
        fft_left(i,idx) = imag(lh(j));
        fft_right(i,idx) = imag(rh(j));
        idx = idx + 1;
    end
    
end

end

