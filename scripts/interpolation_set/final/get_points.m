function [ points, points_angle_indices ] = get_points( )

point_ele = -90:1:90;
point_ele = point_ele(:);
point_degs = zeros(length(point_ele),1);
point_degs(1) = 1;          % -90
point_degs(2:16) = 45;      % -89 to -75
point_degs(17:31) = 90;     % -74 to -60
point_degs(32:46) = 180;    % -59 to -45
point_degs(47:135) = 360;   % -44 to 44
point_degs(136:150) = 180;  % 45 to 59
point_degs(151:165) = 90;   % 60 to 74
point_degs(166:180) = 45;   % 75 to 89
point_degs(end) = 1;        % 90

points = zeros(sum(point_degs),3);
points_angle_indices = zeros(sum(point_degs),2);
idx = 1;
for i=1:size(point_degs,1)    
    theta = linspace(0,2*pi,point_degs(i)+1);
    theta = theta(1:end-1);
    for j=1:length(theta)
        v = [sin(theta(j))*cos(point_ele(i)/180*pi) sin(point_ele(i)/180*pi) cos(theta(j))*cos(point_ele(i)/180*pi)];
        points(idx,:) = v;
        points_angle_indices(idx,:) = [j i];
        idx = idx + 1;
    end    
end

end

