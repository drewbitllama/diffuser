function [ weights, triangle_indices ] = get_triangle_weights_for_points( points, tri_coord )

%% ========================================================================
% split triangles in 8 quadrants
tri_coord_ppp = [];
tri_coord_npp = [];
tri_coord_pnp = [];
tri_coord_nnp = [];
tri_coord_ppn = [];
tri_coord_npn = [];
tri_coord_pnn = [];
tri_coord_nnn = [];

parfor i=1:size(tri_coord,1)    
    
    maxx = max(tri_coord(i,:,1));
    maxy = max(tri_coord(i,:,2));
    maxz = max(tri_coord(i,:,3));
    
    minx = min(tri_coord(i,:,1));
    miny = min(tri_coord(i,:,2));
    minz = min(tri_coord(i,:,3));
    
    if maxx >= 0 && maxy >= 0 && maxz >= 0
        tri_coord_ppp = [tri_coord_ppp; i];
    end
    if minx <= 0 && maxy >= 0 && maxz >= 0
        tri_coord_npp = [tri_coord_npp; i];
    end
    if maxx >= 0 && miny <= 0 && maxz >= 0
        tri_coord_pnp = [tri_coord_pnp; i];
    end
    if minx <= 0 && miny <= 0 && maxz >= 0
        tri_coord_nnp = [tri_coord_nnp; i];
    end
    if maxx >= 0 && maxy >= 0 && minz <= 0
        tri_coord_ppn = [tri_coord_ppn; i];
    end
    if minx <= 0 && maxy >= 0 && minz <= 0
        tri_coord_npn = [tri_coord_npn; i];
    end
    if maxx >= 0 && miny <= 0 && minz <= 0
        tri_coord_pnn = [tri_coord_pnn; i];
    end
    if minx <= 0 && miny <= 0 && minz <= 0
        tri_coord_nnn = [tri_coord_nnn; i];
    end
end

%% ========================================================================
% for each ray, find barycentric coords and generate response
triangle_indices = zeros(size(points,1),1);
weights = zeros(size(points,1),3);

parfor i=1:size(points,1)
    v = points(i,:)';
    if v(1) >= 0
        if v(2) >= 0
            if v(3) >= 0
                [ match, pt_weights, pt_index ] = attempt_match(tri_coord, tri_coord_ppp, v);
            else
                [ match, pt_weights, pt_index ] = attempt_match(tri_coord, tri_coord_ppn, v);
            end
        else
            if v(3) >= 0
                [ match, pt_weights, pt_index ] = attempt_match(tri_coord, tri_coord_pnp, v);
            else
                [ match, pt_weights, pt_index ] = attempt_match(tri_coord, tri_coord_pnn, v);
            end
        end
    else
        if v(2) >= 0
            if v(3) >= 0
                [ match, pt_weights, pt_index ] = attempt_match(tri_coord, tri_coord_npp, v);
            else
                [ match, pt_weights, pt_index ] = attempt_match(tri_coord, tri_coord_npn, v);
            end
        else
            if v(3) >= 0
                [ match, pt_weights, pt_index ] = attempt_match(tri_coord, tri_coord_nnp, v);
            else
                [ match, pt_weights, pt_index ] = attempt_match(tri_coord, tri_coord_nnn, v);
            end
        end
    end   
    
    if match == 0
        error('no match found?');
    else
        weights(i,:) = pt_weights;
        triangle_indices(i) = pt_index;        
    end
end

end

function [ match, pt_weights, pt_index ] = attempt_match(tri_coord, list, v)

match = 0;
pt_weights = [0 0 0];
pt_index = 0;
o = [0 0 0].';

for j=1:length(list)
    v1 = squeeze(tri_coord(list(j),1,:));
    v2 = squeeze(tri_coord(list(j),2,:));
    v3 = squeeze(tri_coord(list(j),3,:));
    [bool, w] = ray_triangle_intersect(o,v,v1,v2,v3);
    if bool == 1
        pt_weights = w;
        pt_index = list(j);
        match = 1;
        break;
    end
end

end
