function [ tri_coord, tri_left, tri_right, hrtf_radius, head_width, left_ear, right_ear ] = ...
    get_triangles( hrtf_database, subject, desired_hrtf_radius )

if strcmp(hrtf_database,'cipic')
    [ tri_coord, tri_left, tri_right, hrtf_radius, head_width, left_ear, right_ear ] = make_cipic_triangles(subject);
elseif strcmp(hrtf_database,'mit')
    [ tri_coord, tri_left, tri_right, hrtf_radius, head_width, left_ear, right_ear ] = make_mit_triangles();
elseif strcmp(hrtf_database,'listen')
    [ tri_coord, tri_left, tri_right, hrtf_radius, head_width, left_ear, right_ear ] = make_listen_triangles(subject);
elseif strcmp(hrtf_database,'scut')
    [ tri_coord, tri_left, tri_right, hrtf_radius, head_width, left_ear, right_ear ] = make_scut_triangles(desired_hrtf_radius);    
else
    error('Bad hrtf_database name (%s)!', hrtf_database);
end

end

