#include <math.h>
#include <string.h>
#include <stdarg.h>
#include <float.h>
#include <stdio.h>
#include <stdlib.h>

#include "hrtf_db.h"

//=============================================================================
void getRaySphereIntersectionAngles(double source_azimuth,
  double source_elevation, double source_radius, double hrtf_radius,
  double head_radius, double* left_azimuth, double* left_elevation,
  double* right_azimuth, double* right_elevation);

int getAziFloorForDesiredElev(int azi, int ele);
int getAziCeilForDesiredElev(int azi_floor, int ele);

hrtf_sphere128_t* loadHRTFsphere128_fromFile(const char* filename);
hrtf_sphere128_t* loadHRTFsphere128_fromMemory(char* data);
void getHRTFsphere128Ptr(hrtf_sphere128_t* ptr,
  int azimuth, int elevation, float** left, float** right);
void getHRTFsphere128_angle(hrtf_sphere128_t* ptr,
  double azimuth, double elevation, float* left, float* right);
void sortHRTFdbsByDistance128(hrtf_db128_t* ptr);

hrtf_sphere256_t* loadHRTFsphere256_fromFile(const char* filename);
hrtf_sphere256_t* loadHRTFsphere256_fromMemory(char* data);
void getHRTFsphere256Ptr(hrtf_sphere256_t* ptr,
  int azimuth, int elevation, float** left, float** right);
void getHRTFsphere256_angle(hrtf_sphere256_t* ptr, 
  double azimuth, double elevation, float* left, float* right);
void sortHRTFdbsByDistance256(hrtf_db256_t* ptr);

//=============================================================================
void getRaySphereIntersectionAngles(double source_azimuth,
  double source_elevation, double source_radius, double hrtf_radius,
  double head_radius, double* left_azimuth, double* left_elevation,
  double* right_azimuth, double* right_elevation)
{
  double recip_source_dist, cos_src_ele, recip_left_dist, recip_right_dist;
  double source_pos[3];
  double source_dir[3];
  double left_dir[3];
  double right_dir[3];
  double tca_l, tca_r, src_dot, d_l, d_r, thc_l, thc_r;
  double left_pos[3];
  double right_pos[3];

  // degrees to radians
  source_azimuth *= 0.01745329251994329577;
  source_elevation *= 0.01745329251994329577;

  // convert angles to cartesian
  cos_src_ele = cos(source_elevation) * source_radius;
  source_pos[0] = sin(source_azimuth) * cos_src_ele;
  source_pos[1] = sin(source_elevation) * source_radius;
  source_pos[2] = cos(source_azimuth) * cos_src_ele;

  // normalize source direction
  recip_source_dist = 1.0 / sqrt(source_pos[0] * source_pos[0] +
    source_pos[1] * source_pos[1] + source_pos[2] * source_pos[2]);
  source_dir[0] = source_pos[0] * recip_source_dist;
  source_dir[1] = source_pos[1] * recip_source_dist;
  source_dir[2] = source_pos[2] * recip_source_dist;

  // compute left/right ear directions to source
  left_dir[0] = -head_radius - source_pos[0];
  left_dir[1] = -source_pos[1];
  left_dir[2] = -source_pos[2];
  recip_left_dist = 1.0 / sqrt(left_dir[0] * left_dir[0] + 
    left_dir[1] * left_dir[1] + left_dir[2] * left_dir[2]);
  left_dir[0] *= recip_left_dist;
  left_dir[1] *= recip_left_dist;
  left_dir[2] *= recip_left_dist;

  right_dir[0] = head_radius - source_pos[0];
  right_dir[1] = -source_pos[1];
  right_dir[2] = -source_pos[2];
  recip_right_dist = 1.0 / sqrt(right_dir[0] * right_dir[0] +
    right_dir[1] * right_dir[1] + right_dir[2] * right_dir[2]);
  right_dir[0] *= recip_right_dist;
  right_dir[1] *= recip_right_dist;
  right_dir[2] *= recip_right_dist;

  // project source along left/right ear directions
  tca_l = -source_pos[0] * left_dir[0] + 
    -source_pos[1] * left_dir[1] + -source_pos[2] * left_dir[2];
  tca_r = -source_pos[0] * right_dir[0] +
    -source_pos[1] * right_dir[1] + -source_pos[2] * right_dir[2];

  // negative source position dot product
  src_dot = source_pos[0] * source_pos[0] +
    source_pos[1] * source_pos[1] + source_pos[2] * source_pos[2];
  
  // compute sqr.dist to "sphere origin" from projection
  d_l = src_dot - tca_l * tca_l;
  d_r = src_dot - tca_r * tca_r;

  // compute length of ray to intersection width
  hrtf_radius *= hrtf_radius;
  thc_l = tca_l - sqrt(hrtf_radius - d_l);
  thc_r = tca_r - sqrt(hrtf_radius - d_r);

  // move ray along directions to initial sphere intersection point
  left_pos[0] = source_pos[0] + thc_l * left_dir[0];
  left_pos[1] = source_pos[1] + thc_l * left_dir[1];
  left_pos[2] = source_pos[2] + thc_l * left_dir[2];

  right_pos[0] = source_pos[0] + thc_l * right_dir[0];
  right_pos[1] = source_pos[1] + thc_l * right_dir[1];
  right_pos[2] = source_pos[2] + thc_l * right_dir[2];

  // compute angles of cartesian points
  *left_azimuth = atan2(left_pos[0], left_pos[2]) * 57.2957795130823208768;
  *left_elevation = atan2(left_pos[1], sqrt(left_pos[0] * left_pos[0] + 
    left_pos[2] * left_pos[2])) * 57.2957795130823208768;

  *right_azimuth = atan2(right_pos[0], right_pos[2]) * 57.2957795130823208768;
  *right_elevation = atan2(right_pos[1], sqrt(right_pos[0] * right_pos[0] +
    right_pos[2] * right_pos[2])) * 57.2957795130823208768;
}

//=============================================================================
int getAziFloorForDesiredElev(int azi, int ele)
{
  int azi_out = azi;
  if (ele == -90)
  {
    azi_out = 0;
  }
  else if (ele <= -75)
  {
    azi_out = (azi / 8) * 8;
  }
  else if (ele <= -60)
  {
    azi_out = (azi / 4) * 4;
  }
  else if (ele <= -45)
  {
    azi_out = (azi / 2) * 2;
  }
  else if (ele <= 44)
  {
    azi_out = azi;
  }
  else if (ele <= 59)
  {
    azi_out = (azi / 2) * 2;
  }
  else if (ele <= 74)
  {
    azi_out = (azi / 4) * 4;
  }
  else if (ele <= 89)
  {
    azi_out = (azi / 8) * 8;
  }
  else
  {
    azi_out = 0;
  }
  return azi_out;
}

//=============================================================================
int getAziCeilForDesiredElev(int azi_floor, int ele)
{
  int azi_out = azi_floor;

  if (ele == -90)
  {
    azi_out = 0;
  }
  else if (ele <= -75)
  {
    azi_out = (azi_floor + 8);
  }
  else if (ele <= -60)
  {
    azi_out = (azi_floor + 4);
  }
  else if (ele <= -45)
  {
    azi_out = (azi_floor + 2);
  }
  else if (ele <= 44)
  {
    azi_out = (azi_floor + 1);
  }
  else if (ele <= 59)
  {
    azi_out = (azi_floor + 2);
  }
  else if (ele <= 74)
  {
    azi_out = (azi_floor + 4);
  }
  else if (ele <= 89)
  {
    azi_out = (azi_floor + 8);
  }
  else
  {
    azi_out = 0;
  }

  return azi_out;
}

//=============================================================================
hrtf_sphere128_t* loadHRTFsphere128_fromFile(const char* filename)
{
  hrtf_sphere128_t* ptr = NULL;
  FILE* fp = NULL;

  fp = fopen(filename, "rb");
  if (NULL == fp)
    return NULL;

  ptr = (hrtf_sphere128_t*)malloc(sizeof(hrtf_sphere128_t));
  if (NULL != ptr)
  {
    // zero-set just in case
    memset(ptr, 0, sizeof(hrtf_sphere128_t));

    fseek(fp, 0, SEEK_END);
    size_t size = ftell(fp);
    if (size == sizeof(hrtf_sphere128_t))
    {
      fseek(fp, 0, SEEK_SET);
      fread((void*)ptr, sizeof(hrtf_sphere128_t), 1, fp);
    }
  }
  fclose(fp);

  return ptr;
}

//=============================================================================
hrtf_sphere128_t* loadHRTFsphere128_fromMemory(char* data)
{
  hrtf_sphere128_t* ptr = NULL;

  ptr = (hrtf_sphere128_t*)malloc(sizeof(hrtf_sphere128_t));
  if (NULL == ptr)
    return NULL;

  memcpy(ptr, data, sizeof(hrtf_sphere128_t));
  return ptr;
}

//=============================================================================
void getHRTFsphere128Ptr(hrtf_sphere128_t* ptr,
  int azimuth, int elevation, float** left, float** right)
{
  int ele_idx = 0, azi_idx = 0;

  if (elevation == -90)
  {
    *left = (float*)&ptr->hdb_fft.hd_n90[0][0];
    *right = (float*)&ptr->hdb_fft.hd_n90[1][0];
  }
  else if (elevation <= -75)
  {
    azi_idx = (azimuth % 360) / 8;
    ele_idx = elevation + 89;
    *left = (float*)&ptr->hdb_fft.hd_n75[0][azi_idx][ele_idx][0];
    *right = (float*)&ptr->hdb_fft.hd_n75[1][azi_idx][ele_idx][0];
  }
  else if (elevation <= -60)
  {
    azi_idx = (azimuth % 360) / 4;
    ele_idx = elevation + 74;
    *left = (float*)&ptr->hdb_fft.hd_n60[0][azi_idx][ele_idx][0];
    *right = (float*)&ptr->hdb_fft.hd_n60[1][azi_idx][ele_idx][0];
  }
  else if (elevation <= -45)
  {
    azi_idx = (azimuth % 360) / 2;
    ele_idx = elevation + 59;
    *left = (float*)&ptr->hdb_fft.hd_n45[0][azi_idx][ele_idx][0];
    *right = (float*)&ptr->hdb_fft.hd_n45[1][azi_idx][ele_idx][0];
  }
  else if (elevation <= 44)
  {
    azi_idx = (azimuth % 360);
    ele_idx = elevation + 44;
    *left = (float*)&ptr->hdb_fft.hd_0[0][azi_idx][ele_idx][0];
    *right = (float*)&ptr->hdb_fft.hd_0[1][azi_idx][ele_idx][0];
  }
  else if (elevation <= 59)
  {
    azi_idx = (azimuth % 360) / 2;
    ele_idx = elevation - 45;
    *left = (float*)&ptr->hdb_fft.hd_45[0][azi_idx][ele_idx][0];
    *right = (float*)&ptr->hdb_fft.hd_45[1][azi_idx][ele_idx][0];
  }
  else if (elevation <= 74)
  {
    azi_idx = (azimuth % 360) / 4;
    ele_idx = elevation - 60;
    *left = (float*)&ptr->hdb_fft.hd_60[0][azi_idx][ele_idx][0];
    *right = (float*)&ptr->hdb_fft.hd_60[1][azi_idx][ele_idx][0];
  }
  else if (elevation <= 89)
  {
    azi_idx = (azimuth % 360) / 8;
    ele_idx = elevation - 75;
    *left = (float*)&ptr->hdb_fft.hd_75[0][azi_idx][ele_idx][0];
    *right = (float*)&ptr->hdb_fft.hd_75[1][azi_idx][ele_idx][0];
  }
  else
  {
    *left = (float*)&ptr->hdb_fft.hd_90[0][0];
    *right = (float*)&ptr->hdb_fft.hd_90[1][0];
  }
}

//=============================================================================
void getHRTFsphere128_angle(hrtf_sphere128_t* ptr,
  double azimuth, double elevation, float* left, float* right)
{
  // A = bottom-left
  // B = bottom-right
  // C = top-left
  // D = top-right

  float *a_left, *a_right, *b_left, *b_right,
    *c_left, *c_right, *d_left, *d_right;

  int i = 0;
  int azi_floor, ele_floor, ele_ceil;
  int azi_bl, azi_br, azi_tl, azi_tr;
  float alpha = 0.0f, beta = 0.0f, gamma = 0.0f, a_w, b_w, c_w, d_w;

  azimuth = fmod(fmod(azimuth, 360.0) + 360.0, 360.0);
  elevation = elevation > 90.0 ? 90.0 : elevation < -90.0 ? -90.0 : elevation;

  azi_floor = (int)floor(azimuth);
  ele_floor = (int)floor(elevation);
  ele_ceil = ele_floor + 1;
  if (ele_ceil > 90)
    ele_ceil = 90;

  azi_bl = getAziFloorForDesiredElev(azi_floor, ele_floor);
  azi_br = getAziCeilForDesiredElev(azi_bl, ele_floor);
  azi_tl = getAziFloorForDesiredElev(azi_floor, ele_ceil);
  azi_tr = getAziCeilForDesiredElev(azi_tl, ele_ceil);

  if (azi_br - azi_bl > 0)
  {
    alpha = ((float)azimuth - (float)azi_bl) / (float)(azi_br - azi_bl);
  }
  if (azi_tr - azi_tl > 0)
  {
    beta = ((float)azimuth - (float)azi_tl) / (float)(azi_tr - azi_tl);
  }
  gamma = (float)elevation - (float)floor(elevation);
  a_w = (1 - gamma) * (1 - alpha);
  b_w = (1 - gamma) * alpha;
  c_w = gamma * (1 - beta);
  d_w = gamma * beta;

  getHRTFsphere128Ptr(ptr, azi_bl, ele_floor, &a_left, &a_right);
  getHRTFsphere128Ptr(ptr, azi_br, ele_floor, &b_left, &b_right);
  getHRTFsphere128Ptr(ptr, azi_tl, ele_ceil, &c_left, &c_right);
  getHRTFsphere128Ptr(ptr, azi_tr, ele_ceil, &d_left, &d_right);

  if (left != NULL && right != NULL)
  {
    for (i = 0; i < 128; i++)
    {
      left[i] = a_w * a_left[i] + b_w * b_left[i] + c_w * c_left[i] + d_w * d_left[i];
      right[i] = a_w * a_right[i] + b_w * b_right[i] + c_w * c_right[i] + d_w * d_right[i];
    }
  }
  else if (left != NULL && right == NULL)
  {
    for (i = 0; i < 128; i++)
    {
      left[i] = a_w * a_left[i] + b_w * b_left[i] + c_w * c_left[i] + d_w * d_left[i];
    }
  }
  else if (left == NULL && right != NULL)
  {
    for (i = 0; i < 128; i++)
    {
      right[i] = a_w * a_right[i] + b_w * b_right[i] + c_w * c_right[i] + d_w * d_right[i];
    }
  }
}

//=============================================================================
void sortHRTFdbsByDistance128(hrtf_db128_t* ptr)
{
  // pretty inefficient, O(n^2), 
  // but n is usually VERY small, so whatever.

  double min_distance;
  int min_distance_idx;
  
  int *bools = (int*)malloc(sizeof(int) * ptr->hdb_count);
  int *order = (int*)malloc(sizeof(int) * ptr->hdb_count);
  hrtf_sphere128_t** temp_spheres = 
    (hrtf_sphere128_t**)malloc(sizeof(hrtf_sphere128_t*) * ptr->hdb_count);

  memset(bools, 0, sizeof(int) * ptr->hdb_count);
  memset(order, 0, sizeof(int) * ptr->hdb_count);

  int i = 0, j = 0;
  for (i = 0; i < ptr->hdb_count; i++)
  {
    min_distance = FLT_MAX;
    min_distance_idx = 0;
    for (j = 0; j < ptr->hdb_count; j++)
    {
      if (bools[j] == 0 && ptr->hdb_spheres[j]->hdb_radius < min_distance)
      {
        min_distance = ptr->hdb_spheres[j]->hdb_radius;
        min_distance_idx = j;
      }
    }
    if (min_distance_idx < ptr->hdb_count)
    {
      bools[min_distance_idx] = 1;
      order[i] = min_distance_idx;
    }
  }

  for (i = 0; i < ptr->hdb_count; i++)
  {
    temp_spheres[i] = ptr->hdb_spheres[order[i]];
  }
  for (i = 0; i < ptr->hdb_count; i++)
  {
    ptr->hdb_spheres[i] = temp_spheres[i];
  }

  free(temp_spheres);
  free(order);
  free(bools);
}

//=============================================================================
hrtf_db128_t* loadHRTFdb128_fromFiles(int num_dbs, ...)
{
  va_list arguments;
  va_start(arguments, num_dbs);

  int i = 0;
  hrtf_db128_t* ptr = NULL;
  ptr = (hrtf_db128_t*)malloc(sizeof(hrtf_db128_t));
  if (NULL == ptr)
    return NULL;

  ptr->hdb_count = num_dbs;
  ptr->hdb_spheres = 
    (hrtf_sphere128_t**)malloc(sizeof(hrtf_sphere128_t*) * num_dbs);
  memset(ptr->hdb_spheres, 0, sizeof(hrtf_sphere128_t*) * num_dbs);

  for (i = 0; i < num_dbs; i++)
  {
    char* filename = va_arg(arguments, char*);
    ptr->hdb_spheres[i] = loadHRTFsphere128_fromFile(filename);
    if (NULL == ptr->hdb_spheres[i])
    {
      destroyHRTFdb128(ptr);
      return NULL;
    }
  }

  sortHRTFdbsByDistance128(ptr);

  return ptr;
}

//=============================================================================
hrtf_db128_t* loadHRTFdb128_fromMemory(int num_dbs, ...)
{
  va_list arguments;
  va_start(arguments, num_dbs);

  int i = 0;
  hrtf_db128_t* ptr = NULL;
  ptr = (hrtf_db128_t*)malloc(sizeof(hrtf_db128_t));
  if (NULL == ptr)
    return NULL;

  ptr->hdb_count = num_dbs;
  ptr->hdb_spheres = 
    (hrtf_sphere128_t**)malloc(sizeof(hrtf_sphere128_t*) * num_dbs);
  for (i = 0; i < num_dbs; i++)
  {
    char* data = va_arg(arguments, char*);
    ptr->hdb_spheres[i] = loadHRTFsphere128_fromMemory(data);
    if (NULL == ptr->hdb_spheres[i])
    {
      destroyHRTFdb128(ptr);
      return NULL;
    }
  }

  sortHRTFdbsByDistance128(ptr);

  return ptr;
}

//=============================================================================
void destroyHRTFdb128(hrtf_db128_t* ptr)
{
  int i = 0;
  for (i = 0; i < ptr->hdb_count; i++)
  {
    free(ptr->hdb_spheres[i]);
  }
  free(ptr->hdb_spheres);
  free(ptr);
}

//=============================================================================
void getHRTF128_angle(hrtf_db128_t* ptr, double azimuth, double elevation,
  double radius, float* left, float* right)
{
  int i = 0;
  float i_alpha = 1.0f, ip_alpha = 0.0f;

  // find two nearest spheres
  for (i = 0; i < ptr->hdb_count; i++)
  {
    if (radius < ptr->hdb_spheres[i]->hdb_radius)
    {
      break;
    }
  }

  if (i > 0 && i < ptr->hdb_count)
  {
    i_alpha = (float)((radius - ptr->hdb_spheres[i - 1]->hdb_radius) /
      (ptr->hdb_spheres[i]->hdb_radius - ptr->hdb_spheres[i - 1]->hdb_radius));
    ip_alpha = (1 - i_alpha);

    getHRTFsphere128_angle(ptr->hdb_spheres[i - 1],
      azimuth, elevation, ptr->hdb_left, ptr->hdb_right);
    getHRTFsphere128_angle(ptr->hdb_spheres[i],
      azimuth, elevation, left, right);

    // interpolate between sets
    if (NULL != left && NULL != right)
    {
      for (i = 0; i < 128; i++)
      {
        left[i] = ip_alpha * ptr->hdb_left[i] + i_alpha * left[i];
        right[i] = ip_alpha * ptr->hdb_right[i] + i_alpha * right[i];
      }
    }
    else if (NULL != left)
    {
      for (i = 0; i < 128; i++)
      {
        left[i] = ip_alpha * ptr->hdb_left[i] + i_alpha * left[i];
      }
    }
    else if (NULL != right)
    {
      for (i = 0; i < 128; i++)
      {
        right[i] = ip_alpha * ptr->hdb_right[i] + i_alpha * right[i];
      }
    }
  }
  else if (i == 0)
  {
    getHRTFsphere128_angle(ptr->hdb_spheres[i],
      azimuth, elevation, left, right);
  }
  else
  {
    getHRTFsphere128_angle(ptr->hdb_spheres[i - 1],
      azimuth, elevation, left, right);
  }
}

//=============================================================================
void getHRTF128(hrtf_db128_t* hrtf_db, 
  double x, double y, double z, float* left, float* right)
{
  double pi = 4.0 * atan(1.0);
  double azimuth = atan2(z, x) / pi * 180.0;
  double elevation = atan2(y, sqrt(x * x + z * z)) / pi * 180.0;
  double radius = sqrt(x * x + y * y + z * z);

  getHRTF128_angle(hrtf_db, 
    azimuth, elevation, radius, left, right);
}

//=============================================================================
hrtf_sphere256_t* loadHRTFsphere256_fromFile(const char* filename)
{
  hrtf_sphere256_t* ptr = NULL;
  FILE* fp = NULL;

  fp = fopen(filename, "rb");
  if (NULL == fp)
    return NULL;

  ptr = (hrtf_sphere256_t*)malloc(sizeof(hrtf_sphere256_t));
  if (NULL != ptr)
  {
    // zero-set just in case
    memset(ptr, 0, sizeof(hrtf_sphere256_t));

    fseek(fp, 0, SEEK_END);
    size_t size = ftell(fp);
    if (size == sizeof(hrtf_sphere256_t))
    {
      fseek(fp, 0, SEEK_SET);
      fread((void*)ptr, sizeof(hrtf_sphere256_t), 1, fp);
    }
  }
  fclose(fp);

  return ptr;
}

//=============================================================================
hrtf_db128_t* loadHRTFdb128_fromFilesp(int num_dbs, char **fileps)
{

  int i = 0;
  hrtf_db128_t* ptr = NULL;
  ptr = (hrtf_db128_t*)malloc(sizeof(hrtf_db128_t));
  if (NULL == ptr) 
  {
    fprintf(stderr, "loadHRTFdb_fromFilesp: malloc failed\n");
    return NULL;
  }

  ptr->hdb_count = num_dbs;
  ptr->hdb_spheres =
    (hrtf_sphere128_t**)malloc(sizeof(hrtf_sphere128_t*) * num_dbs);
  if (!ptr->hdb_spheres) 
  {
    free(ptr);
    fprintf(stderr, "loadHRTFdb_fromFilesp: malloc failed\n");
    return NULL;
  }
  memset(ptr->hdb_spheres, 0, sizeof(hrtf_sphere128_t*) * num_dbs);

  for (i = 0; i < num_dbs; i++)
  {
    ptr->hdb_spheres[i] = loadHRTFsphere128_fromFile(fileps[i]);
    if (NULL == ptr->hdb_spheres[i])
    {
      fprintf(stderr, "loadHRTFdb_fromFilesp: could not load '%s'\n", fileps[i]);
      destroyHRTFdb128(ptr);
      return NULL;
    }
  }

  sortHRTFdbsByDistance128(ptr);

  return ptr;
}

//=============================================================================
hrtf_sphere256_t* loadHRTFsphere256_fromMemory(char* data)
{
  hrtf_sphere256_t* ptr = NULL;

  ptr = (hrtf_sphere256_t*)malloc(sizeof(hrtf_sphere256_t));
  if (NULL == ptr)
    return NULL;

  memcpy(ptr, data, sizeof(hrtf_sphere256_t));
  return ptr;
}

//=============================================================================
void getHRTFsphere256Ptr(hrtf_sphere256_t* sphere,
  int azimuth, int elevation, float** left, float** right)
{
  int ele_idx = 0, azi_idx = 0;
  hrtf_data_fft256_t* ptr = &sphere->hdb_fft;

  if (elevation == -90)
  {
    *left = (float*)&ptr->hd_n90[0][0];
    *right = (float*)&ptr->hd_n90[1][0];
  }
  else if (elevation <= -75)
  {
    azi_idx = (azimuth % 360) / 8;
    ele_idx = elevation + 89;
    *left = (float*)&ptr->hd_n75[0][azi_idx][ele_idx][0];
    *right = (float*)&ptr->hd_n75[1][azi_idx][ele_idx][0];
  }
  else if (elevation <= -60)
  {
    azi_idx = (azimuth % 360) / 4;
    ele_idx = elevation + 74;
    *left = (float*)&ptr->hd_n60[0][azi_idx][ele_idx][0];
    *right = (float*)&ptr->hd_n60[1][azi_idx][ele_idx][0];
  }
  else if (elevation <= -45)
  {
    azi_idx = (azimuth % 360) / 2;
    ele_idx = elevation + 59;
    *left = (float*)&ptr->hd_n45[0][azi_idx][ele_idx][0];
    *right = (float*)&ptr->hd_n45[1][azi_idx][ele_idx][0];
  }
  else if (elevation <= 44)
  {
    azi_idx = (azimuth % 360);
    ele_idx = elevation + 44;
    *left = (float*)&ptr->hd_0[0][azi_idx][ele_idx][0];
    *right = (float*)&ptr->hd_0[1][azi_idx][ele_idx][0];
  }
  else if (elevation <= 59)
  {
    azi_idx = (azimuth % 360) / 2;
    ele_idx = elevation - 45;
    *left = (float*)&ptr->hd_45[0][azi_idx][ele_idx][0];
    *right = (float*)&ptr->hd_45[1][azi_idx][ele_idx][0];
  }
  else if (elevation <= 74)
  {
    azi_idx = (azimuth % 360) / 4;
    ele_idx = elevation - 60;
    *left = (float*)&ptr->hd_60[0][azi_idx][ele_idx][0];
    *right = (float*)&ptr->hd_60[1][azi_idx][ele_idx][0];
  }
  else if (elevation <= 89)
  {
    azi_idx = (azimuth % 360) / 8;
    ele_idx = elevation - 75;
    *left = (float*)&ptr->hd_75[0][azi_idx][ele_idx][0];
    *right = (float*)&ptr->hd_75[1][azi_idx][ele_idx][0];
  }
  else
  {
    *left = (float*)&ptr->hd_90[0][0];
    *right = (float*)&ptr->hd_90[1][0];
  }
}

//=============================================================================
void getHRTFsphere256_angle(hrtf_sphere256_t* sphere,
  double azimuth, double elevation, float* left, float* right)
{
  // A = bottom-left
  // B = bottom-right
  // C = top-left
  // D = top-right

  float *a_left, *a_right, *b_left, *b_right,
    *c_left, *c_right, *d_left, *d_right;

  int i = 0;
  int azi_floor, ele_floor, ele_ceil;
  int azi_bl, azi_br, azi_tl, azi_tr;
  float alpha = 0.0f, beta = 0.0f, gamma = 0.0f, a_w, b_w, c_w, d_w;

  azimuth = fmod(fmod(azimuth, 360.0) + 360.0, 360.0);
  elevation = elevation > 90.0 ? 90.0 : elevation < -90.0 ? -90.0 : elevation;

  azi_floor = (int)floor(azimuth);
  ele_floor = (int)floor(elevation);
  ele_ceil = ele_floor + 1;
  if (ele_ceil > 90)
    ele_ceil = 90;

  azi_bl = getAziFloorForDesiredElev(azi_floor, ele_floor);
  azi_br = getAziCeilForDesiredElev(azi_bl, ele_floor);
  azi_tl = getAziFloorForDesiredElev(azi_floor, ele_ceil);
  azi_tr = getAziCeilForDesiredElev(azi_tl, ele_ceil);

  if (azi_br - azi_bl > 0)
  {
    alpha = ((float)azimuth - (float)azi_bl) / (float)(azi_br - azi_bl);
  }
  if (azi_tr - azi_tl > 0)
  {
    beta = ((float)azimuth - (float)azi_tl) / (float)(azi_tr - azi_tl);
  }
  gamma = (float)elevation - (float)floor(elevation);
  a_w = (1 - gamma) * (1 - alpha);
  b_w = (1 - gamma) * alpha;
  c_w = gamma * (1 - beta);
  d_w = gamma * beta;

  getHRTFsphere256Ptr(sphere, azi_bl, ele_floor, &a_left, &a_right);
  getHRTFsphere256Ptr(sphere, azi_br, ele_floor, &b_left, &b_right);
  getHRTFsphere256Ptr(sphere, azi_tl, ele_ceil, &c_left, &c_right);
  getHRTFsphere256Ptr(sphere, azi_tr, ele_ceil, &d_left, &d_right);

  if (left != NULL && right != NULL)
  {
    for (i = 0; i < 256; i++)
    {
      left[i] = a_w * a_left[i] + b_w * b_left[i] + c_w * c_left[i] + d_w * d_left[i];
      right[i] = a_w * a_right[i] + b_w * b_right[i] + c_w * c_right[i] + d_w * d_right[i];
    }
  }
  else if (left != NULL && right == NULL)
  {
    for (i = 0; i < 256; i++)
    {
      left[i] = a_w * a_left[i] + b_w * b_left[i] + c_w * c_left[i] + d_w * d_left[i];
    }
  }
  else if (left == NULL && right != NULL)
  {
    for (i = 0; i < 256; i++)
    {
      right[i] = a_w * a_right[i] + b_w * b_right[i] + c_w * c_right[i] + d_w * d_right[i];
    }
  }
}

//=============================================================================
void sortHRTFdbsByDistance256(hrtf_db256_t* ptr)
{
  // pretty inefficient, O(n^2), 
  // but n is usually VERY small, so whatever.

  double min_distance;
  int min_distance_idx;

  int *bools = (int*)malloc(sizeof(int) * ptr->hdb_count);
  int *order = (int*)malloc(sizeof(int) * ptr->hdb_count);
  hrtf_sphere256_t** temp_spheres =
    (hrtf_sphere256_t**)malloc(sizeof(hrtf_sphere256_t*) * ptr->hdb_count);

  memset(bools, 0, sizeof(int) * ptr->hdb_count);
  memset(order, 0, sizeof(int) * ptr->hdb_count);

  int i = 0, j = 0;
  for (i = 0; i < ptr->hdb_count; i++)
  {
    min_distance = FLT_MAX;
    min_distance_idx = 0;
    for (j = 0; j < ptr->hdb_count; j++)
    {
      if (bools[j] == 0 && ptr->hdb_spheres[j]->hdb_radius < min_distance)
      {
        min_distance = ptr->hdb_spheres[j]->hdb_radius;
        min_distance_idx = j;
      }
    }
    if (min_distance_idx < ptr->hdb_count)
    {
      bools[min_distance_idx] = 1;
      order[i] = min_distance_idx;
    }
  }

  for (i = 0; i < ptr->hdb_count; i++)
  {
    temp_spheres[i] = ptr->hdb_spheres[order[i]];
  }
  for (i = 0; i < ptr->hdb_count; i++)
  {
    ptr->hdb_spheres[i] = temp_spheres[i];
  }

  free(temp_spheres);
  free(order);
  free(bools);
}

//=============================================================================
hrtf_db256_t* loadHRTFdb256_fromFiles(int num_dbs, ...)
{
  va_list arguments;
  va_start(arguments, num_dbs);

  int i = 0;
  hrtf_db256_t* ptr = NULL;
  ptr = (hrtf_db256_t*)malloc(sizeof(hrtf_db256_t));
  if (NULL == ptr)
    return NULL;

  ptr->hdb_count = num_dbs;
  ptr->hdb_spheres =
    (hrtf_sphere256_t**)malloc(sizeof(hrtf_sphere256_t*) * num_dbs);
  memset(ptr->hdb_spheres, 0, sizeof(hrtf_sphere256_t*) * num_dbs);

  for (i = 0; i < num_dbs; i++)
  {
    char* filename = va_arg(arguments, char*);
    ptr->hdb_spheres[i] = loadHRTFsphere256_fromFile(filename);
    if (NULL == ptr->hdb_spheres[i])
    {
      destroyHRTFdb256(ptr);
      return NULL;
    }
  }

  sortHRTFdbsByDistance256(ptr);

  return ptr;
}

//=============================================================================
hrtf_db256_t* loadHRTFdb256_fromFilesp(int num_dbs, char **fileps)
{

  int i = 0;
  hrtf_db256_t* ptr = NULL;
  ptr = (hrtf_db256_t*)malloc(sizeof(hrtf_db256_t));
  if (NULL == ptr) {
    fprintf(stderr, "loadHRTFdb_fromFilesp: malloc failed\n");
    return NULL;
  }

  ptr->hdb_count = num_dbs;
  ptr->hdb_spheres =
    (hrtf_sphere256_t**)malloc(sizeof(hrtf_sphere256_t*) * num_dbs);
  if (!ptr->hdb_spheres) {
    free(ptr);
    fprintf(stderr, "loadHRTFdb_fromFilesp: malloc failed\n");
    return NULL;
  }
  memset(ptr->hdb_spheres, 0, sizeof(hrtf_sphere256_t*) * num_dbs);

  for (i = 0; i < num_dbs; i++)
  {
    ptr->hdb_spheres[i] = loadHRTFsphere256_fromFile(fileps[i]);
    if (NULL == ptr->hdb_spheres[i])
    {
      fprintf(stderr, "loadHRTFdb_fromFilesp: could not load '%s'\n", fileps[i]);
      destroyHRTFdb256(ptr);
      return NULL;
    }
  }

  sortHRTFdbsByDistance256(ptr);

  return ptr;
}
//=============================================================================
hrtf_db256_t* loadHRTFdb256_fromMemory(int num_dbs, ...)
{
  va_list arguments;
  va_start(arguments, num_dbs);

  int i = 0;
  hrtf_db256_t* ptr = NULL;
  ptr = (hrtf_db256_t*)malloc(sizeof(hrtf_db256_t));
  if (NULL == ptr)
    return NULL;

  ptr->hdb_count = num_dbs;
  ptr->hdb_spheres =
    (hrtf_sphere256_t**)malloc(sizeof(hrtf_sphere256_t*) * num_dbs);
  for (i = 0; i < num_dbs; i++)
  {
    char* data = va_arg(arguments, char*);
    ptr->hdb_spheres[i] = loadHRTFsphere256_fromMemory(data);
    if (NULL == ptr->hdb_spheres[i])
    {
      destroyHRTFdb256(ptr);
      return NULL;
    }
  }

  sortHRTFdbsByDistance256(ptr);

  return ptr;
}

//=============================================================================
void destroyHRTFdb256(hrtf_db256_t* hrtf_db)
{
  int i = 0;
  for (i = 0; i < hrtf_db->hdb_count; i++)
  {
    free(hrtf_db->hdb_spheres[i]);
  }
  free(hrtf_db->hdb_spheres);
  free(hrtf_db);
}

//=============================================================================
void getHRTF256_angle(hrtf_db256_t* ptr, 
  double azimuth, double elevation, double radius, float* left, float* right)
{
  int i = 0;
  float i_alpha = 1.0f, ip_alpha = 0.0f;

  // find two nearest spheres
  for (i = 0; i < ptr->hdb_count; i++)
  {
    if (radius < ptr->hdb_spheres[i]->hdb_radius)
    {
      break;
    }
  }

  if (i > 0 && i < ptr->hdb_count)
  {
    i_alpha = (float)((radius - ptr->hdb_spheres[i - 1]->hdb_radius) /
      (ptr->hdb_spheres[i]->hdb_radius - ptr->hdb_spheres[i - 1]->hdb_radius));
    ip_alpha = (1 - i_alpha);

    getHRTFsphere256_angle(ptr->hdb_spheres[i - 1],
      azimuth, elevation, ptr->hdb_left, ptr->hdb_right);
    getHRTFsphere256_angle(ptr->hdb_spheres[i],
      azimuth, elevation, left, right);

    // interpolate between sets
    if (NULL != left && NULL != right)
    {
      for (i = 0; i < 256; i++)
      {
        left[i] = ip_alpha * ptr->hdb_left[i] + i_alpha * left[i];
        right[i] = ip_alpha * ptr->hdb_right[i] + i_alpha * right[i];
      }
    }
    else if (NULL != left)
    {
      for (i = 0; i < 256; i++)
      {
        left[i] = ip_alpha * ptr->hdb_left[i] + i_alpha * left[i];
      }
    }
    else if (NULL != right)
    {
      for (i = 0; i < 256; i++)
      {
        right[i] = ip_alpha * ptr->hdb_right[i] + i_alpha * right[i];
      }
    }
  }
  else if (i == 0)
  {
    getHRTFsphere256_angle(ptr->hdb_spheres[i],
      azimuth, elevation, left, right);
  }
  else
  {
    getHRTFsphere256_angle(ptr->hdb_spheres[i - 1],
      azimuth, elevation, left, right);
  }
}

//=============================================================================
void getHRTF256(hrtf_db256_t* ptr, 
  double x, double y, double z, float* left, float* right)
{
  double pi = 4.0 * atan(1.0);
  double azimuth = atan2(z, x) / pi * 180.0;
  double elevation = atan2(y, sqrt(x * x + z * z)) / pi * 180.0;
  double radius = sqrt(x * x + y * y + z * z);

  getHRTF256_angle(ptr,
    azimuth, elevation, radius, left, right);
}

//=============================================================================
void getHRTF128_angle_ears(hrtf_db128_t* ptr,
  double azimuth, double elevation, double radius, float* left, float* right)
{
  int i = 0;
  float i_alpha = 1.0f, ip_alpha = 0.0f;
  double laz0, lel0, raz0, rel0, laz1, lel1, raz1, rel1;

  // find two nearest spheres
  for (i = 0; i < ptr->hdb_count; i++)
  {
    if (radius < ptr->hdb_spheres[i]->hdb_radius)
    {
      break;
    }
  }

  if (i > 0 && i < ptr->hdb_count)
  {
    i_alpha = (float)((radius - ptr->hdb_spheres[i - 1]->hdb_radius) /
      (ptr->hdb_spheres[i]->hdb_radius - ptr->hdb_spheres[i - 1]->hdb_radius));
    ip_alpha = (1 - i_alpha);

    getRaySphereIntersectionAngles(azimuth, elevation, radius,
      ptr->hdb_spheres[i-1]->hdb_radius, ptr->hdb_spheres[i-1]->hdb_headSize * 0.5,
      &laz0, &lel0, &raz0, &rel0);

    getRaySphereIntersectionAngles(azimuth, elevation, radius,
      ptr->hdb_spheres[i]->hdb_radius, ptr->hdb_spheres[i]->hdb_headSize * 0.5,
      &laz1, &lel1, &raz1, &rel1);

    if (NULL != left)
      getHRTFsphere128_angle(ptr->hdb_spheres[i - 1],
      laz0, lel0, ptr->hdb_left, NULL);
    if (NULL != right)
      getHRTFsphere128_angle(ptr->hdb_spheres[i - 1],
      raz0, rel0, NULL, ptr->hdb_right);
    
    if (NULL != left)
      getHRTFsphere128_angle(ptr->hdb_spheres[i],
      laz1, lel1, left, NULL);
    if (NULL != right)
      getHRTFsphere128_angle(ptr->hdb_spheres[i],
      raz1, rel1, NULL, right);

    // interpolate between sets
    if (NULL != left && NULL != right)
    {
      for (i = 0; i < 256; i++)
      {
        left[i] = ip_alpha * ptr->hdb_left[i] + i_alpha * left[i];
        right[i] = ip_alpha * ptr->hdb_right[i] + i_alpha * right[i];
      }
    }
    else if (NULL != left)
    {
      for (i = 0; i < 128; i++)
      {
        left[i] = ip_alpha * ptr->hdb_left[i] + i_alpha * left[i];
      }
    }
    else if (NULL != right)
    {
      for (i = 0; i < 128; i++)
      {
        right[i] = ip_alpha * ptr->hdb_right[i] + i_alpha * right[i];
      }
    }
  }
  else if (i == 0)
  {
    getRaySphereIntersectionAngles(azimuth, elevation, radius,
      ptr->hdb_spheres[i]->hdb_radius, ptr->hdb_spheres[i]->hdb_headSize * 0.5,
      &laz1, &lel1, &raz1, &rel1);

    if (NULL != left)
      getHRTFsphere128_angle(ptr->hdb_spheres[i],
      laz1, lel1, left, NULL);
    if (NULL != right)
      getHRTFsphere128_angle(ptr->hdb_spheres[i],
      raz1, rel1, NULL, right);
  }
  else
  {
    getRaySphereIntersectionAngles(azimuth, elevation, radius,
      ptr->hdb_spheres[i - 1]->hdb_radius, ptr->hdb_spheres[i - 1]->hdb_headSize * 0.5,
      &laz0, &lel0, &raz0, &rel0);

    if (NULL != left)
      getHRTFsphere128_angle(ptr->hdb_spheres[i - 1],
      laz0, lel0, left, NULL);
    if (NULL != right)
      getHRTFsphere128_angle(ptr->hdb_spheres[i - 1],
      raz0, rel0, NULL, right);
  }
}

//=============================================================================
void getHRTF256_angle_ears(hrtf_db256_t* ptr,
  double azimuth, double elevation, double radius, float* left, float* right)
{
  int i = 0;
  float i_alpha = 1.0f, ip_alpha = 0.0f;
  double laz0, lel0, raz0, rel0, laz1, lel1, raz1, rel1;

  // find two nearest spheres
  for (i = 0; i < ptr->hdb_count; i++)
  {
    if (radius < ptr->hdb_spheres[i]->hdb_radius)
    {
      break;
    }
  }

  if (i > 0 && i < ptr->hdb_count)
  {
    i_alpha = (float)((radius - ptr->hdb_spheres[i - 1]->hdb_radius) /
      (ptr->hdb_spheres[i]->hdb_radius - ptr->hdb_spheres[i - 1]->hdb_radius));
    ip_alpha = (1 - i_alpha);

    getRaySphereIntersectionAngles(azimuth, elevation, radius,
      ptr->hdb_spheres[i - 1]->hdb_radius, ptr->hdb_spheres[i - 1]->hdb_headSize * 0.5,
      &laz0, &lel0, &raz0, &rel0);

    getRaySphereIntersectionAngles(azimuth, elevation, radius,
      ptr->hdb_spheres[i]->hdb_radius, ptr->hdb_spheres[i]->hdb_headSize * 0.5,
      &laz1, &lel1, &raz1, &rel1);

    if (NULL != left)
      getHRTFsphere256_angle(ptr->hdb_spheres[i - 1],
        laz0, lel0, ptr->hdb_left, NULL);
    if (NULL != right)
      getHRTFsphere256_angle(ptr->hdb_spheres[i - 1],
        raz0, rel0, NULL, ptr->hdb_right);

    if (NULL != left)
      getHRTFsphere256_angle(ptr->hdb_spheres[i],
        laz1, lel1, left, NULL);
    if (NULL != right)
      getHRTFsphere256_angle(ptr->hdb_spheres[i],
        raz1, rel1, NULL, right);

    // interpolate between sets
    if (NULL != left && NULL != right)
    {
      for (i = 0; i < 256; i++)
      {
        left[i] = ip_alpha * ptr->hdb_left[i] + i_alpha * left[i];
        right[i] = ip_alpha * ptr->hdb_right[i] + i_alpha * right[i];
      }
    }
    else if (NULL != left)
    {
      for (i = 0; i < 256; i++)
      {
        left[i] = ip_alpha * ptr->hdb_left[i] + i_alpha * left[i];
      }
    }
    else if (NULL != right)
    {
      for (i = 0; i < 256; i++)
      {
        right[i] = ip_alpha * ptr->hdb_right[i] + i_alpha * right[i];
      }
    }
  }
  else if (i == 0)
  {
    getRaySphereIntersectionAngles(azimuth, elevation, radius,
      ptr->hdb_spheres[i]->hdb_radius, ptr->hdb_spheres[i]->hdb_headSize * 0.5,
      &laz1, &lel1, &raz1, &rel1);

    if (NULL != left)
      getHRTFsphere256_angle(ptr->hdb_spheres[i],
        laz1, lel1, left, NULL);
    if (NULL != right)
      getHRTFsphere256_angle(ptr->hdb_spheres[i],
        raz1, rel1, NULL, right);
  }
  else
  {
    getRaySphereIntersectionAngles(azimuth, elevation, radius,
      ptr->hdb_spheres[i - 1]->hdb_radius, ptr->hdb_spheres[i - 1]->hdb_headSize * 0.5,
      &laz0, &lel0, &raz0, &rel0);

    if (NULL != left)
      getHRTFsphere256_angle(ptr->hdb_spheres[i - 1],
        laz0, lel0, left, NULL);
    if (NULL != right)
      getHRTFsphere256_angle(ptr->hdb_spheres[i - 1],
        raz0, rel0, NULL, right);
  }
}

//=============================================================================
void getHRTF128_ears(hrtf_db128_t* ptr,
  double x, double y, double z, float* left, float* right)
{
  double pi = 4.0 * atan(1.0);
  double azimuth = atan2(z, x) / pi * 180.0;
  double elevation = atan2(y, sqrt(x * x + z * z)) / pi * 180.0;
  double radius = sqrt(x * x + y * y + z * z);

  getHRTF128_angle_ears(ptr,
    azimuth, elevation, radius, left, right);
}

//=============================================================================
void getHRTF256_ears(hrtf_db256_t* ptr,
  double x, double y, double z, float* left, float* right)
{
  double pi = 4.0 * atan(1.0);
  double azimuth = atan2(z, x) / pi * 180.0;
  double elevation = atan2(y, sqrt(x * x + z * z)) / pi * 180.0;
  double radius = sqrt(x * x + y * y + z * z);

  getHRTF256_angle_ears(ptr,
    azimuth, elevation, radius, left, right);
}
