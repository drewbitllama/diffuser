#ifndef HRTF_DB_H__
#define HRTF_DB_H__

//=============================================================================
// Meyer FFT format, where N is full FFT size (N/2 is nyquist)
//   H(0), real(H(1)), real(H(2)), ..., H(N/2), ... imag(H(2)), imag(H(1))
//
// i.e. DC (real), real of ascending non-DC/non-nyquist, nyquist (real), 
//      imag of descending non-DC/non-nyquist.

//=============================================================================
void getRaySphereIntersectionAngles(double source_azimuth,
  double source_elevation, double source_radius, double hrtf_radius,
  double head_radius, double* left_azimuth, double* left_elevation,
  double* right_azimuth, double* right_elevation);

typedef struct hrtf_data_fft128
{
  float hd_n90[2][128];           /* -90 degrees */
  float hd_n75[2][45][15][128];   /* -89 to -75 degrees */
  float hd_n60[2][90][15][128];   /* -74 to -60 degrees */
  float hd_n45[2][180][15][128];	/* -59 to -45 degrees */
  float hd_0[2][360][89][128];    /* -44 to 44 degrees */
  float hd_45[2][180][15][128];	  /* 45 to 59 degrees */
  float hd_60[2][90][15][128];	  /* 60 to 74 degrees */
  float hd_75[2][45][15][128];	  /* 75 to 89 degrees */
  float hd_90[2][128];			      /* 90 degrees */

} hrtf_data_fft128_t;

//=============================================================================
typedef struct hrtf_data_fft256
{
  float hd_n90[2][256];           /* -90 degrees */
  float hd_n75[2][45][15][256];   /* -89 to -75 degrees */
  float hd_n60[2][90][15][256];   /* -74 to -60 degrees */
  float hd_n45[2][180][15][256];	/* -59 to -45 degrees */
  float hd_0[2][360][89][256];    /* -44 to 44 degrees */
  float hd_45[2][180][15][256];	  /* 45 to 59 degrees */
  float hd_60[2][90][15][256];	  /* 60 to 74 degrees */
  float hd_75[2][45][15][256];	  /* 75 to 89 degrees */
  float hd_90[2][256];			      /* 90 degrees */

} hrtf_data_fft256_t;

//=============================================================================
typedef struct hrtf_sphere128
{
  char               hdb_name[8];      /* database name */
  int                hdb_user;         /* user number */
  int                _reserved;        /* reserved so byte-aligned */
  char               hdb_interp[8];    /* interpolation type */
  double             hdb_headSize;     /* diameter of the head (meters) */
  double             hdb_radius;       /* the radius of object */
  hrtf_data_fft128_t hdb_fft;          /* fft interpolation */

} hrtf_sphere128_t;

//=============================================================================
typedef struct hrtf_sphere256
{
  char               hdb_name[8];      /* database name */
  int                hdb_user;         /* user number */
  int                _reserved;        /* reserved so byte-aligned */
  char               hdb_interp[8];    /* interpolation type */
  double             hdb_headSize;     /* diameter of the head (meters) */
  double             hdb_radius;       /* the radius of object */
  hrtf_data_fft256_t hdb_fft;          /* fft interpolation */

} hrtf_sphere256_t;

//=============================================================================
typedef struct hrtf_db128
{
  hrtf_sphere128_t** hdb_spheres;      /* spheres in database */
  int                hdb_count;        /* num of spheres */
  float              hdb_left[128];    /* temp storage for loading */
  float              hdb_right[128];   /* temp storage for loading */

} hrtf_db128_t;

//=============================================================================
typedef struct hrtf_db256
{
  hrtf_sphere256_t** hdb_spheres;      /* spheres in database */
  int                hdb_count;        /* num of spheres */
  float              hdb_left[256];    /* temp storage for loading */
  float              hdb_right[256];   /* temp storage for loading */

} hrtf_db256_t;

//=============================================================================
hrtf_db128_t* loadHRTFdb128_fromFiles(int num_dbs, ...); /* list of filenames */
hrtf_db128_t* loadHRTFdb128_fromFilesp(int num_dbs, char **fileps);
hrtf_db256_t* loadHRTFdb256_fromFiles(int num_dbs, ...);
hrtf_db256_t* loadHRTFdb256_fromFilesp(int num_dbs, char **fileps);

//=============================================================================
hrtf_db128_t* loadHRTFdb128_fromMemory(int num_dbs, ...); /* list of char*'s */
hrtf_db256_t* loadHRTFdb256_fromMemory(int num_dbs, ...);

//=============================================================================
void destroyHRTFdb128(hrtf_db128_t* ptr);
void destroyHRTFdb256(hrtf_db256_t* ptr);

//=============================================================================
void getHRTF128_angle(hrtf_db128_t* ptr, 
  double azimuth, double elevation, double radius, float* left, float* right);
void getHRTF256_angle(hrtf_db256_t* ptr, 
  double azimuth, double elevation, double radius, float* left, float* right);

//=============================================================================
void getHRTF128(hrtf_db128_t* ptr, 
  double x, double y, double z, float* left, float* right);
void getHRTF256(hrtf_db256_t* ptr, 
  double x, double y, double z, float* left, float* right);

//=============================================================================
void getHRTF128_angle_ears(hrtf_db128_t* ptr,
  double azimuth, double elevation, double radius, float* left, float* right);
void getHRTF256_angle_ears(hrtf_db256_t* ptr,
  double azimuth, double elevation, double radius, float* left, float* right);

//=============================================================================
void getHRTF128_ears(hrtf_db128_t* ptr,
  double x, double y, double z, float* left, float* right);
void getHRTF256_ears(hrtf_db256_t* ptr,
  double x, double y, double z, float* left, float* right);

//=============================================================================

#endif /* HRTF_DB_H__ */
