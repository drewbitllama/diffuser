function [ y ] = intshift( x, int_shift )

x = x(:);
if (int_shift >= 0)    
    y = [x(1+int_shift:end); zeros(min(int_shift,length(x)),1)];
else
    y = [zeros(min(abs(int_shift),length(x)),1); x(1:end+int_shift)];
end

end

