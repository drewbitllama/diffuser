function [ tri_coord, tri_left, tri_right, hrtf_radius, head_width, left_ear, right_ear ] = make_cipic_triangles( subject )

hrtf_radius = 1;

addpath('./API_MO');
SOFAstart();
obj = SOFAload(sprintf('./database/cipic/subject_%03d.sofa',subject));
p = obj.SourcePosition;

coords = zeros(25,50,3);
lefts = zeros(25,50,200);
rights = zeros(25,50,200);

idx = 1;
for j=1:25
    for i=1:50
        v = [-sin(p(idx,1)/180*pi)*cos(p(idx,2)/180*pi) sin(p(idx,2)/180*pi) cos(p(idx,1)/180*pi)*cos(p(idx,2)/180*pi)];
        coords(j,i,:) = v;
        lefts((25-j+1),i,:) = squeeze(obj.Data.IR(idx,1,:));
        rights((25-j+1),i,:) = squeeze(obj.Data.IR(idx,2,:));
        idx = idx + 1;
    end
end

% filename = sprintf('./CIPIC_hrtf_database/standard_hrir_database/subject_%03d/hrir_final.mat',subject);
% load(filename);
% lefts = hrir_l;
% rights = hrir_r;

% compute ear coordinates
head_width = 0.1449;
pinna_offset_down = 0.0303;
pinna_offset_back = 0.0046;
left_ear = [head_width/2 -pinna_offset_down -pinna_offset_back].';
right_ear = [-head_width/2 -pinna_offset_down -pinna_offset_back].';

% load CIPIC coordinates
cipic_azi = [-80 -65 -55 -45:5:45 55 65 80];
cipic_ele = -45 + 5.625*(0:49);

tri_coord = zeros(2496,3,3);
tri_left = zeros(2496,3,200);
tri_right = zeros(2496,3,200);

idx = 1;

% regular faces
for i=2:length(cipic_azi)
    for j=2:length(cipic_ele)
        
        v1 = squeeze(coords(i-1,j-1,:));
        l1 = squeeze(lefts(i-1,j-1,:));
        r1 = squeeze(rights(i-1,j-1,:));
        
        v2 = squeeze(coords(i-1,j,:));
        l2 = squeeze(lefts(i-1,j,:));
        r2 = squeeze(rights(i-1,j,:));
        
        v3 = squeeze(coords(i,j,:));
        l3 = squeeze(lefts(i,j,:));
        r3 = squeeze(rights(i,j,:));
        
        v4 = squeeze(coords(i,j-1,:));
        l4 = squeeze(lefts(i,j-1,:));
        r4 = squeeze(rights(i,j-1,:));
        
        tri_coord(idx,1,:) = v1;
        tri_coord(idx,2,:) = v2;
        tri_coord(idx,3,:) = v3;
        
        tri_left(idx,1,:) = l1;
        tri_left(idx,2,:) = l2;
        tri_left(idx,3,:) = l3;
                
        tri_right(idx,1,:) = r1;
        tri_right(idx,2,:) = r2;
        tri_right(idx,3,:) = r3;
        
        idx = idx + 1;

        tri_coord(idx,1,:) = v1;
        tri_coord(idx,2,:) = v3;
        tri_coord(idx,3,:) = v4;
        
        tri_left(idx,1,:) = l1;
        tri_left(idx,2,:) = l3;
        tri_left(idx,3,:) = l4;
        
        tri_right(idx,1,:) = r1;
        tri_right(idx,2,:) = r3;
        tri_right(idx,3,:) = r4;

        idx = idx + 1;
        
    end
end

% bottom faces
for i=2:length(cipic_azi)
    
    v1 = squeeze(coords(i-1,1,:));
    l1 = squeeze(lefts(i-1,1,:));
    r1 = squeeze(rights(i-1,1,:));

    v2 = squeeze(coords(i-1,50,:));
    l2 = squeeze(lefts(i-1,50,:));
    r2 = squeeze(rights(i-1,50,:));

    v3 = squeeze(coords(i,50,:));
    l3 = squeeze(lefts(i,50,:));
    r3 = squeeze(rights(i,50,:));

    v4 = squeeze(coords(i,1,:));
    l4 = squeeze(lefts(i,1,:));
    r4 = squeeze(rights(i,1,:));
    
    tri_coord(idx,1,:) = v1;
    tri_coord(idx,2,:) = v2;
    tri_coord(idx,3,:) = v3;

    tri_left(idx,1,:) = l1;
    tri_left(idx,2,:) = l2;
    tri_left(idx,3,:) = l3;

    tri_right(idx,1,:) = r1;
    tri_right(idx,2,:) = r2;
    tri_right(idx,3,:) = r3;

    idx = idx + 1;

    tri_coord(idx,1,:) = v1;
    tri_coord(idx,2,:) = v3;
    tri_coord(idx,3,:) = v4;

    tri_left(idx,1,:) = l1;
    tri_left(idx,2,:) = l3;
    tri_left(idx,3,:) = l4;

    tri_right(idx,1,:) = r1;
    tri_right(idx,2,:) = r3;
    tri_right(idx,3,:) = r4;

    idx = idx + 1;
    
end

% side faces (left and right sides)
for i=2:length(cipic_ele)/2

    v1 = squeeze(coords(1,i-1,:));
    l1 = squeeze(lefts(1,i-1,:));
    r1 = squeeze(rights(1,i-1,:));

    v2 = squeeze(coords(1,i,:));
    l2 = squeeze(lefts(1,i,:));
    r2 = squeeze(rights(1,i,:));

    v3 = squeeze(coords(1,50-i+1,:));
    l3 = squeeze(lefts(1,50-i+1,:));
    r3 = squeeze(rights(1,50-i+1,:));

    v4 = squeeze(coords(1,50-i+2,:));
    l4 = squeeze(lefts(1,50-i+2,:));
    r4 = squeeze(rights(1,50-i+2,:));
    
    tri_coord(idx,1,:) = v1;
    tri_coord(idx,2,:) = v2;
    tri_coord(idx,3,:) = v3;

    tri_left(idx,1,:) = l1;
    tri_left(idx,2,:) = l2;
    tri_left(idx,3,:) = l3;

    tri_right(idx,1,:) = r1;
    tri_right(idx,2,:) = r2;
    tri_right(idx,3,:) = r3;

    idx = idx + 1;

    tri_coord(idx,1,:) = v1;
    tri_coord(idx,2,:) = v3;
    tri_coord(idx,3,:) = v4;
    
    tri_left(idx,1,:) = l1;
    tri_left(idx,2,:) = l3;
    tri_left(idx,3,:) = l4;

    tri_right(idx,1,:) = r1;
    tri_right(idx,2,:) = r3;
    tri_right(idx,3,:) = r4;

    idx = idx + 1;

end

for i=2:length(cipic_ele)/2

    v1 = squeeze(coords(25,i-1,:));
    l1 = squeeze(lefts(25,i-1,:));
    r1 = squeeze(rights(25,i-1,:));

    v2 = squeeze(coords(25,i,:));
    l2 = squeeze(lefts(25,i,:));
    r2 = squeeze(rights(25,i,:));

    v3 = squeeze(coords(25,50-i+1,:));
    l3 = squeeze(lefts(25,50-i+1,:));
    r3 = squeeze(rights(25,50-i+1,:));

    v4 = squeeze(coords(25,50-i+2,:));
    l4 = squeeze(lefts(25,50-i+2,:));
    r4 = squeeze(rights(25,50-i+2,:));
    
    tri_coord(idx,1,:) = v1;
    tri_coord(idx,2,:) = v2;
    tri_coord(idx,3,:) = v3;

    tri_left(idx,1,:) = l1;
    tri_left(idx,2,:) = l2;
    tri_left(idx,3,:) = l3;

    tri_right(idx,1,:) = r1;
    tri_right(idx,2,:) = r2;
    tri_right(idx,3,:) = r3;

    idx = idx + 1;

    tri_coord(idx,1,:) = v1;
    tri_coord(idx,2,:) = v3;
    tri_coord(idx,3,:) = v4;

    tri_left(idx,1,:) = l1;
    tri_left(idx,2,:) = l3;
    tri_left(idx,3,:) = l4;

    tri_right(idx,1,:) = r1;
    tri_right(idx,2,:) = r3;
    tri_right(idx,3,:) = r4;

    idx = idx + 1;

end

end

% function [ coords, indexes, lookups ] = get_cipic_cartcoords( )
% 
% azi = [-80 -65 -55 -45:5:45 55 65 80];
% ele = -45 + 5.625*(0:49);
% coords = [];
% indexes = [];
% lookups = zeros(length(azi),length(ele),3);
% 
% % convert CIPIC angles to cartesian coordinates
% idx = 1;
% for i=1:length(azi)
%     for j=1:length(ele)
%         coords = [coords; cipic2cart(azi(i),ele(j))];
%         indexes = [indexes; i j];
%         lookups(i,j,:) = [idx azi(i) ele(j)];
%         idx = idx + 1;
%     end
% end
% 
% end
% 
% function [ v ] = cipic2cart( azi, ele )
% 
% theta = azi/180*pi;
% phi = ele/180*pi;
% 
% v = [0 0 1]';
% 
% Rx = [1 0 0; 0 cos(phi) sin(phi); 0 -sin(phi) cos(phi)];
% Ry = [cos(theta) 0 sin(theta); 0 1 0; -sin(theta) 0 cos(theta)];
% 
% v = Rx * Ry * v;
% v = v';
% 
% end