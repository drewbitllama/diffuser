function [ tri_coord, tri_left, tri_right, hrtf_radius, head_width, left_ear, right_ear ] = ...
    make_listen_triangles( subject )

addpath('./API_MO');
SOFAstart();
obj = SOFAload(sprintf('./database/listen/irc_%04d.sofa',subject));

origin = obj.EmitterPosition;
ear_distances = obj.ReceiverPosition;

head_width = sqrt(sum((ear_distances(1,:) - ear_distances(2,:)).^2));
left = sqrt(sum((ear_distances(1,:) - origin).^2));
right = sqrt(sum((ear_distances(2,:) - origin).^2));

left_ear = [left 0 0].';
right_ear = [-right 0 0].';
hrtf_radius = mean(obj.SourcePosition(:,3));

ele = [ones(24,1)*-45; ones(24,1)*-30; ones(24,1)*-15; ones(24,1)*0; ones(24,1)*15; ones(24,1)*30; ones(24,1)*45; ones(12,1)*60; ones(6,1)*75; 90];
azi = [(0:15:359).'; (0:15:359).'; (0:15:359).'; (0:15:359).'; (0:15:359).'; (0:15:359).'; (0:15:359).'; (0:30:359).'; (0:60:359).'; 0]; 

p = obj.SourcePosition;

prev = Inf;
ele_idx = 0;
azi_idx = 0;

coords = zeros(1,1,3);
lefts = zeros(1,1,512);
rights = zeros(1,1,512);

match = zeros(length(ele),1);
for i=1:length(ele)
    
    if (ele(i) == prev)
        azi_idx = azi_idx + 1;
    else
        azi_idx = 1;
        ele_idx = ele_idx + 1;
    end
    
    prev = ele(i);

    for j=1:size(p,1)
        
        if ele(i) == p(j,2) && (azi(i) == mod(360 - p(j,1), 360))
            if match(i) == 0
                match(i) = j;
            else
                error('multi match');
            end
        end
        
    end
    if match(i) == 0
        error('no match');
    end
    
    v = [-sin(azi(i)/180*pi)*cos(ele(i)/180*pi) sin(ele(i)/180*pi) cos(azi(i)/180*pi)*cos(ele(i)/180*pi)];
    coords(ele_idx,azi_idx,:) = v;
    l = squeeze(obj.Data.IR(match(i),1,:));
    r = squeeze(obj.Data.IR(match(i),2,:));
    lefts(ele_idx,azi_idx,:) = l;
    rights(ele_idx,azi_idx,:) = r;
    
end

% regular face
tri_coord = zeros(1,3,3);
tri_left = zeros(1,3,512);
tri_right = zeros(1,3,512);

tri_idx = 1;
for i=1:6
    for j=1:24
        jn = mod(j,24)+1;
        
        tri_coord(tri_idx,1,:) = squeeze(coords(i,j,:));
        tri_coord(tri_idx,2,:) = squeeze(coords(i+1,j,:));
        tri_coord(tri_idx,3,:) = squeeze(coords(i,jn,:));
        
        tri_left(tri_idx,1,:) = squeeze(lefts(i,j,:));
        tri_left(tri_idx,2,:) = squeeze(lefts(i+1,j,:));
        tri_left(tri_idx,3,:) = squeeze(lefts(i,jn,:));
        
        tri_right(tri_idx,1,:) = squeeze(rights(i,j,:));
        tri_right(tri_idx,2,:) = squeeze(rights(i+1,j,:));
        tri_right(tri_idx,3,:) = squeeze(rights(i,jn,:));
        
        tri_idx = tri_idx + 1;

        tri_coord(tri_idx,1,:) = squeeze(coords(i+1,j,:));
        tri_coord(tri_idx,2,:) = squeeze(coords(i,jn,:));
        tri_coord(tri_idx,3,:) = squeeze(coords(i+1,jn,:));

        tri_left(tri_idx,1,:) = squeeze(lefts(i+1,j,:));
        tri_left(tri_idx,2,:) = squeeze(lefts(i,jn,:));
        tri_left(tri_idx,3,:) = squeeze(lefts(i+1,jn,:));
        
        tri_right(tri_idx,1,:) = squeeze(rights(i+1,j,:));
        tri_right(tri_idx,2,:) = squeeze(rights(i,jn,:));
        tri_right(tri_idx,3,:) = squeeze(rights(i+1,jn,:));
        
        tri_idx = tri_idx + 1;
    end
end

% midface
for j=1:12
    jn = mod(j,12)+1;
    jd = (j-1)*2+1;
    jdn = mod(jd+1,24)+1;
    
    tri_coord(tri_idx,1,:) = squeeze(coords(7,jd,:));
    tri_coord(tri_idx,2,:) = squeeze(coords(8,j,:));
    tri_coord(tri_idx,3,:) = squeeze(coords(7,jd+1,:));

    tri_left(tri_idx,1,:) = squeeze(lefts(7,jd,:));
    tri_left(tri_idx,2,:) = squeeze(lefts(8,j,:));
    tri_left(tri_idx,3,:) = squeeze(lefts(7,jd+1,:));

    tri_right(tri_idx,1,:) = squeeze(rights(7,jd,:));
    tri_right(tri_idx,2,:) = squeeze(rights(8,j,:));
    tri_right(tri_idx,3,:) = squeeze(rights(7,jd+1,:));

    tri_idx = tri_idx + 1;

    tri_coord(tri_idx,1,:) = squeeze(coords(8,j,:));
    tri_coord(tri_idx,2,:) = squeeze(coords(8,jn,:));
    tri_coord(tri_idx,3,:) = squeeze(coords(7,jd+1,:));

    tri_left(tri_idx,1,:) = squeeze(lefts(8,j,:));
    tri_left(tri_idx,2,:) = squeeze(lefts(8,jn,:));
    tri_left(tri_idx,3,:) = squeeze(lefts(7,jd+1,:));

    tri_right(tri_idx,1,:) = squeeze(rights(8,j,:));
    tri_right(tri_idx,2,:) = squeeze(rights(8,jn,:));
    tri_right(tri_idx,3,:) = squeeze(rights(7,jd+1,:));

    tri_idx = tri_idx + 1;

    tri_coord(tri_idx,1,:) = squeeze(coords(7,jd+1,:));
    tri_coord(tri_idx,2,:) = squeeze(coords(8,jn,:));
    tri_coord(tri_idx,3,:) = squeeze(coords(7,jdn,:));

    tri_left(tri_idx,1,:) = squeeze(lefts(7,jd+1,:));
    tri_left(tri_idx,2,:) = squeeze(lefts(8,jn,:));
    tri_left(tri_idx,3,:) = squeeze(lefts(7,jdn,:));

    tri_right(tri_idx,1,:) = squeeze(rights(7,jd+1,:));
    tri_right(tri_idx,2,:) = squeeze(rights(8,jn,:));
    tri_right(tri_idx,3,:) = squeeze(rights(7,jdn,:));

    tri_idx = tri_idx + 1;

end

% midface
for j=1:6
    jn = mod(j,6)+1;
    jd = (j-1)*2+1;
    jdn = mod(jd+1,12)+1;
    
    tri_coord(tri_idx,1,:) = squeeze(coords(8,jd,:));
    tri_coord(tri_idx,2,:) = squeeze(coords(9,j,:));
    tri_coord(tri_idx,3,:) = squeeze(coords(8,jd+1,:));

    tri_left(tri_idx,1,:) = squeeze(lefts(8,jd,:));
    tri_left(tri_idx,2,:) = squeeze(lefts(9,j,:));
    tri_left(tri_idx,3,:) = squeeze(lefts(8,jd+1,:));

    tri_right(tri_idx,1,:) = squeeze(rights(8,jd,:));
    tri_right(tri_idx,2,:) = squeeze(rights(9,j,:));
    tri_right(tri_idx,3,:) = squeeze(rights(8,jd+1,:));

    tri_idx = tri_idx + 1;

    tri_coord(tri_idx,1,:) = squeeze(coords(9,j,:));
    tri_coord(tri_idx,2,:) = squeeze(coords(9,jn,:));
    tri_coord(tri_idx,3,:) = squeeze(coords(8,jd+1,:));

    tri_left(tri_idx,1,:) = squeeze(lefts(9,j,:));
    tri_left(tri_idx,2,:) = squeeze(lefts(9,jn,:));
    tri_left(tri_idx,3,:) = squeeze(lefts(8,jd+1,:));

    tri_right(tri_idx,1,:) = squeeze(rights(9,j,:));
    tri_right(tri_idx,2,:) = squeeze(rights(9,jn,:));
    tri_right(tri_idx,3,:) = squeeze(rights(8,jd+1,:));

    tri_idx = tri_idx + 1;

    tri_coord(tri_idx,1,:) = squeeze(coords(8,jd+1,:));
    tri_coord(tri_idx,2,:) = squeeze(coords(9,jn,:));
    tri_coord(tri_idx,3,:) = squeeze(coords(8,jdn,:));
    
    tri_left(tri_idx,1,:) = squeeze(lefts(8,jd+1,:));
    tri_left(tri_idx,2,:) = squeeze(lefts(9,jn,:));
    tri_left(tri_idx,3,:) = squeeze(lefts(8,jdn,:));

    tri_right(tri_idx,1,:) = squeeze(rights(8,jd+1,:));
    tri_right(tri_idx,2,:) = squeeze(rights(9,jn,:));
    tri_right(tri_idx,3,:) = squeeze(rights(8,jdn,:));

    tri_idx = tri_idx + 1;
end

% top fan
for j=1:6
    jn = mod(j,6)+1;
    
    tri_coord(tri_idx,1,:) = squeeze(coords(9,j,:));
    tri_coord(tri_idx,2,:) = squeeze(coords(10,1,:));
    tri_coord(tri_idx,3,:) = squeeze(coords(9,jn,:));
    
    tri_left(tri_idx,1,:) = squeeze(lefts(9,j,:));
    tri_left(tri_idx,2,:) = squeeze(lefts(10,1,:));
    tri_left(tri_idx,3,:) = squeeze(lefts(9,jn,:));

    tri_right(tri_idx,1,:) = squeeze(rights(9,j,:));
    tri_right(tri_idx,2,:) = squeeze(rights(10,1,:));
    tri_right(tri_idx,3,:) = squeeze(rights(9,jn,:));
    
    tri_idx = tri_idx + 1;    
end

% bottom face
for j=1:11
    
    tri_coord(tri_idx,1,:) = squeeze(coords(1,j,:));
    tri_coord(tri_idx,2,:) = squeeze(coords(1,j+1,:));
    tri_coord(tri_idx,3,:) = squeeze(coords(1,24-j,:));

    tri_left(tri_idx,1,:) = squeeze(lefts(1,j,:));
    tri_left(tri_idx,2,:) = squeeze(lefts(1,j+1,:));
    tri_left(tri_idx,3,:) = squeeze(lefts(1,24-j,:));

    tri_right(tri_idx,1,:) = squeeze(rights(1,j,:));
    tri_right(tri_idx,2,:) = squeeze(rights(1,j+1,:));
    tri_right(tri_idx,3,:) = squeeze(rights(1,24-j,:));
    
    tri_idx = tri_idx + 1;    

    tri_coord(tri_idx,1,:) = squeeze(coords(1,j,:));
    tri_coord(tri_idx,2,:) = squeeze(coords(1,24-j,:));
    tri_coord(tri_idx,3,:) = squeeze(coords(1,24-j+1,:));

    tri_left(tri_idx,1,:) = squeeze(lefts(1,j,:));
    tri_left(tri_idx,2,:) = squeeze(lefts(1,24-j,:));
    tri_left(tri_idx,3,:) = squeeze(lefts(1,24-j+1,:));

    tri_right(tri_idx,1,:) = squeeze(rights(1,j,:));
    tri_right(tri_idx,2,:) = squeeze(rights(1,24-j,:));
    tri_right(tri_idx,3,:) = squeeze(rights(1,24-j+1,:));
    
    tri_idx = tri_idx + 1;    

end

end