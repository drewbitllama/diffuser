function [ tri_coord, tri_left, tri_right, hrtf_radius, head_width, left_ear, right_ear ] = ...
    make_mit_triangles( )

addpath('./API_MO');
SOFAstart();
obj = SOFAload('./database/mit/mit_kemar_normal_pinna.sofa');
p = obj.SourcePosition;

origin = obj.EmitterPosition;
ear_distances = obj.ReceiverPosition;

head_width = sqrt(sum((ear_distances(1,:) - ear_distances(2,:)).^2));
left = sqrt(sum((ear_distances(1,:) - origin).^2));
right = sqrt(sum((ear_distances(2,:) - origin).^2));

left_ear = [-left 0 0].';
right_ear = [right 0 0].';
hrtf_radius = mean(obj.SourcePosition(:,3));

ele = -40:10:90;
azi = [56 60 72 72 72 72 72 60 56 45 36 24 12 1];

coords = zeros(length(ele),max(azi),3);
lefts = zeros(length(ele),max(azi),512);
rights = zeros(length(ele),max(azi),512);

for i=1:length(ele)
    azi_deg = linspace(0,360,azi(i)+1);
    azi_deg = azi_deg(1:end-1);
    for j=1:length(azi_deg)
        match = 0;
        for k=1:size(p,1)
            if abs(p(k,1) - azi_deg(j)) < 1e-5 && p(k,2) == ele(i)
                match = k;
                break;
            end
        end
        if match == 0
            error('no match found!');
        end
        l = squeeze(obj.Data.IR(match,1,:));
        r = squeeze(obj.Data.IR(match,2,:));
        lefts(i,j,:) = l;
        rights(i,j,:) = r;
     end
end

tri_coord = zeros(1,3,3);
tri_left = zeros(1,3,512);
tri_right = zeros(1,3,512);

tri_idx = 1;

points = zeros(sum(azi),3);
idx = 1;
for i=1:length(ele)

    phi = ele(i)/180*pi;
    theta = linspace(0,2*pi,azi(i)+1);
    theta = theta(1:end-1);
    
    for j=1:length(theta)
        v = [-sin(theta(j))*cos(phi) sin(phi) cos(theta(j))*cos(phi)];
        points(idx,:) = v;
        coords(i,j,:) = v;
        idx = idx + 1;
    end
    
end

% regular faces 
for ele_idx=1:12;
    i = 1;
    j = 1;
    if (azi(ele_idx) < azi(ele_idx+1))
        slow = azi(ele_idx);
        slow_idx = ele_idx;
        fast = azi(ele_idx+1);
        fast_idx = ele_idx+1;
    else
        slow = azi(ele_idx+1);
        slow_idx = ele_idx+1;
        fast = azi(ele_idx);
        fast_idx = ele_idx;
    end
    a = linspace(0,1,slow+1);
    b = linspace(0,1,fast+1);
    t = 0;
    list = [];
    order = [];
    aziCoord = [];
    eleCoord = [];
    while (i <= slow+1 && j <= fast+1)
        if (a(i) < b(j))
            list = [list; squeeze(coords(slow_idx,mod((i-1),slow)+1,:))'];
            aziCoord = [aziCoord; mod((i-1),slow)+1];
            eleCoord = [eleCoord; slow_idx];
            order = [order; 1];
            i = i + 1;
        else
            list = [list; squeeze(coords(fast_idx,mod((j-1),fast)+1,:))'];
            aziCoord = [aziCoord; mod((j-1),fast)+1];
            eleCoord = [eleCoord; fast_idx];
            order = [order; 2];
            j = j + 1;
        end
    end
    if (i > slow+1)
        list = [list; squeeze(coords(fast_idx,1,:))'];
        aziCoord = [aziCoord; 1];
        eleCoord = [eleCoord; fast_idx];
        order = [order; 2];
    else
        list = [list; squeeze(coords(slow_idx,1,:))'];
        aziCoord = [aziCoord; 1];
        eleCoord = [eleCoord; slow_idx];
        order = [order; 1];
    end

    for i=3:length(list)
        if (order(i-2) == order(i-1))        
            tri_coord(tri_idx,1,:) = list(i-3,:);
            tri_coord(tri_idx,2,:) = list(i-1,:);
            tri_coord(tri_idx,3,:) = list(i,:);
            
            tri_left(tri_idx,1,:) = squeeze(lefts(eleCoord(i-3),aziCoord(i-3),:));
            tri_left(tri_idx,2,:) = squeeze(lefts(eleCoord(i-1),aziCoord(i-1),:));
            tri_left(tri_idx,3,:) = squeeze(lefts(eleCoord(i),aziCoord(i),:));
            
            tri_right(tri_idx,1,:) = squeeze(rights(eleCoord(i-3),aziCoord(i-3),:));
            tri_right(tri_idx,2,:) = squeeze(rights(eleCoord(i-1),aziCoord(i-1),:));
            tri_right(tri_idx,3,:) = squeeze(rights(eleCoord(i),aziCoord(i),:));
        else
            tri_coord(tri_idx,1,:) = list(i-2,:);
            tri_coord(tri_idx,2,:) = list(i-1,:);
            tri_coord(tri_idx,3,:) = list(i,:);
            
            tri_left(tri_idx,1,:) = squeeze(lefts(eleCoord(i-2),aziCoord(i-2),:));
            tri_left(tri_idx,2,:) = squeeze(lefts(eleCoord(i-1),aziCoord(i-1),:));
            tri_left(tri_idx,3,:) = squeeze(lefts(eleCoord(i),aziCoord(i),:));
            
            tri_right(tri_idx,1,:) = squeeze(rights(eleCoord(i-2),aziCoord(i-2),:));
            tri_right(tri_idx,2,:) = squeeze(rights(eleCoord(i-1),aziCoord(i-1),:));
            tri_right(tri_idx,3,:) = squeeze(rights(eleCoord(i),aziCoord(i),:));
        end
        tri_idx = tri_idx + 1;
    end

end

% top faces (triangle fan)
for i=1:azi(end-1)
    in = mod(i,azi(end-1))+1;
    tri_coord(tri_idx,1,:) = squeeze(coords(13,i,:))';
    tri_coord(tri_idx,2,:) = squeeze(coords(14,1,:))';
    tri_coord(tri_idx,3,:) = squeeze(coords(13,in,:))';
    
    tri_left(tri_idx,1,:) = squeeze(lefts(13,i,:));
    tri_left(tri_idx,2,:) = squeeze(lefts(14,i,:));
    tri_left(tri_idx,3,:) = squeeze(lefts(13,in,:));
    
    tri_right(tri_idx,1,:) = squeeze(rights(13,i,:));
    tri_right(tri_idx,2,:) = squeeze(rights(14,i,:));
    tri_right(tri_idx,3,:) = squeeze(rights(13,in,:));
    
    tri_idx = tri_idx + 1;
end

% bottom faces
for i=1:azi(1)/2-1
    tri_coord(tri_idx,1,:) = squeeze(coords(1,i,:))';
    tri_coord(tri_idx,2,:) = squeeze(coords(1,i+1,:))';
    tri_coord(tri_idx,3,:) = squeeze(coords(1,azi(1)-i,:))';
    
    tri_left(tri_idx,1,:) = squeeze(lefts(1,i,:));
    tri_left(tri_idx,2,:) = squeeze(lefts(1,i+1,:));
    tri_left(tri_idx,3,:) = squeeze(lefts(1,azi(1)-i,:));
    
    tri_right(tri_idx,1,:) = squeeze(rights(1,i,:));
    tri_right(tri_idx,2,:) = squeeze(rights(1,i+1,:));
    tri_right(tri_idx,3,:) = squeeze(rights(1,azi(1)-i,:));
    
    tri_idx = tri_idx + 1;
    
    tri_coord(tri_idx,1,:) = squeeze(coords(1,i,:))';
    tri_coord(tri_idx,2,:) = squeeze(coords(1,azi(1)-i,:))';
    tri_coord(tri_idx,3,:) = squeeze(coords(1,azi(1)-i+1,:))';
    
    tri_left(tri_idx,1,:) = squeeze(lefts(1,i,:));
    tri_left(tri_idx,2,:) = squeeze(lefts(1,azi(1)-i,:));
    tri_left(tri_idx,3,:) = squeeze(lefts(1,azi(1)-i+1,:));
    
    tri_right(tri_idx,1,:) = squeeze(rights(1,i,:));
    tri_right(tri_idx,2,:) = squeeze(rights(1,azi(1)-i,:));
    tri_right(tri_idx,3,:) = squeeze(rights(1,azi(1)-i+1,:));
    
    tri_idx = tri_idx + 1;
end

end