function [ tri_coord, tri_left, tri_right, hrtf_radius, head_width, left_ear, right_ear ] = make_scut_triangles( desired_radius )

hrtf_radius = desired_radius;

addpath('./API_MO');
SOFAstart();
obj = SOFAload('./database/scut/SCUT_KEMAR_radius_all.sofa');

origin = obj.EmitterPosition;
ear_distances = obj.ReceiverPosition;

head_width = sqrt(sum((ear_distances(1,:) - ear_distances(2,:)).^2));
left = sqrt(sum((ear_distances(1,:) - origin).^2));
right = sqrt(sum((ear_distances(2,:) - origin).^2));

left_ear = [-left 0 0].';
right_ear = [right 0 0].';

angles = [-30 72; -15 72; 0 72; 15 72; 30 72; 45 72; 60 36; 75 24; 90 1];

coords = zeros(1,1,3);
lefts = zeros(1,1,512);
rights = zeros(1,1,512);

for i=1:size(angles,1)
    phi = angles(i,1)/180*pi;
    azi = linspace(0,360,angles(i,2)+1);
    azi = azi(1:end-1);
    theta = linspace(0,2*pi,angles(i,2)+1);
    theta = theta(1:end-1);
    for j=1:length(theta)
        v = [-sin(theta(j))*cos(phi) sin(phi) cos(theta(j))*cos(phi)];
        coords(i,j,:) = v;
        idx = 0;
        for k=1:size(obj.SourcePosition,1)
            if angles(i,1) == obj.SourcePosition(k,2) && azi(j) == obj.SourcePosition(k,1) && abs(desired_radius - obj.SourcePosition(k,3)) < eps
                idx = k;
            end
        end
        if idx == 0
            error('position not found in SCUT set');
        end
        lefts(i,j,:) = obj.Data.IR(idx,1,:);
        rights(i,j,:) = obj.Data.IR(idx,2,:);
    end
end

tri_coord = zeros(1,3,3);
tri_left = zeros(1,3,512);
tri_right = zeros(1,3,512);

% first 5 layers from bottom
tri_idx = 1;
for i=1:5
    for j=1:72
        jn = mod(j,72)+1;
        
        tri_coord(tri_idx,1,:) = squeeze(coords(i,j,:));
        tri_coord(tri_idx,2,:) = squeeze(coords(i+1,j,:));
        tri_coord(tri_idx,3,:) = squeeze(coords(i,jn,:));

        tri_left(tri_idx,1,:) = squeeze(lefts(i,j,:));
        tri_left(tri_idx,2,:) = squeeze(lefts(i+1,j,:));
        tri_left(tri_idx,3,:) = squeeze(lefts(i,jn,:));
        
        tri_right(tri_idx,1,:) = squeeze(rights(i,j,:));
        tri_right(tri_idx,2,:) = squeeze(rights(i+1,j,:));
        tri_right(tri_idx,3,:) = squeeze(rights(i,jn,:));
        
        tri_idx = tri_idx + 1;
        
        tri_coord(tri_idx,1,:) = squeeze(coords(i+1,j,:));
        tri_coord(tri_idx,2,:) = squeeze(coords(i,jn,:));
        tri_coord(tri_idx,3,:) = squeeze(coords(i+1,jn,:));
        
        tri_left(tri_idx,1,:) = squeeze(lefts(i+1,j,:));
        tri_left(tri_idx,2,:) = squeeze(lefts(i,jn,:));
        tri_left(tri_idx,3,:) = squeeze(lefts(i+1,jn,:));
        
        tri_right(tri_idx,1,:) = squeeze(rights(i+1,j,:));
        tri_right(tri_idx,2,:) = squeeze(rights(i,jn,:));
        tri_right(tri_idx,3,:) = squeeze(rights(i+1,jn,:));

        tri_idx = tri_idx + 1;
    
    end
end

% 45 to 60
for j=1:36
    jn = mod(j,36)+1;
    jd = (j-1)*2+1;
    jdn = mod(jd,72)+1;
    jdnn = mod(jdn,72)+1;
    
    tri_coord(tri_idx,1,:) = squeeze(coords(6,jd,:));
    tri_coord(tri_idx,2,:) = squeeze(coords(7,j,:));
    tri_coord(tri_idx,3,:) = squeeze(coords(6,jdn,:));
    
    tri_left(tri_idx,1,:) = squeeze(lefts(6,jd,:));
    tri_left(tri_idx,2,:) = squeeze(lefts(7,j,:));
    tri_left(tri_idx,3,:) = squeeze(lefts(6,jdn,:));

    tri_right(tri_idx,1,:) = squeeze(rights(6,jd,:));
    tri_right(tri_idx,2,:) = squeeze(rights(7,j,:));
    tri_right(tri_idx,3,:) = squeeze(rights(6,jdn,:));

    tri_idx = tri_idx + 1;

    tri_coord(tri_idx,1,:) = squeeze(coords(7,j,:));
    tri_coord(tri_idx,2,:) = squeeze(coords(6,jdn,:));
    tri_coord(tri_idx,3,:) = squeeze(coords(7,jn,:));

    tri_left(tri_idx,1,:) = squeeze(lefts(7,j,:));
    tri_left(tri_idx,2,:) = squeeze(lefts(6,jdn,:));
    tri_left(tri_idx,3,:) = squeeze(lefts(7,jn,:));

    tri_right(tri_idx,1,:) = squeeze(rights(7,j,:));
    tri_right(tri_idx,2,:) = squeeze(rights(6,jdn,:));
    tri_right(tri_idx,3,:) = squeeze(rights(7,jn,:));

    tri_idx = tri_idx + 1;

    tri_coord(tri_idx,1,:) = squeeze(coords(6,jdn,:));
    tri_coord(tri_idx,2,:) = squeeze(coords(7,jn,:));
    tri_coord(tri_idx,3,:) = squeeze(coords(6,jdnn,:));

    tri_left(tri_idx,1,:) = squeeze(lefts(6,jdn,:));
    tri_left(tri_idx,2,:) = squeeze(lefts(7,jn,:));
    tri_left(tri_idx,3,:) = squeeze(lefts(6,jdnn,:));

    tri_right(tri_idx,1,:) = squeeze(rights(6,jdn,:));
    tri_right(tri_idx,2,:) = squeeze(rights(7,jn,:));
    tri_right(tri_idx,3,:) = squeeze(rights(6,jdnn,:));

    tri_idx = tri_idx + 1;
end

% 60 to 75
j = 1;
jd = 1;
for jidx=1:12
    jn = mod(j,24)+1;
    jnn = mod(jn,24)+1;

    jdn = mod(jd,36)+1;
    jdnn = mod(jdn,36)+1;
    jdnnn = mod(jdnn,36)+1;
    
    tri_coord(tri_idx,1,:) = squeeze(coords(7,jd,:));
    tri_coord(tri_idx,2,:) = squeeze(coords(8,j,:));
    tri_coord(tri_idx,3,:) = squeeze(coords(7,jdn,:));
    
    tri_left(tri_idx,1,:) = squeeze(lefts(7,jd,:));
    tri_left(tri_idx,2,:) = squeeze(lefts(8,j,:));
    tri_left(tri_idx,3,:) = squeeze(lefts(7,jdn,:));

    tri_right(tri_idx,1,:) = squeeze(rights(7,jd,:));
    tri_right(tri_idx,2,:) = squeeze(rights(8,j,:));
    tri_right(tri_idx,3,:) = squeeze(rights(7,jdn,:));

    tri_idx = tri_idx + 1;

    tri_coord(tri_idx,1,:) = squeeze(coords(8,j,:));
    tri_coord(tri_idx,2,:) = squeeze(coords(7,jdn,:));
    tri_coord(tri_idx,3,:) = squeeze(coords(8,jn,:));
    
    tri_left(tri_idx,1,:) = squeeze(lefts(8,j,:));
    tri_left(tri_idx,2,:) = squeeze(lefts(7,jdn,:));
    tri_left(tri_idx,3,:) = squeeze(lefts(8,jn,:));

    tri_right(tri_idx,1,:) = squeeze(rights(8,j,:));
    tri_right(tri_idx,2,:) = squeeze(rights(7,jdn,:));
    tri_right(tri_idx,3,:) = squeeze(rights(8,jn,:));

    tri_idx = tri_idx + 1;

    tri_coord(tri_idx,1,:) = squeeze(coords(7,jdn,:));
    tri_coord(tri_idx,2,:) = squeeze(coords(8,jn,:));
    tri_coord(tri_idx,3,:) = squeeze(coords(7,jdnn,:));
    
    tri_left(tri_idx,1,:) = squeeze(lefts(7,jdn,:));
    tri_left(tri_idx,2,:) = squeeze(lefts(8,jn,:));
    tri_left(tri_idx,3,:) = squeeze(lefts(7,jdnn,:));

    tri_right(tri_idx,1,:) = squeeze(rights(7,jdn,:));
    tri_right(tri_idx,2,:) = squeeze(rights(8,jn,:));
    tri_right(tri_idx,3,:) = squeeze(rights(7,jdnn,:));

    tri_idx = tri_idx + 1;

    tri_coord(tri_idx,1,:) = squeeze(coords(8,jn,:));
    tri_coord(tri_idx,2,:) = squeeze(coords(7,jdnn,:));
    tri_coord(tri_idx,3,:) = squeeze(coords(8,jnn,:));
    
    tri_left(tri_idx,1,:) = squeeze(lefts(8,jn,:));
    tri_left(tri_idx,2,:) = squeeze(lefts(7,jdnn,:));
    tri_left(tri_idx,3,:) = squeeze(lefts(8,jnn,:));

    tri_right(tri_idx,1,:) = squeeze(rights(8,jn,:));
    tri_right(tri_idx,2,:) = squeeze(rights(7,jdnn,:));
    tri_right(tri_idx,3,:) = squeeze(rights(8,jnn,:));

    tri_idx = tri_idx + 1;

    tri_coord(tri_idx,1,:) = squeeze(coords(7,jdnn,:));
    tri_coord(tri_idx,2,:) = squeeze(coords(8,jnn,:));
    tri_coord(tri_idx,3,:) = squeeze(coords(7,jdnnn,:));
    
    tri_left(tri_idx,1,:) = squeeze(lefts(7,jdnn,:));
    tri_left(tri_idx,2,:) = squeeze(lefts(8,jnn,:));
    tri_left(tri_idx,3,:) = squeeze(lefts(7,jdnnn,:));

    tri_right(tri_idx,1,:) = squeeze(rights(7,jdnn,:));
    tri_right(tri_idx,2,:) = squeeze(rights(8,jnn,:));
    tri_right(tri_idx,3,:) = squeeze(rights(7,jdnnn,:));

    tri_idx = tri_idx + 1;

    j = mod(j + 1, 24) + 1;
    jd = mod(jd + 2, 36) + 1;
end

% fan (75 to 90)
for j=1:24
    jn = mod(j,24)+1;
    
    tri_coord(tri_idx,1,:) = squeeze(coords(8,j,:));
    tri_coord(tri_idx,2,:) = squeeze(coords(9,1,:));
    tri_coord(tri_idx,3,:) = squeeze(coords(8,jn,:));
    
    tri_left(tri_idx,1,:) = squeeze(lefts(8,j,:));
    tri_left(tri_idx,2,:) = squeeze(lefts(9,1,:));
    tri_left(tri_idx,3,:) = squeeze(lefts(8,jn,:));

    tri_right(tri_idx,1,:) = squeeze(rights(8,j,:));
    tri_right(tri_idx,2,:) = squeeze(rights(9,1,:));
    tri_right(tri_idx,3,:) = squeeze(rights(8,jn,:));

    tri_idx = tri_idx + 1;
    
end    

% bottom floor
for j=1:36
    jn = mod(j,72)+1;
    jd = 72-j+1;
    jdn = jd-1;
    
    tri_coord(tri_idx,1,:) = squeeze(coords(1,j,:));
    tri_coord(tri_idx,2,:) = squeeze(coords(1,jn,:));
    tri_coord(tri_idx,3,:) = squeeze(coords(1,jd,:));
    
    tri_left(tri_idx,1,:) = squeeze(lefts(1,j,:));
    tri_left(tri_idx,2,:) = squeeze(lefts(1,jn,:));
    tri_left(tri_idx,3,:) = squeeze(lefts(1,jd,:));

    tri_right(tri_idx,1,:) = squeeze(rights(1,j,:));
    tri_right(tri_idx,2,:) = squeeze(rights(1,jn,:));
    tri_right(tri_idx,3,:) = squeeze(rights(1,jd,:));

    tri_idx = tri_idx + 1;
    
    tri_coord(tri_idx,1,:) = squeeze(coords(1,jn,:));
    tri_coord(tri_idx,2,:) = squeeze(coords(1,jd,:));
    tri_coord(tri_idx,3,:) = squeeze(coords(1,jdn,:));
    
    tri_left(tri_idx,1,:) = squeeze(lefts(1,jn,:));
    tri_left(tri_idx,2,:) = squeeze(lefts(1,jd,:));
    tri_left(tri_idx,3,:) = squeeze(lefts(1,jdn,:));

    tri_right(tri_idx,1,:) = squeeze(rights(1,jn,:));
    tri_right(tri_idx,2,:) = squeeze(rights(1,jd,:));
    tri_right(tri_idx,3,:) = squeeze(rights(1,jdn,:));

    tri_idx = tri_idx + 1;

end

% end
