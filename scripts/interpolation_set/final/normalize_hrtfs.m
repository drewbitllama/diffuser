function [ tri_left_norm, tri_right_norm ] = ...
    normalize_hrtfs( tri_coord, tri_left, tri_right, desired_db_max, hrtf_radius, left_ear, right_ear, distance_alpha )

% correct for distance
for i=1:size(tri_coord,1)
    for j=1:3
        left_dist = sqrt(sum((squeeze(tri_coord(i,j,:))*hrtf_radius - left_ear).^2));
        right_dist = sqrt(sum((squeeze(tri_coord(i,j,:))*hrtf_radius - right_ear).^2));
        tri_left(i,j,:) = squeeze(tri_left(i,j,:)).*left_dist.^distance_alpha;
        tri_right(i,j,:) = squeeze(tri_right(i,j,:)).*right_dist.^distance_alpha;
    end
end

% normalize set based on max dB present
max_left_rms = 0;
max_right_rms = 0;

for i=1:size(tri_left,1)
    for j=1:3
        left_rms = max(abs(fft(squeeze(tri_left(i,j,:)))));
        if left_rms > max_left_rms
            max_left_rms = left_rms;
        end
    end
end

for i=1:size(tri_right,1)
    for j=1:3
        right_rms = max(abs(fft(squeeze(tri_right(i,j,:)))));
        if right_rms > max_right_rms
            max_right_rms = right_rms;
        end
    end
end

tri_left_norm = tri_left ./ max_left_rms .* 10.^(desired_db_max./20);
tri_right_norm = tri_right ./ max_right_rms .* 10.^(desired_db_max./20);



end

