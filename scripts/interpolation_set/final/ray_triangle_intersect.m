function [ bool, w ] = ray_triangle_intersect( o, d, v1, v2, v3 )

% Molle - Trombore algorithm

eps = 1e-6;

bool = 0;
w = [0 0 0];

e1 = v2 - v1;
e2 = v3 - v1;

p = cross(d,e2);
det = dot(e1,p);
if (det > -eps && det < eps)
    return;
end

t = o - v1;
w(2) = dot(t,p) / det;
if w(2) < -eps || w(2) > 1+eps
    return;
elseif w(2) < 0
    w(2) = 0;
elseif w(2) > 1
    w(2) = 1;
end

q = cross(t,e1);
w(3) = dot(d,q) / det;
if w(3) < -eps || w(2) + w(3) > 1+eps
    return;
elseif w(3) < 0
    w(3) = 0;
elseif w(3) > 1
    w(3) = 1;
end

t = dot(e2,q) / det;
if (t > eps)
    bool = 1;
else
    return;
end

w(1) = 1 - w(2) - w(3);
if w(1) < 0
    w(1) = 0;
elseif w(1) > 1
    w(1) = 1;
end

end