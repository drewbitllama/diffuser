function [ y, del ] = remove_prering( x, peak_thresh, crest_thresh, use_peak_for_prering, prepeak_samples )


x_norm = x ./ max(abs(x));
[~,locs0] = findpeaks(x_norm);
[~,locs1] = findpeaks(-x_norm);
locs = [locs0(:); locs1(:)];
locs = sort(locs,'ascend');
if isempty(locs)
    y = x(:);
    return;
end

for idx=1:length(locs)
    if abs(x_norm(locs(idx))) > peak_thresh
        break;
    end
end

min_onset = 1;
if idx > 1
    min_onset = locs(idx-1);
end

for onset=locs(idx):-1:min_onset
    if abs(x_norm(onset)) < crest_thresh
        break;
    end
end

if (use_peak_for_prering == 1)
    y = x(max(locs(idx)-prepeak_samples,1):end);
    del = max(locs(idx)-prepeak_samples,1) - 1;
else
    y = x(max(onset-prepeak_samples,1):end);
    del = max(onset-prepeak_samples,1) - 1;
end

y = [y(:); zeros(length(x) - length(y), 1)];

end

