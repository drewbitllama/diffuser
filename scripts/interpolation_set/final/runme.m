%% ========================================================================
% CIPIC/MIT/LISTEN interpolation tool
%==========================================================================

% supplemental scripts:
%   arc_interp.m
%   display_triangle_mesh.m
%   fracshift.m
%   get_hrtf_data_from_responses.m
%   get_interp_fir_from_weights.m
%   get_mayerfft_from_firs.m
%   get_points.m
%   get_triangle_weights_for_points.m
%   get_triangles.m
%   intshift.m
%   make_cipic_triangles.m
%   make_mit_triangles.m
%   make_listen_triangles.m
%   make_scut_triangles.m
%   minphase.m
%   normalize_hrtfs.m
%   ray_triangle_intersect.m
%   remove_prering.m
%   truncate_hrtfs.m
%   write_interps_to_file.m

% cipic_subjects = [3 8 9 10 11 12 15 17 18 19 20 21 27 28 33 40 44 48 50 
% 51 58 59 60 61 65 119 124 126 127 131 133 134 135 137 147 148 152 153 154 
% 155 156 158 162 163 165];
% listen_subjects = [1002 1003 1004 1005 1006 1007 1008 1009 1012 1013 1014
% 1015 1016 1017 1018 1020 1021 1022 1023 1025 1026 1028 1029 1030 1031 
% 1032 1033 1034 1037 1038 1039 1040 1041 1042 1043 1044 1045 1046 1047
% 1048 1049 1050 1051 1052 1053 1054 1055 1056 1057 1058 1059];
% scut_radi = [0.2 0.25 0.3 0.4 0.5 0.6 0.7 0.8 0.9 1]
clearvars;
clc;

subject = 27;
desired_hrtf_radius = 1;
hrtf_database = 'cipic'; % [allowed: 'cipic', 'listen', 'mit', 'scut']
interp_mode = 'linear'; % [allowed: 'linear', 'minphase', 'arc']

% constants
c = 325;
fs = 44100;

% desired fft file outputs
fft_sizes = [128];

%% ========================================================================
% signal settings
truncate_method = 0; % 0 = use distance-based truncate, 1 = use peak-finding truncate

% distance-based truncation settings
dropped_samples = 105; % # of samples to drop if dist. truncating
distance_alpha = 1; % typically 1, can be slightly more or less if normalization isn't balanced between ears

% peak-finding truncation settings
use_peak_for_prering = 0; % 0 = use base, 1 = use peak (base seems to be preferred)
prering_samples = 10; % samples to keep before base/peak

% normalization settings
desired_db_max = 0; % 0db is default, use higher if precision is lost

% display instead of render
display_hrtf_waveforms = 0; % display HRTF set after truncation/normalization

% add linear fade-in/fade-out to rendered hrtfs
fadein_samples = 0; % # of samples to fade-in with (linear fade)
fadeout_samples = 0; % # of samples to fade-out with (linear fade)

%% ========================================================================
% load CIPIC/LISTEN/KEMAR/SCUT/etc triangles
disp('loading database and creating triangle mesh...');
tic;
[ tri_coord, tri_left, tri_right, hrtf_radius, head_width, left_ear, right_ear ] = ...
    get_triangles(hrtf_database, subject, desired_hrtf_radius);
toc;

%% ========================================================================
% truncate respones based on distance from coords to ears
% or use peak-detection to crop
disp('truncating transforms...');
tic;
[ tri_left_trunc, tri_right_trunc, dels ] = ...
    truncate_hrtfs(tri_left, tri_right, truncate_method, use_peak_for_prering, ...
    prering_samples, c, tri_coord, hrtf_radius, left_ear, right_ear, dropped_samples, fs);
toc;

% dels_ring_l = [];
% dels_ring_r = [];
% for i=1:size(tri_coord,1)
%     for j=1:3
%         if abs(tri_coord(i,j,2)) < eps
%             dels_ring_l = [dels_ring_l squeeze(dels(i,j,1))];
%             dels_ring_r = [dels_ring_r squeeze(dels(i,j,2))];
%         end
%     end
% end
% 
% figure(2);
% plot(dels_ring_l);
% hold on;
% plot(dels_ring_r);
% hold off;
% return;

%% ========================================================================
% normalize hrtfs to desired dB max
disp('normalizing HRTFs...');
tic;
[ tri_left_norm, tri_right_norm ] = normalize_hrtfs(...
    tri_coord, tri_left_trunc, tri_right_trunc, desired_db_max, ...
    hrtf_radius, left_ear, right_ear, distance_alpha);
toc;

%% ========================================================================
% display truncated/normalized HRTFs
if display_hrtf_waveforms == 1
    for i=1:size(tri_coord,1)
        figure(1);
        subplot(3,2,1);
        plot(squeeze(tri_left_trunc(i,1,:)));
%         hold on;
%         plot(squeeze(tri_left(i,1,:)));
%         hold off;
        ylim([-1 1]);
        subplot(3,2,3);
        plot(squeeze(tri_left_trunc(i,2,:)));
%         hold on;
%         plot(squeeze(tri_left(i,2,:)));
%         hold off;
        ylim([-1 1]);
        subplot(3,2,5);
        plot(squeeze(tri_left_trunc(i,3,:)));
%         hold on;
%         plot(squeeze(tri_left(i,3,:)));
%         hold off;
        ylim([-1 1]);
        subplot(3,2,2);
        plot(squeeze(tri_right_trunc(i,1,:)));
%         hold on;
%         plot(squeeze(tri_right(i,1,:)));
%         hold off;
        ylim([-1 1]);
        subplot(3,2,4);
        plot(squeeze(tri_right_trunc(i,2,:)));
%         hold on;
%         plot(squeeze(tri_right(i,2,:)));
%         hold off;
        ylim([-1 1]);
        subplot(3,2,6);
        plot(squeeze(tri_right_trunc(i,3,:)));
%         hold on;
%         plot(squeeze(tri_right(i,3,:)));
%         hold off;
        ylim([-1 1]);
        pause(0.01);
    end
    return;
end

%% ========================================================================
% generate horizontal rings at various elevations
tic;
disp('generating point cloud...');
[ points, points_angle_indices ] = get_points();
toc;

%% ========================================================================
% get weights for points (use pre-made weights when possible)
disp('computing barycentric weights... (this might take awhile if not pre-baked)');
tic;
barycentric_filename = sprintf('barycentric_%s.mat',hrtf_database);
fid = fopen(barycentric_filename,'r');
fclose(fid);
if fid ~= 0
    load(barycentric_filename);
else
    % if we don't have it, do it manually and then save it for future
    [weights, triangle_indices] = ...
        get_triangle_weights_for_points(points, tri_coord);
    save(barycentric_filename,'weights','triangle_indices');
end
toc;

%% ========================================================================
% compute interpolations
disp('generate interpolations...');
tic;
[ fir_left, fir_right ] = get_interp_fir_from_weights(...
    weights, triangle_indices, tri_left_norm, tri_right_norm, interp_mode);
toc;

%% ========================================================================
% create file and write output
disp('compute ffts and write output to file...');
tic;
write_interps_to_file( ...
    hrtf_database, subject, interp_mode, hrtf_radius, head_width, ...
    fft_sizes, 'single', fir_left, fir_right, ...
    points_angle_indices, fadein_samples, fadeout_samples)
toc;
