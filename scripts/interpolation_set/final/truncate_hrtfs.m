function [ tri_left_trunc, tri_right_trunc, dels ] = ...
    truncate_hrtfs( tri_left, tri_right, use_prering, use_peak_for_prering, ...
    prering_samples, c, tri_coord, hrtf_radius, left_ear, right_ear, dropped_samples, fs )

tri_left_trunc = zeros(size(tri_left,1),size(tri_left,2),size(tri_left,3));
tri_right_trunc = zeros(size(tri_right,1),size(tri_right,2),size(tri_right,3));

dels = zeros(size(tri_left,1),size(tri_left,2),2);

if use_prering == 1
    % truncate using peak finding
    for i=1:size(tri_left,1)    
        for j=1:3

            l = squeeze(tri_left(i,j,:));
            r = squeeze(tri_right(i,j,:));

            % throw away prering
            peak_thresh = 10^(-12/20);
            crest_thresh = 10^(-24/20);

            [l, left_delay] = remove_prering(l,peak_thresh,crest_thresh,use_peak_for_prering,prering_samples);
            [r, right_delay] = remove_prering(r,peak_thresh,crest_thresh,use_peak_for_prering,prering_samples);

            dels(i,j,1) = left_delay;
            dels(i,j,2) = right_delay;

            tri_left_trunc(i,j,:) = l;
            tri_right_trunc(i,j,:) = r;
        end
    end
else
    % truncate using distance delay    
    for i=1:size(tri_left,1)    
        for j=1:3

            l = squeeze(tri_left(i,j,:));
            r = squeeze(tri_right(i,j,:));

            left_delay = sqrt(sum((squeeze(tri_coord(i,j,:)) .* hrtf_radius - left_ear).^2)) / c * fs;
            right_delay = sqrt(sum((squeeze(tri_coord(i,j,:)) .* hrtf_radius - right_ear).^2)) / c * fs;
            
            dels(i,j,1) = left_delay;
            dels(i,j,2) = right_delay;
                        
            l = intshift(l,round(left_delay - dropped_samples));
            r = intshift(r,round(right_delay - dropped_samples));
            
            tri_left_trunc(i,j,:) = l;
            tri_right_trunc(i,j,:) = r;
        end
    end
    
end

end
