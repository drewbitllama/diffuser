clearvars;
clc;

addpath('./API_MO');
SOFAstart();
obj = SOFAload('./database/listen/irc_1002.sofa');
p = obj.SourcePosition;

ele = -40:10:90;
azi = [56 60 72 72 72 72 72 60 56 45 36 24 12 1];

for i=1:length(ele)
    azi_deg = linspace(0,360,azi(i)+1);
    azi_deg = azi_deg(1:end-1);
    for j=1:length(azi_deg)
        match = 0;
        for k=1:size(p,1)
            if abs(p(k,1) - azi_deg(j)) < 1e-5 && p(k,2) == ele(i)
                match = 1;
            end
        end
        if match == 0
            error('1');
        end
    end
end

pts = [];
for i=1:size(p,1)
    v = [-sin(p(i,1)/180*pi)*cos(p(i,2)/180*pi) sin(p(i,2)/180*pi) cos(p(i,1)/180*pi)*cos(p(i,2)/180*pi)];
    pts = [pts; v];
end

figure(1);
plot3(pts(:,1),pts(:,3),pts(:,2),'.','MarkerSize',8);
axis equal;
