clearvars;
clc;

addpath('./API_MO');
SOFAstart();
obj = SOFAload('./database/scut/SCUT_KEMAR_radius_all.sofa');
% obj = SOFAload('./database/listen/irc_1002.sofa');

load('IRC_1002_R_HRIR.mat');

idx = 79;
l0 = squeeze(obj.Data.IR(idx,1,:));
r0 = squeeze(obj.Data.IR(idx,2,:));
l1 = squeeze(l_hrir_S.content_m(idx,:)).';
r1 = squeeze(r_hrir_S.content_m(idx,:)).';

[l0_h,w] = freqz(l0,1,8192);
l0_h = 20*log10(abs(l0_h));
r0_h = 20*log10(abs(freqz(r0,1,8192)));

l1_h = 20*log10(abs(freqz(l1,1,8192)));
r1_h = 20*log10(abs(freqz(r1,1,8192)));

fs = 44100;
freqs = w./pi*fs/2;

figure(1);
subplot(2,1,1);
semilogx(freqs,l0_h);
hold on;
semilogx(freqs,l1_h);
hold off;
xlim([20 20000]);
ylim([-60 20]);
subplot(2,1,2);
semilogx(freqs,r0_h);
hold on;
semilogx(freqs,r1_h);
hold off;
xlim([20 20000]);
ylim([-60 20]);

% sum(abs(l1 - l0))
% 
% fs = 44100;
% noise_len = round(fs * 0.1);
% noise_burst = rand(noise_len,1)*2-1;
% noise_burst = linspace(1,0,noise_len).' .* noise_burst;
% 
% out_l0 = conv(noise_burst,l0);
% out_r0 = conv(noise_burst,r0);
% out_l1 = conv(noise_burst,l1);
% out_r1 = conv(noise_burst,r1);
% 
% out0 = [out_l0 out_r0];
% out1 = [out_l1 out_r1];
% 
% playblocking(audioplayer(out0,fs));
% playblocking(audioplayer(out1,fs));


% p = obj.SourcePosition;
% pts = [];
% for i=1:size(p,1)
%     v = [-sin(p(i,1)/180*pi)*cos(p(i,2)/180*pi) sin(p(i,2)/180*pi) cos(p(i,1)/180*pi)*cos(p(i,2)/180*pi)];
%     pts = [pts; v .* p(i,3)];
% end
% 
% fs = 44100;
% noise_len = round(fs * 0.1);
% noise_burst = rand(noise_len,1)*2-1;
% noise_burst = linspace(1,0,noise_len).' .* noise_burst;
% 
% figure(1);
% for i=1:size(p,1)
%     out_l = conv(noise_burst,squeeze(obj.Data.IR(i,1,:)));
%     out_r = conv(noise_burst,squeeze(obj.Data.IR(i,2,:)));
%     
%     plot3(pts(:,1),pts(:,3),pts(:,2),'b.','MarkerSize',8);
%     hold on;
%     plot3(pts(i,1),pts(i,3),pts(i,2),'r.','MarkerSize',16);
%     plot3([0 0],[0 1],[0 0],'k');
%     hold off;
%     axis('equal');
%     gcf.rotateOn = 'enable';
%     drawnow;
%     
%     out = [out_l out_r];
%     playblocking(audioplayer(out,fs));
% 
% end
