function [ ] = ...
    write_interps_to_file( ...
    hrtf_database, subject, interp_mode, hrtf_radius, head_width, ...
    fft_sizes, precision, fir_left, fir_right, ...
    points_angle_indices, fadein, fadeout)

for i=1:length(fft_sizes)
    
    if strcmp(hrtf_database,'cipic') || strcmp(hrtf_database,'listen')
        filename = sprintf('%s_%d_%s%d_r%g.dat',...
            hrtf_database,subject,interp_mode,fft_sizes(i),hrtf_radius);
    else
        filename = sprintf('%s_%s%d_r%g.dat',...
        hrtf_database,interp_mode,fft_sizes(i),hrtf_radius);
    end
    fid = fopen(filename,'wb');

    % hdb_name[8]
    fwrite(fid,hrtf_database(1:min(length(hrtf_database),8)),'char');
    for j=1:8-length(hrtf_database)
        fwrite(fid,' ','char');
    end

    % hdb_user
    fwrite(fid,subject,'int');

    % _reserved
    fwrite(fid,0,'int');

    % hdb_interp[8]
    fwrite(fid,interp_mode(1:min(length(interp_mode),8)),'char');
    for j=1:8-length(interp_mode)
        fwrite(fid,' ','char');
    end

    % hdb_headSize
    fwrite(fid,head_width,'double');

    % hdb_radius
    fwrite(fid,hrtf_radius,'double');

    % hdb_fft
    [fft_left, fft_right] = get_mayerfft_from_firs(fir_left, fir_right, fft_sizes(i), fadein, fadeout);
    [ hd_n90, hd_n75, hd_n60, hd_n45, hd_0, hd_45, hd_60, hd_75, hd_90 ] = ...
        get_hrtf_data_from_responses( fft_left, fft_right, points_angle_indices );
    fwrite(fid,hd_n90,precision);
    fwrite(fid,hd_n75,precision);
    fwrite(fid,hd_n60,precision);
    fwrite(fid,hd_n45,precision);
    fwrite(fid,hd_0,precision);
    fwrite(fid,hd_45,precision);
    fwrite(fid,hd_60,precision);
    fwrite(fid,hd_75,precision);
    fwrite(fid,hd_90,precision);

    fclose(fid);
    
end

end

