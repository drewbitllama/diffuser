function [ v ] = cipic2cart( azi, ele )

theta = azi/180*pi;
phi = ele/180*pi;

v = [0 0 1]';

Rx = [1 0 0; 0 cos(phi) sin(phi); 0 -sin(phi) cos(phi)];
Ry = [cos(theta) 0 sin(theta); 0 1 0; -sin(theta) 0 cos(theta)];

v = Rx * Ry * v;
v = v';

end

