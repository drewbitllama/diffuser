clearvars;
clc;

vert_rez = 37; % (37 steps are 5deg apart) # steps between 0 and 90 (hemisphere)
horz_rez = 360; % # steps between 0 and 360 (best if a multiple of 2)
min_horz_rez = 90;

filter_length = 200; % 200 is what is given in CIPIC

subjects = [3,8,9,10,11,12,15,17,18,19,20,21,27,28,33,40,44,48,50,51,58,59,...
    60,61,65,119,124,126,127,131,133,134,135,137,147,148,152,154,155,156,158,162,163,165];
subject = 3;

filename = sprintf('../CIPIC_hrtf_database/standard_hrir_database/subject_%03d/hrir_final.mat',subject);
load(filename);

[cipic_coords, cipic_indexes] = get_cipic_cartcoords();

ele = linspace(-90,90,vert_rez);
ele_radius = cos(ele./180.*pi);
ele_height = sin(ele./180.*pi);
ele_step = round(horz_rez ./ 2.^round(log2(1 ./ ele_radius)));
ele_step = ele_step .* (ele_step >= min_horz_rez) + min_horz_rez .* (ele_step < min_horz_rez);
ele_step(1) = 1; % 90-degs only has a single point
ele_step(end) = 1;

% generate interpolation fields
v = [];
for i=1:length(ele)
    azi = (0:(ele_step(i)-1))' ./ ele_step(i) .* 360;
    for j=1:length(azi)
        x = cos(azi(j)/180*pi)*cos(ele(i)/180*pi);
        y = sin(ele(i)/180*pi);
        z = sin(azi(j)/180*pi)*cos(ele(i)/180*pi);
        v = [v; x y z];
    end
end

hrtf_l = zeros(size(v,1),filter_length);
hrtf_r = zeros(size(v,1),filter_length);

% find 3 nearest points and use those as triangle (assume you are inside
% the triangle)
distances = zeros(size(cipic_coords,1),1);
for i=1:length(v)
    for j=1:length(distances)
        distances(j) = sqrt(sum((cipic_coords(j,:) - v(i,:)).^2, 2));
    end
    [~,idxs] = sort(distances,'ascend');
    v1 = cipic_coords(idxs(1),:);
    v2 = cipic_coords(idxs(2),:);
    v3 = cipic_coords(idxs(3),:); 
    
    % place point on triangle
    [v_bool, v_point] = triangle_intersection(v(i,:), -v(i,:), v1, v2, v3);
    if (v_bool == 0)
        % point doesn't intersect, just pick the nearest and complain
        w = [1 0 0];
    else
        w = get_barycentric_weights(v_point,v1,v2,v3);
    end
    
    % use weighted linear combination to produce interpolations
    azi1 = cipic_indexes(idxs(1),1);
    azi2 = cipic_indexes(idxs(2),1);
    azi3 = cipic_indexes(idxs(3),1);
    ele1 = cipic_indexes(idxs(1),2);
    ele2 = cipic_indexes(idxs(2),2);
    ele3 = cipic_indexes(idxs(3),2);
    
    % truncate points using onsets from CIPIC database
    hrtf_l1 = squeeze(hrir_l(azi1,ele1,round(OnL(azi1,ele1)):end));
    hrtf_l2 = squeeze(hrir_l(azi2,ele2,round(OnL(azi2,ele2)):end));
    hrtf_l3 = squeeze(hrir_l(azi3,ele3,round(OnL(azi3,ele3)):end));
    hrtf_r1 = squeeze(hrir_r(azi1,ele1,round(OnR(azi1,ele1)):end));
    hrtf_r2 = squeeze(hrir_r(azi2,ele2,round(OnR(azi2,ele2)):end));
    hrtf_r3 = squeeze(hrir_r(azi3,ele3,round(OnR(azi3,ele3)):end));
    
    % convert to minimum-phase (if requested)
    % ...
    
    % zero-pad truncated IRs
    hrtf_l1 = [hrtf_l1(:); zeros(filter_length - length(hrtf_l1), 1)];
    hrtf_l2 = [hrtf_l2(:); zeros(filter_length - length(hrtf_l2), 1)];
    hrtf_l3 = [hrtf_l3(:); zeros(filter_length - length(hrtf_l3), 1)];
    hrtf_r1 = [hrtf_r1(:); zeros(filter_length - length(hrtf_r1), 1)];
    hrtf_r2 = [hrtf_r2(:); zeros(filter_length - length(hrtf_r2), 1)];
    hrtf_r3 = [hrtf_r3(:); zeros(filter_length - length(hrtf_r3), 1)];
    
    % interpolate across the 3 possible hrtfs
    hrtf_ln = w(1)*hrtf_l1 + w(2)*hrtf_l2 + w(3)*hrtf_l3;
    hrtf_rn = w(1)*hrtf_r1 + w(2)*hrtf_r2 + w(3)*hrtf_r3;
    hrtf_lo = round(w(1)*OnL(azi1,ele1) + w(2)*OnL(azi2,ele2) + w(3)*OnL(azi3,ele3));
    hrtf_ro = round(w(1)*OnR(azi1,ele1) + w(2)*OnR(azi2,ele2) + w(3)*OnR(azi3,ele3));
    if (hrtf_lo < 0)
        hrtf_lo = 0;
    end
    if (hrtf_ro < 0)
        hrtf_ro = 0;
    end
    
    % add back offsets using interpolated values
    hrtf_ln = [zeros(round(hrtf_lo),1); hrtf_ln];
    hrtf_rn = [zeros(round(hrtf_ro),1); hrtf_rn];
    hrtf_ln = hrtf_ln(1:filter_length);
    hrtf_rn = hrtf_rn(1:filter_length);
    
    % add interpolated hrtf to set of IRs
    hrtf_l(i,:) = hrtf_ln;
    hrtf_r(i,:) = hrtf_rn;
    
%     if (w(1) < 0 || w(2) < 0 || w(3) < 0)
%         
%     else
%         
%     end

    fprintf('%d%% complete...\n',round(i/(size(v,1)-1)*100));
    
end

figure(1);
scatter3(v(:,1), v(:,3), v(:,2), '.');
xlim([-1 1]);
ylim([-1 1]);
zlim([-1 1]);
axis equal;
title('Interpolation Points');

% for i=1:size(hrtf_l,1)
%     figure(2);
%     subplot(1,2,1);
%     plot(hrtf_l(i,:));
%     xlim([1 filter_length]);
%     ylim([-1 1]);
%     subplot(1,2,2);
%     plot(hrtf_r(i,:));
%     xlim([1 filter_length]);
%     ylim([-1 1]);
%     pause(0.01);
% end

fs = 44100;
burst_len = round(fs*0.04);
out = zeros(burst_len * length(v), 2);
for i=1:size(hrtf_l,1)
    burst = rand(burst_len,1) * 2 - 1;
    l = conv(burst,hrtf_l(i,:));
    r = conv(burst,hrtf_r(i,:));
    idx_start = (i-1)*burst_len+1;
    idx_range = min(length(v)*burst_len - idx_start, burst_len);
    out(idx_start:idx_start+idx_range-1,1) = ...
        out(idx_start:idx_start+idx_range-1,1) + l(1:idx_range);
    out(idx_start:idx_start+idx_range-1,2) = ...
        out(idx_start:idx_start+idx_range-1,2) + r(1:idx_range);
end
audiowrite('sweep.wav',out ./ max(max(abs(out))),fs);

