function [ coords, indexes, lookups ] = get_cipic_cartcoords( )

azi = [-80 -65 -55 -45:5:45 55 65 80];
ele = -45 + 5.625*(0:49);
coords = [];
indexes = [];
lookups = zeros(length(azi),length(ele),3);

% convert CIPIC angles to cartesian coordinates
idx = 1;
for i=1:length(azi)
    for j=1:length(ele)
        coords = [coords; cipic2cart(azi(i),ele(j))];
        indexes = [indexes; i j];
        lookups(i,j,:) = [idx azi(i) ele(j)];
        idx = idx + 1;
    end
end

end
