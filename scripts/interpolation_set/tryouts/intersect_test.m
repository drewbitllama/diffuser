clearvars;
clc;

v1 = [0 1 0]';
v2 = [1 0 0]';
v3 = [1 1 0]';

theta0 = 45/180*pi;
theta1 = 22/180*pi;
Rz = [cos(theta0) sin(theta0) 0; -sin(theta0) cos(theta0) 0; 0 0 1;];
Rx = [1 0 0; 0 cos(theta1) sin(theta1); 0 -sin(theta1) cos(theta1)];
v1 = Rx * Rz * v1;
v2 = Rx * Rz * v2;
v3 = Rx * Rz * v3;

u = v2 - v1;
v = v3 - v1;
n = cross(u,v);

n0 = [1 0 0];

acos(dot(n,n0))/pi*180


% mult = 30;
% for i=0:mult
%     for j=0:mult
%         v = [j i 0]'/mult;
%         v = Rx * Rz * v;
%         
%         
%         T = [v1(1:2)' - v3(1:2)'; v2(1:2)' - v3(1:2)']';
%         w = T\(v(1:2)' - v3(1:2)')';
%         w(3) = 1 - sum(w);
%         
%         figure(1);
%         plot([v1(1) v2(1) v3(1) v1(1)],[v1(2) v2(2) v3(2) v1(2)]);
%         hold on;
%         
%         if (w(1) < 0 || w(2) < 0 || w(3) < 0)
%             plot(v(1),v(2),'r.','MarkerSize',30);
%         else
%             plot(v(1),v(2),'k.','MarkerSize',20);
%         end
%         hold off;
%         xlim([-2 2]);
%         ylim([-2 2]);
%         pause(0.01);    
%     end
% end