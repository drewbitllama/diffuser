function [ v_bool, v_out, u, v ] = triangle_intersection( o, d, v1, v2, v3 )

u = 0;
v = 0;
v_bool = 0;
v_out = 0 * v1;
epsilon = 1e-6;

% Moller-Trumbore intersection algorithm

e1 = v2 - v1;
e2 = v3 - v1;
p = cross(d, e2);
det = dot(e1, p);
if (abs(det) < epsilon)
    return;
end

inv_det = 1/det;
t = o - v1;
u = dot(t, p) * inv_det;
if (u < 0 || u > 1)
    return;
end

q = cross(t, e1);
v = dot(d, q) * inv_det;
if (v < 0 || u + v > 1)
    return;
end

v_bool = 1;
v_out = (1-u-v) * v1 + u * v2 + v * v3;

end

