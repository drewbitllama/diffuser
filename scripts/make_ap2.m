function [b,a] = make_ap2( g )

b = [g.^2 0 -1];
a = [1 0 -g.^2];

end