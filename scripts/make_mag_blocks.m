function [ X ] = make_mag_blocks( x, block_size, hop_size )

pfreq_size = block_size / 2 + 1;
win = blackman(block_size);
blocks = ceil((length(x) - block_size) / hop_size) + 1;
zeropad = (blocks - 1) * hop_size + block_size - length(x);
xT = [x; zeros(zeropad, 1)];

X = zeros(pfreq_size, blocks);
parfor i=1:blocks
    xs_idx = (i - 1) * hop_size + 1;
    block = xT(xs_idx:xs_idx + block_size - 1);
    Xslice = abs(fft(block .* win));
    X(:,i) = Xslice(1:pfreq_size);
end

end

