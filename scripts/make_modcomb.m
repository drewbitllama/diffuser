function [ x ] = make_modcomb( len, t60_dc, t60_ny, m, fs )

alpha = t60_ny / t60_dc;

Kp = -60 * m / fs / t60_dc;
Gp = -60 * m / fs / t60_ny;

kp = 10^(Kp / 20);
gp = 10^(Gp / 20);

bp = (kp - gp) / (kp + gp);
% bp = Kp * log(10) / 80 * (1 - 1 / alpha^2);

b0 = kp * (1 - bp);
a1 = -bp;

b = zeros(m+1,1);
b(m+1) = b0;

a = zeros(m+1,1);
a(1) = 1;
a(2) = a1;
a(m+1) = -b0;

% len = round(max(t60_dc, t60_ny) * fs);
dirac = [1; zeros(len - 1, 1)];
x = filter(b, a, dirac);

end

