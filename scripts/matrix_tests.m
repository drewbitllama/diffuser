clearvars;
clc;

n = 3;

name = 'med';

prime_min = 500;
prime_max = 2000;
dels = primes(prime_max);
for i=1:length(dels)
    if dels(i) > prime_min
        dels = dels(i:end);
        break;
    end
end

del_idx = round(linspace(1,length(dels),n));
dels = dels(del_idx);

M = eye(n);
for i=1:n
    for j=i+1:n
        M = axis_rotate(M,i,j,45);
    end
end
svd(M)

c_code1 = sprintf('const real %s_delay_values[%d] = {',name,n);
for i=1:n
    if i==1
        prefix = c_code1;
    else
        prefix = strcat(c_code1,',');
    end
    c_code1 = strcat(prefix,num2str(dels(i)));
end
c_code1 = strcat(c_code1,'};');

c_code2 = sprintf('const real %s_matrix_values[%d] = {',name,n*n);
for i=1:n
    for j=1:n
        if i == 1 && j == 1
            prefix = c_code2;
        else
            prefix = strcat(c_code2,',');
        end
        c_code2 = strcat(prefix,num2str(M(i,j)));
    end
end
c_code2 = strcat(c_code2,'};');

c_code1
c_code2



