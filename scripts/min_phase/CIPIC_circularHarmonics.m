clearvars;
clc;

subject = 27;
len = 256;
num_angles = round(360/10);
w = 1:(len/2+1);

filename = sprintf('../CIPIC_hrtf_database/standard_hrir_database/subject_%03d/hrir_final.mat',subject);
load(filename);

% 1-25 9, 25-1 41
azis = [-80 -65 -55 -45:5:45 55 65 80];
eles = -45 + 5.625*(0:49);

h_l = zeros(50,len);
h_r = zeros(50,len);
del_l = zeros(50,1);
del_r = zeros(50,1);

for i=1:25  
    del_l(i) = round(OnL(i,9));
    del_r(i) = round(OnR(i,9));
    del_l(i+25) = round(OnL(26-i,41));
    del_r(i+25) = round(OnR(26-i,41));
    a1 = squeeze(hrir_l(i,9,del_l(i):end));
    a2 = squeeze(hrir_r(i,9,del_r(i):end));
    a3 = squeeze(hrir_l(26-i,41,del_l(i+25):end));
    a4 = squeeze(hrir_r(26-i,41,del_r(i+25):end));
    h_l(i,:) = fft([a1(:); zeros(len - length(a1),1)]);
    h_r(i,:) = fft([a2(:); zeros(len - length(a2),1)]);
    h_l(i+25,:) = fft([a3(:); zeros(len - length(a3),1)]);
    h_r(i+25,:) = fft([a4(:); zeros(len - length(a4),1)]);   
    
end

cipic_horz = [azis (azis + 180)];

h_l_reg = zeros(num_angles,len);
h_r_reg = zeros(num_angles,len);
del_l_reg = round(irregular_circular_resample(del_l,cipic_horz,num_angles));
del_r_reg = round(irregular_circular_resample(del_r,cipic_horz,num_angles));
for i=1:len
    h_l_reg(:,i) = irregular_circular_resample(h_l(:,i),cipic_horz,num_angles);
    h_r_reg(:,i) = irregular_circular_resample(h_r(:,i),cipic_horz,num_angles);    
end

circ_l = zeros(num_angles,len);
circ_r = zeros(num_angles,len);

for i=1:len
    circ_l(:,i) = fft(h_l_reg(:,i));
    circ_r(:,i) = fft(h_r_reg(:,i));
end

circ_l_recon = circ_l;
circ_r_recon = circ_r;

h_l_recon = zeros(num_angles,len);
h_r_recon = zeros(num_angles,len);

for i=1:len
    h_l_recon(:,i) = ifft(circ_l_recon(:,i));
    h_r_recon(:,i) = ifft(circ_r_recon(:,i));
end

fs = 44100;
burst_len = round(fs*0.1);
burst = rand(burst_len,1)*2-1;
burst = burst .* linspace(1,0,burst_len)';

% playblocking(audioplayer(burst,fs));

out_l = [];
out_r = [];

del_len = len * 2;

for i=1:50
    imp_l = [zeros(del_l(i),1); ifft(h_l(i,:))'];
    imp_r = [zeros(del_r(i),1); ifft(h_r(i,:))'];
    imp_l = [imp_l(:); zeros(del_len - length(imp_l), 1)];
    imp_r = [imp_r(:); zeros(del_len - length(imp_r), 1)];
    out_l = [out_l; conv(burst, imp_l)];
    out_r = [out_r; conv(burst, imp_r)];
end

% for i=1:num_angles
%     imp_l = [zeros(del_l_reg(i),1); ifft(h_l_reg(i,:))'];
%     imp_r = [zeros(del_r_reg(i),1); ifft(h_r_reg(i,:))'];
%     imp_l = [imp_l(:); zeros(del_len - length(imp_l), 1)];
%     imp_r = [imp_r(:); zeros(del_len - length(imp_r), 1)];
%     out_l = [out_l; conv(burst, imp_l)];
%     out_r = [out_r; conv(burst, imp_r)];
% end

out = [out_l out_r];
out = out ./ max(max(abs(out)));
playblocking(audioplayer(out,fs));
error;

for i=1:50
    figure(1);
    subplot(2,1,1);
    plot([zeros(del_l(i),1); ifft(h_l(i,:))']);
    xlim([1 len]);
    ylim([-1 1]);
    subplot(2,1,2);
    plot([zeros(del_r(i),1); ifft(h_r(i,:))']);
    xlim([1 len]);
    ylim([-1 1]);
    pause(0.1);
end

% for i=1:num_angles
%     figure(1);
%     subplot(2,1,1);
%     plot(20*log10(abs(h_l_reg(i,w))));
%     xlim([1 len/2+1]);
%     ylim([-60 20]);
%     subplot(2,1,2);
%     plot(20*log10(abs(h_r_reg(i,w))));
%     xlim([1 len/2+1]);
%     ylim([-60 20]);
%     pause(0.1);
% end


