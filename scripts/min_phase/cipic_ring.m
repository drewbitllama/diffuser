clearvars;
clc;

subject = 126;

use_zero_phase = 0;
use_min_phase = 0;
deg_step = 1; % azimuth degree stepsiez
len = 2000; % FIR length (input is 200)

filename = sprintf('../CIPIC_hrtf_database/standard_hrir_database/subject_%03d/hrir_final.mat',subject);
load(filename);

% ele: 9 & 41

azis = [-80 -65 -55 -45:5:45 55 65 80];
eles = -45 + 5.625*(0:49);

lm = zeros(50,len);
rm = zeros(50,len);

for i=1:25
    
    lmt = squeeze(hrir_l(i,9,round(OnL(i,9)):end));
    if (use_zero_phase == 1)
        lmt = zerophase(lmt);
    elseif (use_min_phase == 1)
        lmt = ifftshift(ifft(mps(fft(lmt)),'symmetric'));
    end
    lmt = [zeros(round(OnL(i,9)),1); lmt(:)];
    lm(i,:) = [lmt(1:min(len,length(lmt))); zeros(len - min(len,length(lmt)),1)];
    
    rmt = squeeze(hrir_r(i,9,round(OnR(i,9)):end));
    if (use_zero_phase == 1)
        rmt = zerophase(rmt);
    elseif (use_min_phase == 1)
        rmt = ifftshift(ifft(mps(fft(rmt)),'symmetric'));
    end
    rmt = [zeros(round(OnR(i,9)),1); rmt(:)];
    rm(i,:) = [rmt(1:min(len,length(rmt))); zeros(len - min(len,length(rmt)),1)];
    
    lmt = squeeze(hrir_l(26-i,41,round(OnL(26-i,41)):end));
    if (use_zero_phase == 1)
        lmt = zerophase(lmt);
    elseif (use_min_phase == 1)
        lmt = ifftshift(ifft(mps(fft(lmt)),'symmetric'));
    end
    lmt = [zeros(round(OnL(26-i,41)),1); lmt(:)];
    lm(i+25,:) = [lmt(1:min(len,length(lmt))); zeros(len - min(len,length(lmt)),1)];
    
    rmt = squeeze(hrir_r(26-i,41,round(OnR(26-i,41)):end));
    if (use_zero_phase == 1)
        rmt = zerophase(rmt);
    elseif (use_min_phase == 1)
        rmt = ifftshift(ifft(mps(fft(rmt)),'symmetric'));
    end
    rmt = [zeros(round(OnR(26-i,41)),1); rmt(:)];
    rm(i+25,:) = [rmt(1:min(len,length(rmt))); zeros(len - min(len,length(rmt)),1)];

end

compare_azis = [azis (azis + 180)];
deg = -90:deg_step:270;

% first and second azi indexes, alpha val
interp_azis = zeros(length(deg),3); 

for i=1:length(deg)
    for j=1:length(compare_azis)
        if j == 1
            if deg(i) <= compare_azis(j)
                azi_min = (compare_azis(end) - 360);
                azi_max = compare_azis(1);
                azi_min_idx = length(compare_azis);
                azi_max_idx = 1;
                break;
            end
        elseif j == length(compare_azis)            
            if deg(i) <= compare_azis(j)
                azi_min = compare_azis(j-1);
                azi_max = compare_azis(j);
                azi_min_idx = j-1;
                azi_max_idx = j;
                break;
            else
                azi_min = compare_azis(end);
                azi_max = compare_azis(1) + 360;
                azi_min_idx = length(compare_azis);
                azi_max_idx = 1;   
                break;
            end
        else
            if deg(i) > compare_azis(j-1) && deg(i) <= compare_azis(j)
                azi_min = compare_azis(j-1);
                azi_max = compare_azis(j);
                azi_min_idx = j-1;
                azi_max_idx = j;
                break;
            end
        end
    end
    interp_azis(i,1) = azi_min_idx;
    interp_azis(i,2) = azi_max_idx;
    interp_azis(i,3) = (deg(i) - azi_min) / (azi_max - azi_min);
end

out_l = zeros(length(deg),len);
out_r = zeros(length(deg),len);

fs = 44100;
burst_len = round(fs*0.1);
burst = rand(burst_len,1) * 2 - 1;
burst = burst .* linspace(1,0,burst_len)';

outL = [];
outR = [];
for i=1:length(deg)
    
    out_l(i,:) = interp_azis(i,3) .* lm(interp_azis(i,2),:) + (1 - interp_azis(i,3)) .* lm(interp_azis(i,1),:);
    out_r(i,:) = interp_azis(i,3) .* rm(interp_azis(i,2),:) + (1 - interp_azis(i,3)) .* rm(interp_azis(i,1),:);

%     out_l(i,:) = interp_arc(lm(interp_azis(i,2),:),...
%         lm(interp_azis(i,1),:),interp_azis(i,3));
%     out_r(i,:) = interp_arc(rm(interp_azis(i,2),:),...
%         rm(interp_azis(i,1),:),interp_azis(i,3));

    outL = [outL; conv(burst, out_l(i,:))];
    outR = [outR; conv(burst, out_r(i,:))];
    
%     figure(1);
%     subplot(2,1,1);
%     plot(out_l(i,:));
%     ylim([-1 1]);
%     subplot(2,1,2);
%     plot(out_r(i,:));
%     ylim([-1 1]);
%     pause(0.1);
end

% outL = [];
% outR = [];
% for i=1:50
%     outL = [outL; conv(burst,lm(i,:))];
%     outR = [outR; conv(burst,rm(i,:))];
% end

out = [outL outR];
out = out ./ max(max(abs(out)));
playblocking(audioplayer(out,fs));
