function [ coords ] = get_cipic_cartcoords( )

cipic_azis = [-80 -65 -55 -45:5:45 55 65 80];
cipic_eles = -45 + 5.625*(0:49);
coords = [];

% convert CIPIC angles to cartesian coordinates
for i=1:25
    for j=1:50
        theta = cipic_azis(i)/180*pi;
        phi = (cipic_eles(j)-90)/180*pi;
        v = [0 0 1]';
        Rx = [1 0 0; 0 cos(theta) -sin(theta); 0 sin(theta) cos(theta)];
        Ry = [cos(phi) 0 sin(phi); 0 1 0; -sin(phi) 0 cos(phi)];
        coords = [coords Ry * Rx * v];
    end
end

end

