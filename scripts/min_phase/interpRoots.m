clearvars;
clc;

len = 4000;
subject = 15;

filename = sprintf('../CIPIC_hrtf_database/standard_hrir_database/subject_%03d/hrir_final.mat',subject);
load(filename);

azi0 = 1;
ele0 = 1;

azi1 = 3;
ele1 = 1;

x0 = squeeze(hrir_l(azi0,ele0,round(OnL(azi0,ele0)):end));
x1 = squeeze(hrir_l(azi1,ele1,round(OnL(azi1,ele1)):end));

x0 = x0(1:min(length(x0),len));
x1 = x1(1:min(length(x1),len));

x0 = [x0(:); zeros(len - length(x0), 1)];
x1 = [x1(:); zeros(len - length(x1), 1)];

for alpha=0:0.01:1

%     xa = interp_arc(x0,x1,alpha);
    xa = (1-alpha)*x0+alpha*x1;
    
    figure(1);
    subplot(2,1,1);
    plot(x0,'--','Color',[1 0.75 0.75]);
    hold on;
    plot(x1,'--','Color',[0.75 0.75 1]);
    plot(xa,'k');
    hold off;
    
    subplot(2,1,2);
    w = 1:(len/2+1);
    h0 = fft(x0);
    h1 = fft(x1);
    ha = fft(xa);
    plot(20*log10(abs(h0(w))),'--','Color',[1 0.75 0.75]);
    hold on;
    plot(20*log10(abs(h1(w))),'--','Color',[0.75 0.75 1]);
    plot(20*log10(abs(ha(w))),'k');
    xlim([1 len/2+1]);
    ylim([-60 20]);
    hold off;

    pause(0.1);
    
end
% xm0 = mps(x0);
% xm1 = mps(x1);
% 
% w = 1:len/2+1;
% hm0 = fft(xm0);
% hm1 = fft(xm1);
% 
% for alpha=0:0.01:1
% 
%     x = interp_arc(xm0,xm1,alpha);
%     h = fft(x);
% 
%     xi = (1-alpha)*xm0+alpha*xm1;
%     hi = fft(xi);
%     
%     figure(1);
%     
%     subplot(2,1,1);
%     plot(w,20*log10(abs(hm0(w))),'--','Color',[0.75 0.75 1]);
%     hold on;
%     plot(w,20*log10(abs(hm1(w))),'--','Color',[1 0.75 0.75]);
%     plot(w,20*log10(abs(h(w))),'Color',[1 0.5 0]);
%     plot(w,20*log10(abs(hi(w))),'Color',[0 0.5 1]);
%     hold off;
%     
%     subplot(2,1,2);
%     plot(xm0,'--','Color',[0.75 0.75 1]);
%     hold on;
%     plot(xm1,'--','Color',[1 0.75 0.75]);
%     plot(x,'Color',[1 0.5 0]);
%     plot(xi,'Color',[0 0.5 1]);
%     hold off;
%     ylim([-1 1]);
%     
%     pause(0.1);
% 
% end