function [ c ] = interp_arc( a, b, alpha )
% a and b are time-domain signal
% alpha is a value between 0 (all a) and 1 (all b)
% returns an arc-interpolated time-domain signal

max_len = max(length(a),length(b));
a = [a(:); zeros(max_len - length(a), 1)];
b = [b(:); zeros(max_len - length(b), 1)];

h_a = fft(a);
h_b = fft(b);

ang_a = angle(h_a);
ang_b = angle(h_b);
ang_diff = ang_a - ang_b;
direction = (ang_diff > pi) .* 1 + (ang_diff < -pi) .* -1;

mag_a = abs(h_a);
mag_b = abs(h_b);

ang_c = zeros(max_len,1);
for i=1:max_len
    if (abs(imag(h_a(i))) < 1e-8 && abs(imag(h_b(i))) < 1e-8)
        ang_c(i) = 0;
    elseif (direction(i) == 0)
        ang_c(i) = (1 - alpha) * ang_a(i) + alpha * ang_b(i);
    elseif (direction(i) == 1)
        ang_c(i) = (1 - alpha) * ang_a(i) + alpha * (ang_b(i) + 2*pi);
    elseif (direction(i) == -1)
        ang_c(i) = (1 - alpha) * (ang_a(i) - 2*pi) + alpha * ang_b(i);
    end
end   
% ang_c = zeros(length(h_a),1);
mag_c = (1 - alpha) * mag_a + alpha * mag_b;

h_c = mag_c .* (cos(ang_c) + sqrt(-1)*sin(ang_c));
c = ifft(h_c, 'symmetric');

end