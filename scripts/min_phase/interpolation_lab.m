clearvars;
clc;

use_truncate = 1;
use_min_phase = 1;

zero_phase_expon = 1;

subject = 15;

filename = sprintf('../CIPIC_hrtf_database/standard_hrir_database/subject_%03d/hrir_final.mat',subject);
load(filename);

azis = [-80 -65 -55 -45:5:45 55 65 80];
eles = -45 + 5.625*(0:49);

% chosen degrees
angles = [1 9; 2 9];
[azis(angles(1,1)) eles(angles(1,2));
 azis(angles(2,1)) eles(angles(2,2))]

len = 256;
t = 1:len;

% raw inputs
xl0 = squeeze(hrir_l(angles(1,1),angles(1,2),:));
xr0 = squeeze(hrir_r(angles(1,1),angles(1,2),:));
xl1 = squeeze(hrir_l(angles(2,1),angles(2,2),:));
xr1 = squeeze(hrir_r(angles(2,1),angles(2,2),:));

% truncated inputs
if (use_truncate == 0)
    xl0t = xl0;
    xr0t = xr0;
    xl1t = xl1;
    xr1t = xr1;
else
    xl0t = xl0(round(OnL(4,9)):end);
    xr0t = xr0(round(OnR(4,9)):end);
    xl1t = xl1(round(OnL(5,9)):end);
    xr1t = xr1(round(OnR(5,9)):end);
end

% pad signal regardless
xl0t = [xl0t(:); zeros(len - length(xl0t), 1)];
xr0t = [xr0t(:); zeros(len - length(xr0t), 1)];
xl1t = [xl1t(:); zeros(len - length(xl1t), 1)];
xr1t = [xr1t(:); zeros(len - length(xr1t), 1)];

% min.phase inputs
if (use_min_phase == 0)
    xl0m = xl0t;
    xr0m = xr0t;
    xl1m = xl1t;
    xr1m = xr1t;
else
    xl0m = ifft(mps(fft(xl0t)),'symmetric');
    xr0m = ifft(mps(fft(xr0t)),'symmetric');
    xl1m = ifft(mps(fft(xl1t)),'symmetric');
    xr1m = ifft(mps(fft(xr1t)),'symmetric');
end

% plot truncated-raw vs min.phase
% figure(1);
% subplot(2,2,1);
% plot(t,xl0t,t,xl0m);
% subplot(2,2,2);
% plot(t,xr0t,t,xr0m);
% subplot(2,2,3);
% plot(t,xl1t,t,xl1m);
% subplot(2,2,4);
% plot(t,xr1t,t,xr1m);

% plot zero-phase filters
% figure(3);
% h_t = fft(xl0t);
% xl0t_a = ifftshift(ifft(abs(h_t),'symmetric'));
% h_a = fft(xl0t_a);
% plot(t,20*log10(abs(h_t)),t,20*log10(abs(h_a)));

% create zero-phase filters
xl0z = ifftshift(ifft(abs(fft(xl0t)),'symmetric'));
xr0z = ifftshift(ifft(abs(fft(xr0t)),'symmetric'));
xl1z = ifftshift(ifft(abs(fft(xl1t)),'symmetric'));
xr1z = ifftshift(ifft(abs(fft(xr1t)),'symmetric'));

% plot interpolated filters
nfft = 512;
w = 1:(len/2)+1;
for alpha=0:0.01:1
    
    xlarc = interp_arc(xl0t,xl1t,alpha);
    xrarc = interp_arc(xr0t,xr1t,alpha);
    xlzarc = interp_arc(xl0z,xl1z,alpha);
    xrzarc = interp_arc(xr0z,xr1z,alpha);
    xlmarc = interp_arc(xl0m,xl1m,alpha);
    xrmarc = interp_arc(xr0m,xr1m,alpha);
    xli = (1 - alpha) .* xl0t + alpha .* xl1t;
    xri = (1 - alpha) .* xr0t + alpha .* xr1t;
    xlz = zeroPhaseInterp(xl0t,xl1t,alpha,zero_phase_expon);
    xrz = zeroPhaseInterp(xr0t,xr1t,alpha,zero_phase_expon);
    xlzm = zeroPhaseInterp(xl0m,xl1m,alpha,zero_phase_expon);
    xrzm = zeroPhaseInterp(xr0m,xr1m,alpha,zero_phase_expon);
    xlm = (1 - alpha) .* xl0m + alpha .* xl1m;
    xrm = (1 - alpha) .* xr0m + alpha .* xr1m;
    
    hl0 = fft(xl0t);
    hl1 = fft(xl1t);
    hr0 = fft(xr0t);
    hr1 = fft(xr1t);
    
    hlarc = fft(xlarc);
    hrarc = fft(xrarc);
    hli = fft(xli);
    hri = fft(xri);
    hlz = fft(xlz);
    hrz = fft(xrz);
    hlzm = fft(xlzm);
    hrzm = fft(xrzm);
    hlzarc = fft(xlzarc);
    hrzarc = fft(xrzarc);
    hlm = fft(xlm);
    hrm = fft(xrm);
    
%     figure(10);
%     theta = linspace(0,2*pi,3600);
%     plot(real(hlzarc),imag(hlzarc),'x',cos(theta),sin(theta),'--');
%     axis equal;
%     ylim([-1 1]);
%     xlim([-1 1]);

    figure(2);
    subplot(2,2,1);
    plot(w,20*log10(abs(hl0(w))),'--b',...
        w,20*log10(abs(hl1(w))),'--r',...
        w,20*log10(abs(hlarc(w))),'k');
    xlim([1 len/2+1]);
    ylim([-60 20]);
    subplot(2,2,2);
    plot(w,20*log10(abs(hr0(w))),'--b',...
        w,20*log10(abs(hr1(w))),'--r',...
        w,20*log10(abs(hrarc(w))),'k');
    xlim([1 len/2+1]);
    ylim([-60 20]);
    subplot(2,2,3);
    plot(xlarc);
    xlim([1 len]);
    ylim([-1 1]);
    subplot(2,2,4);
    plot(xrarc);
    xlim([1 len]);
    ylim([-1 1]);
    pause(0.01);
    
end




