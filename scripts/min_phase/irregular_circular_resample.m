function [ y ] = irregular_circular_resample( x, degrees, degree_steps )

x = [x(:); x(:); x(:)];
degrees = [degrees(:) - 360; degrees(:); degrees(:) + 360];

yt = linspace(0,360,degree_steps+1);
yt = yt(1:end-1);

% degrees
y = spline(degrees,x,yt);

end

