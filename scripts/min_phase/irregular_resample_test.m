clearvars;
clc;

azis = [-80 -65 -55 -45:5:45 55 65 80];
horz = [azis (azis + 180)];

x = linspace(1,2,length(horz))';
y = irregular_circular_resample(x,horz,360);

figure(1);
plot(horz,x,'o',0:359,y);
