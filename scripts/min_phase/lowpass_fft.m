function [ h_out ] = lowpass_fft( h, max_idx )

h_out = zeros(length(h),1);
for i=1:(length(h)/2+1)
    if (i <= max_idx)
        h_out(i) = h(i);
        if (i > 1)
            h_out(end-i+2) = h(end-i+2);
        end
    end
end

end
