function [ y ] = mps( x )

y = ifft(exp(fft(fold(ifft(log(fft(x)))))),'symmetric');

end

