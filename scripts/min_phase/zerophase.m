function [ y ] = zerophase( x )

y = ifftshift(ifft(abs(fft(x))));

end

