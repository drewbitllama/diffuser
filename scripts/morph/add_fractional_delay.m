function [ y ] = add_fractional_delay( x, del, wrap_mode )

x = x(:);
if nargin == 3
    x = [x(end-floor(del)+1:end); x(1:end-floor(del))];
else
    x = [zeros(floor(del),1); x(:)];
end

frac = del - floor(del);
t = (1:length(x))';

y = zeros(length(x),1);

for i=1:length(y)
    y(i) = sum(sinc(t - (i - frac)) .* x);
end

end

