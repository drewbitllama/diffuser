clearvars;
clc;

subjects = [3,8,9,10,11,12,15,17,18,19,20,21,27,28,33,40,44,48,50,51,58,59,...
    60,61,65,119,124,126,127,131,133,134,135,137,147,148,152,154,155,156,158,162,163,165];
subject = 126;

filename = sprintf('../CIPIC_hrtf_database/standard_hrir_database/subject_%03d/hrir_final.mat',subject);
load(filename);

fs = 44100;
burst_len = round(fs*0.1);
burst = rand(burst_len,1)*2-1;
burst = burst .* linspace(1,0,burst_len)';

l = zeros(50,200);
r = zeros(50,200);
for i=1:25
    l(i,:) = squeeze(hrir_l(i,9,:));
    r(i,:) = squeeze(hrir_r(i,9,:));
    l(i+25,:) = squeeze(hrir_l(26-i,41,:));
    r(i+25,:) = squeeze(hrir_r(26-i,41,:));
end

out_l = [];
out_r = [];
for i=1:50
    
    out_l = [out_l; conv(burst, l(i,:))];
    out_r = [out_r; conv(burst, r(i,:))];
    
end

out = [out_l out_r];
out = out ./ max(max(abs(out)));
playblocking(audioplayer(out,fs));
