clearvars;
clc;
% close all;

morph_mode = 2;
weight = 10;
use_min_phase = 1;

% lin/mph, l/r, mag/phase/total
err = zeros(25,2,2,3);

subjects = [3,8,9,10,11,12,15,17,18,19,20,21,27,28,33,40,44,48,50,51,58,59,60,61,65,119,124,126,127,131,133,134,135,137,147,148,152,154,155,156,158,162,163,165];
for subject_idx = 1:length(subjects)

    subject = subjects(subject_idx);
    filename = sprintf('../CIPIC_hrtf_database/standard_hrir_database/subject_%03d/hrir_final.mat',subject);
    load(filename);

    cipic_azis = [-80 -65 -55 -45:5:45 55 65 80];
    azis = [cipic_azis (cipic_azis + 180)];

    len = 1024;
    ring_l = zeros(50,len);
    ring_r = zeros(50,len);

    for i=1:25

        l0 = squeeze(hrir_l(i,9,:));
        r0 = squeeze(hrir_r(i,9,:));
        l1 = squeeze(hrir_l(26-i,41,:));
        r1 = squeeze(hrir_r(26-i,41,:));

    %     if (truncate_hrtfs == 0)
    %         l0 = remove_fractional_delay(l0, OnL(i,9));
    %         r0 = remove_fractional_delay(r0, OnR(i,9));
    %         l1 = remove_fractional_delay(l1, OnL(26-i,41));
    %         r1 = remove_fractional_delay(r1, OnR(26-i,41));
    %     end

        l0 = [l0(:); zeros(len - length(l0), 1)];
        r0 = [r0(:); zeros(len - length(r0), 1)];
        l1 = [l1(:); zeros(len - length(l1), 1)];
        r1 = [r1(:); zeros(len - length(r1), 1)];

        if (use_min_phase == 1)
            l0 = mps(l0);
            r0 = mps(r0);
            l1 = mps(l1);
            r1 = mps(r1);
        end

        ring_l(i,:) = l0;
        ring_r(i,:) = r0;
        ring_l(25+i,:) = l1;
        ring_r(25+i,:) = r1;

    end

    t = 1:len;
    w = 1:len/2+1;


    idx = 1;
    for i=2:2:50

        hl = fft(ring_l(i,:)');
        hr = fft(ring_r(i,:)');
        fl = hl(w);
        fr = hr(w);

        hl0 = fft(ring_l(i-1,:)');
        hr0 = fft(ring_r(i-1,:)');
        if (i == 50)
            hl1 = fft(ring_l(1,:)');
            hr1 = fft(ring_r(1,:)');
        else
            hl1 = fft(ring_l(i+1,:)');
            hr1 = fft(ring_r(i+1,:)');
        end

        fl0 = hl0(w);
        fr0 = hr0(w);
        fl1 = hl1(w);
        fr1 = hr1(w);

        alpha = 0.5;
        fl_lin = (1-alpha)*fl0+alpha*fl1;
        fr_lin = (1-alpha)*fr0+alpha*fr1;
        if (i == 50)
            xl_mph = get_morph_between(ring_l(i-1,:),ring_l(1,:),alpha,weight,morph_mode);
            xr_mph = get_morph_between(ring_r(i-1,:),ring_r(1,:),alpha,weight,morph_mode);
        else
            xl_mph = get_morph_between(ring_l(i-1,:),ring_l(i-1,:),alpha,weight,morph_mode);
            xr_mph = get_morph_between(ring_r(i-1,:),ring_r(i-1,:),alpha,weight,morph_mode);
        end

        hl_lin = [fl_lin(:); fl_lin(end-1:-1:2)];
        hr_lin = [fr_lin(:); fr_lin(end-1:-1:2)];

        xl_lin = ifft(hl_lin,'symmetric');
        xr_lin = ifft(hr_lin,'symmetric');

        hl_mph = fft(xl_mph);
        hr_mph = fft(xr_mph);
        fl_mph = hl_mph(w);
        fr_mph = hr_mph(w);

    %     figure(i);
    %     subplot(2,2,1);
    %     plot(t,ring_l(i,:),t,xl_lin,t,xl_mph);
    %     xlim([1 len]);
    %     ylim([-1 1]);
    %     subplot(2,2,2);
    %     plot(w,20*log10(abs(hl(w))),w,20*log10(abs(fl_lin)),w,20*log10(abs(fl_mph)));
    %     xlim([1 len/2+1]);
    %     ylim([-60 20]);
    %     subplot(2,2,3);
    %     plot(t,ring_r(i,:),t,xr_lin,t,xr_mph);
    %     xlim([1 len]);
    %     ylim([-1 1]);
    %     subplot(2,2,4);
    %     plot(w,20*log10(abs(hr(w))),w,20*log10(abs(fr_lin)),w,20*log10(abs(fr_mph)));
    %     xlim([1 len/2+1]);
    %     ylim([-60 20]);

        % lin/mph l/r mag/phase/total
        err(idx,1,1,1) = err(idx,1,1,1) + sum(abs(abs(fl) - abs(fl_lin))) / (len/2+1);
        err(idx,1,2,1) = err(idx,1,2,1) + sum(abs(abs(fr) - abs(fr_lin))) / (len/2+1);
        err(idx,1,1,2) = err(idx,1,1,2) + sum(abs(angle(fl) - angle(fl_lin))) / (len/2+1) / (2*pi);
        err(idx,1,2,2) = err(idx,1,2,2) + sum(abs(angle(fr) - angle(fr_lin))) / (len/2+1) / (2*pi);
        err(idx,1,1,3) = err(idx,1,1,3) + sum(abs(fl - fl_lin)) / (len/2+1) / 2;
        err(idx,1,2,3) = err(idx,1,2,3) + sum(abs(fr - fr_lin)) / (len/2+1) / 2;

        err(idx,2,1,1) = err(idx,2,1,1) + sum(abs(abs(fl) - abs(fl_mph))) / (len/2+1);
        err(idx,2,2,1) = err(idx,2,2,1) + sum(abs(abs(fr) - abs(fr_mph))) / (len/2+1);
        err(idx,2,1,2) = err(idx,2,1,2) + sum(abs(angle(fl) - angle(fl_mph))) / (len/2+1) / (2*pi);
        err(idx,2,2,2) = err(idx,2,2,2) + sum(abs(angle(fr) - angle(fr_mph))) / (len/2+1) / (2*pi);
        err(idx,2,1,3) = err(idx,2,1,3) + sum(abs(fl - fl_mph)) / (len/2+1) / 2;
        err(idx,2,2,3) = err(idx,2,2,3) + sum(abs(fr - fr_mph)) / (len/2+1) / 2;

        idx = idx + 1;

    end

end

err = err ./ length(subjects);

et = 1:25;
figure(1);

subplot(2,3,1);
plot(et,squeeze(err(:,1,1,1)),et,squeeze(err(:,2,1,1)));
xlim([1 25]);
ylim([0 2]);
title('Lin vs Mph Mag (L)');

subplot(2,3,2);
plot(et,squeeze(err(:,1,1,2)),et,squeeze(err(:,2,1,2)));
xlim([1 25]);
ylim([0 2]);
title('Lin vs Mph Phase (L)');

subplot(2,3,3);
plot(et,squeeze(err(:,1,1,3)),et,squeeze(err(:,2,1,3)));
xlim([1 25]);
ylim([0 2]);
title('Lin vs Mph Total (L)');

subplot(2,3,4);
plot(et,squeeze(err(:,1,2,1)),et,squeeze(err(:,2,2,1)));
xlim([1 25]);
ylim([0 2]);
title('Lin vs Mph Mag (R)');

subplot(2,3,5);
plot(et,squeeze(err(:,1,2,2)),et,squeeze(err(:,2,2,2)));
xlim([1 25]);
ylim([0 2]);
title('Lin vs Mph Phase (R)');

subplot(2,3,6);
plot(et,squeeze(err(:,1,2,3)),et,squeeze(err(:,2,2,3)));
xlim([1 25]);
ylim([0 2]);
title('Lin vs Mph Total (R)');

