clearvars;
clc;

subject = 27;

x = audioread(sprintf('./output/cipic_%03d_linear_a%03d.wav',subject,0));
line_l = sprintf('float hrir_l[%d][%d] = {\n',360,size(x,1));
line_r = sprintf('float hrir_r[%d][%d] = {\n',360,size(x,1));
for i=0%:359
    
    x = audioread(sprintf('./output/cipic_%03d_linear_a%03d.wav',subject,i));
    
    line_l = sprintf('%s{ ',line_l);
    for i=1:size(x,1)
        line_l = sprintf('%s %f',line_l,x(i,1));
        line_r = sprintf('%s %f',line_r,x(i,2));
        if (i < size(x,1))
            line_l = sprintf('%s,',line_l);
            line_r = sprintf('%s,',line_r);
        end
    end
    line_l = sprintf('%s}, \n',line_l);
end
line_l = sprintf('%s\n};',line_l);
line_r = sprintf('%s\n};',line_r);

line_l
line_r