function [ avg_diff_l avg_diff_r ] = frontbackcompare()

% clearvars;
% clc;

subjects = [3,8,9,10,11,12,15,17,18,19,20,21,27,28,33,40,44,48,50,51,58,59,60,61,65,119,124,126,127,131,133,134,135,137,147,148,152,154,155,156,158,162,163,165];

len = 200;
w = 1:len/2+1;

diff_db_l = zeros(25,len/2+1,length(subjects));
diff_db_r = zeros(25,len/2+1,length(subjects));
diff_l = zeros(25,len/2+1,length(subjects));
diff_r = zeros(25,len/2+1,length(subjects));

for subjects_idx = 1:length(subjects)
    
    subject = subjects(subjects_idx);
    filename = sprintf('../CIPIC_hrtf_database/standard_hrir_database/subject_%03d/hrir_final.mat',subject);
    load(filename);

    for azi0 = 1:25;
        ele0 = 9;
        azi1 = 26 - azi0;
        ele1 = ele0 + 32;

        x0_l = squeeze(hrir_l(azi0,ele0,:));
        x0_r = squeeze(hrir_r(azi0,ele0,:));
        x1_l = squeeze(hrir_l(azi1,ele1,:));
        x1_r = squeeze(hrir_r(azi1,ele1,:));

        h0_l = fft(x0_l);
        h0_r = fft(x0_r);
        h1_l = fft(x1_l);
        h1_r = fft(x1_r);

        diff_db_l(azi0,:,subjects_idx) = 20*log10(abs(h1_r(w))) - 20*log10(abs(h0_l(w)));
        diff_db_r(azi0,:,subjects_idx) = 20*log10(abs(h1_l(w))) - 20*log10(abs(h0_r(w)));
        diff_l(azi0,:,subjects_idx) = 10.^(diff_db_l(azi0,:,subjects_idx)/20);
        diff_r(azi0,:,subjects_idx) = 10.^(diff_db_r(azi0,:,subjects_idx)/20);
    end
    
end

avg_diff_db_l = zeros(25,len/2+1);
avg_diff_db_r = zeros(25,len/2+1);
avg_diff_l = zeros(25,len/2+1);
avg_diff_r = zeros(25,len/2+1);

for i=1:25
    avg_diff_db_l(i,:) = sum(diff_db_l(i,:,:),3) ./ length(subjects);
    avg_diff_db_r(i,:) = sum(diff_db_r(i,:,:),3) ./ length(subjects);
    avg_diff_l(i,:) = sum(diff_l(i,:,:),3) ./ length(subjects);
    avg_diff_r(i,:) = sum(diff_r(i,:,:),3) ./ length(subjects);
end

% for i=1:25
%     figure(2);
%     subplot(1,2,1);
%     plot(avg_diff_db_l(i,:));
%     ylim([-30 30]);
%     subplot(1,2,2);
%     plot(avg_diff_db_r(i,:));
%     ylim([-30 30]);
%     pause(0.1);
% end

end
