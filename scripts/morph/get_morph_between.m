function [ xn ] = get_morph_between( x0, x1, alpha, mag_weighting, mode )

if nargin == 4
    mode = 0;
end

% zero-pad / make inputs same length
x0 = [x0(:); zeros(length(x1) - length(x0), 1)];
x1 = [x1(:); zeros(length(x0) - length(x1), 1)];

% quick-return if simple
if (alpha == 0)
    xn = x0;
    return;
elseif (alpha == 1)
    xn = x1;
    return;
end

% store some variables for ease later
N = length(x0);
w = 1:N/2+1;

% ffts
h0 = fft(x0);
h1 = fft(x1);

% extract positive freq. response
f0 = h0(w);
f1 = h1(w);

% mag response
mag0 = abs(f0);
mag1 = abs(f1);

% phase response
ang0 = unwrap(angle(f0));
ang1 = unwrap(angle(f1));

if (mode == 0)
    target0 = mag0;
    target1 = mag1;
elseif (mode == 1)
    target0 = ang0;
    target1 = ang1;
elseif (mode == 2)
    target0 = mag0;
    target1 = mag1;
end


% find local minima/maxima peaks on mag response
[pp0,lp0] = findpeaks(target0);
[pn0,ln0] = findpeaks(-target0);
[pp1,lp1] = findpeaks(target1);
[pn1,ln1] = findpeaks(-target1);

% collect local minima/maxima peaks into single lists
% (peaks & troughs)
p0 = [target0(1); pp0(:); -pn0(:); target0(end)];
l0 = [1; lp0(:); ln0(:); N/2+1];
p1 = [target1(1); pp1(:); -pn1(:); target1(end)];
l1 = [1; lp1(:); ln1(:); N/2+1];

% sort lists by index order
[l0,idx0] = sort(l0,'ascend');
[l1,idx1] = sort(l1,'ascend');
p0 = p0(idx0);
p1 = p1(idx1);

% compute alignment of lists via Needleman-Wunsch-ish algorithm
[lin0,lin1] = get_alignment(l0,p0,l1,p1,1,mag_weighting);

if (mode == 2)
    target2 = ang0;
    target3 = ang1;
    [pp2,lp2] = findpeaks(target2);
    [pn2,ln2] = findpeaks(-target2);
    [pp3,lp3] = findpeaks(target3);
    [pn3,ln3] = findpeaks(-target3);
    
    p2 = [target2(1); pp2(:); -pn2(:); target2(end)];
    l2 = [1; lp2(:); ln2(:); N/2+1];
    p3 = [target3(1); pp3(:); -pn3(:); target3(end)];
    l3 = [1; lp3(:); ln3(:); N/2+1];
    
    [l2,idx2] = sort(l2,'ascend');
    [l3,idx3] = sort(l3,'ascend');
    p2 = p2(idx2);
    p3 = p3(idx3);

    [lin2,lin3] = get_alignment(l2,p2,l3,p3,1,mag_weighting);
end

if (mode ~= 2)
    fn = morph( lin0, lin1, f0, f1, alpha );
else
    magn = morph(lin0,lin1,target0,target1,alpha);
    angn = morph(lin2,lin3,target2,target3,alpha);
    fn = magn .* (cos(angn) + 1i*sin(angn));
end

hn = [fn(:); fn(end-1:-1:2)];
xn = ifft(hn,'symmetric');

end

function [ z0, z1 ] = get_alignment( x0, y0, x1, y1, alpha, beta )

dist = zeros(length(x0),length(x1));
for i=1:length(x0)
    for j=1:length(x1)
        dist(i,j) = sqrt(alpha*(x0(i) - x1(j)).^2 + beta*(y0(i) - y1(j)).^2);
    end
end

% navigate db_dist via NW algo
nw_mat = zeros(length(x0),length(x1));

% set front edges
for i=2:length(x0)
    nw_mat(i,1) = nw_mat(i-1,1) - dist(i,1);
end
for j=2:length(x1)
    nw_mat(1,j) = nw_mat(1,j-1) - dist(1,j);
end
for i=2:length(x0)
    for j=2:length(x1)
        d0 = nw_mat(i-1,j) - dist(i,j);
        d1 = nw_mat(i,j-1) - dist(i,j);
        nw_mat(i,j) = max(d0,d1);
    end
end

% crawl up back to beginning
i=length(x0);
j=length(x1);
z0 = [x0(i)];
z1 = [x1(j)];
while (i > 1 || j > 1)

    % pick a direction
    if (i > 1 && j > 1)
        dLeft = dist(i-1,j);
        dUp = dist(i,j-1);
        dDiag = dist(i-1,j-1);
        if (dDiag <= dLeft && dDiag <= dUp)
            i = i - 1;
            j = j - 1;
            z0 = [z0; x0(i)];
            z1 = [z1; x1(j)];
        elseif (dLeft <= dUp)
            i = i - 1;
        else
            j = j - 1;
        end
    elseif (i > 1 && j == 1)
        i = i - 1;
    elseif (i == 1 && j > 1)
        j = j - 1;
    end
end

z0 = [z0; x0(1)];
z1 = [z1; x1(1)];

z0 = z0(end:-1:1);
z1 = z1(end:-1:1);

end

function [ y ] = morph( lin0, lin1, x0, x1, alpha )

% verify lin0 and lin1 are same length
if (length(lin0) ~= length(lin1))
    return;
end

% allocate output
y = zeros(length(x1),1);

% determine subsample access points for each sample in output
xn = 1;
while (xn < length(y))
    
    for lin_idx=1:length(lin0)
        if xn < lin0(lin_idx)
            break;
        end
    end
    
    xn_min = ((1 - alpha) * lin0(lin_idx-1) + alpha * lin1(lin_idx-1));
    xn_max = ((1 - alpha) * lin0(lin_idx) + alpha * lin1(lin_idx));
    
    for i=round(xn_min):round(xn_max)

        pos0 = (i - xn_min) / (xn_max - xn_min) * (lin0(lin_idx) - lin0(lin_idx-1)) + lin0(lin_idx-1);
        pos1 = (i - xn_min) / (xn_max - xn_min) * (lin1(lin_idx) - lin1(lin_idx-1)) + lin1(lin_idx-1);
        
        xi0 = lookup_subsample(x0, pos0);
        xi1 = lookup_subsample(x1, pos1);
        
        y(i) = (1 - alpha) * xi0 + alpha * xi1;

    end    

    xn = lin0(lin_idx);
    
end

end

function [ val ] = lookup_subsample( x, pos )

w = (1:length(x))';
% val = interp1(w,x,pos);
val = sum(sinc(pos - w) .* x(:));

end