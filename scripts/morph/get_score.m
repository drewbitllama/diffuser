function [ z0, z1 ] = get_score( x0, y0, x1, y1, alpha, beta )

dist = zeros(length(x0),length(x1));
for i=1:length(x0)
    for j=1:length(x1)
        dist(i,j) = sqrt(alpha*(x0(i) - x1(j)).^2 + beta*(y0(i) - y1(j)).^2);
    end
end

db_dist = 20*log10(abs(dist));

% navigate db_dist via NW algo
nw_mat = zeros(length(x0),length(x1));

% set front edges
for i=2:length(x0)
    nw_mat(i,1) = nw_mat(i-1,1) - dist(i,1);
end
for j=2:length(x1)
    nw_mat(1,j) = nw_mat(1,j-1) - dist(1,j);
end
for i=2:length(x0)
    for j=2:length(x1)
        d0 = nw_mat(i-1,j) - dist(i,j);
        d1 = nw_mat(i,j-1) - dist(i,j);
        nw_mat(i,j) = max(d0,d1);
    end
end

% crawl up back to beginning
i=length(x0);
j=length(x1);
ticks = zeros(length(x0),length(x1));
z0 = [x0(i)];
z1 = [x1(j)];
while (i > 1 || j > 1)
    % set current value
    ticks(i,j) = 1;
    
    % pick a direction
    if (i > 1 && j > 1)
        dLeft = dist(i-1,j);
        dUp = dist(i,j-1);
        dDiag = dist(i-1,j-1);
        if (dDiag <= dLeft && dDiag <= dUp)
            i = i - 1;
            j = j - 1;
            z0 = [z0; x0(i)];
            z1 = [z1; x1(j)];
        elseif (dLeft <= dUp)
            i = i - 1;
        else
            j = j - 1;
        end
    elseif (i > 1 && j == 1)
        i = i - 1;
    elseif (i == 1 && j > 1)
        j = j - 1;
    end
end
ticks(1,1) = 1;
z0 = [z0; x0(1)];
z1 = [z1; x1(1)];

z0 = z0(end:-1:1);
z1 = z1(end:-1:1);

end
