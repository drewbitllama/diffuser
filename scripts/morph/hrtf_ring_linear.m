clearvars;
clc;

subject = 126;
filename = sprintf('../CIPIC_hrtf_database/standard_hrir_database/subject_%03d/hrir_final.mat',subject);
load(filename);

cipic_azis = [-80 -65 -55 -45:5:45 55 65 80];
azis = [cipic_azis (cipic_azis + 180)];

len = 1024;
ring_l = zeros(50,len);
ring_r = zeros(50,len);
del_l = zeros(50,1);
del_r = zeros(50,1);

ring360_l = zeros(360,len);
ring360_r = zeros(360,len);
del360_l = zeros(360,1);
del360_r = zeros(360,1);

out_len = 200;
out_l = zeros(360,out_len);
out_r = zeros(360,out_len);

truncate_hrtfs = 1;

for i=1:25

    if (truncate_hrtfs == 0)
        l0 = squeeze(hrir_l(i,9,:));
        r0 = squeeze(hrir_r(i,9,:));
        l1 = squeeze(hrir_l(26-i,41,:));
        r1 = squeeze(hrir_r(26-i,41,:));
    else
        l0 = squeeze(hrir_l(i,9,round(OnL(i,9)):end));
        r0 = squeeze(hrir_r(i,9,round(OnR(i,9)):end));
        l1 = squeeze(hrir_l(26-i,41,round(OnL(26-i,41)):end));
        r1 = squeeze(hrir_r(26-i,41,round(OnR(26-i,41)):end));
    end
    
    l0 = [l0(:); zeros(len - length(l0), 1)];
    r0 = [r0(:); zeros(len - length(r0), 1)];
    l1 = [l1(:); zeros(len - length(l1), 1)];
    r1 = [r1(:); zeros(len - length(r1), 1)];

    ring_l(i,:) = l0;
    ring_r(i,:) = r0;
    ring_l(25+i,:) = l1;
    ring_r(25+i,:) = r1;
    
    del_l(i) = OnL(i,9);
    del_r(i) = OnR(i,9);
    del_l(25+i) = OnL(26-i,41);
    del_r(25+i) = OnR(26-i,41);
end

degs = zeros(360,1);
for i=1:length(azis)
    
    if (i == length(azis))
        for deg=azis(end):(azis(1)+360)
            alpha = (deg - azis(end)) / ((azis(1)+360) - azis(end));
            deg0 = deg;
            if (deg < 0)
                deg0 = deg + 360;
            end
            del360_l(deg0+1) = (1-alpha)*del_l(end)+alpha*del_l(1);
            del360_r(deg0+1) = (1-alpha)*del_r(end)+alpha*del_r(1);
            ring360_l(deg0+1,:) = (1-alpha)*ring_l(end,:)+alpha*ring_l(1,:);
            ring360_r(deg0+1,:) = (1-alpha)*ring_r(end,:)+alpha*ring_r(1,:);
            degs(deg0+1) = alpha;
        end
    else
        for deg=azis(i):azis(i+1)            
            alpha = (deg - azis(i)) / (azis(i+1) - azis(i));
            deg0 = deg;
            if (deg < 0)
                deg0 = deg + 360;
            end
            del360_l(deg0+1) = (1-alpha)*del_l(i)+alpha*del_l(i+1);
            del360_r(deg0+1) = (1-alpha)*del_r(i)+alpha*del_r(i+1);
            ring360_l(deg0+1,:) = (1-alpha)*ring_l(i,:)+alpha*ring_l(i+1,:);
            ring360_r(deg0+1,:) = (1-alpha)*ring_r(i,:)+alpha*ring_r(i+1,:);
            degs(deg0+1) = alpha;
        end
    end
    
end

% shfit and truncate and re-apply onset delay
for i=1:360
    
    l = add_fractional_delay(ring360_l(i,:), del360_l(i), 1);
    r = add_fractional_delay(ring360_r(i,:), del360_r(i), 1);
    
    out_l(i,:) = [l(1:min(out_len,length(l))); zeros(out_len - length(l),1)]';
    out_r(i,:) = [r(1:min(out_len,length(l))); zeros(out_len - length(r),1)]';
    
end

fs = 44100;
burst = rand(round(fs*0.1),1)*2-1;
burst = burst .* linspace(1,0,length(burst))';

audio_out_l = [];
audio_out_r = [];
for i=1:360
    audio_out_l = [audio_out_l; conv(burst, out_l(i,:))];
    audio_out_r = [audio_out_r; conv(burst, out_r(i,:))];
end
audio_out = [audio_out_l audio_out_r];
audio_out = audio_out ./ max(max(abs(audio_out)));
% playblocking(audioplayer(audio_out,fs));
audiowrite(sprintf('subject_%03d_linear.wav',subject),audio_out,fs);
