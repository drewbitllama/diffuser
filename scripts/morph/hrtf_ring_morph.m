clearvars;
clc;

use_morph = 0;
use_min_phase = 0;
use_min_phase_post = 0;

truncate_hrtfs = 0;

generate_hrirs = 1;
generate_sweep = 0;
sweep_speed = 18; % in seconds

mag_weighting = 1000;

subject = 27;

deg_step = 1;

filename = sprintf('../CIPIC_hrtf_database/standard_hrir_database/subject_%03d/hrir_final.mat',subject);
load(filename);

cipic_azis = [-80 -65 -55 -45:5:45 55 65 80];
azis = [cipic_azis (cipic_azis + 180)];

len = 1024;
ring_l = zeros(50,len);
ring_r = zeros(50,len);
del_l = zeros(50,1);
del_r = zeros(50,1);

steps360 = round(360/deg_step);
ring360_l = zeros(steps360,len);
ring360_r = zeros(steps360,len);
del360_l = zeros(steps360,1);
del360_r = zeros(steps360,1);

out_len = 200; % same length as CIPIC input
out_l = zeros(steps360,out_len);
out_r = zeros(steps360,out_len);

for i=1:25

    l0 = squeeze(hrir_l(i,9,:));
    r0 = squeeze(hrir_r(i,9,:));
    l1 = squeeze(hrir_l(26-i,41,:));
    r1 = squeeze(hrir_r(26-i,41,:));
    
    if (truncate_hrtfs == 0)
        l0 = remove_fractional_delay(l0, OnL(i,9));
        r0 = remove_fractional_delay(r0, OnR(i,9));
        l1 = remove_fractional_delay(l1, OnL(26-i,41));
        r1 = remove_fractional_delay(r1, OnR(26-i,41));
    end
    
    l0 = [l0(:); zeros(len - length(l0), 1)];
    r0 = [r0(:); zeros(len - length(r0), 1)];
    l1 = [l1(:); zeros(len - length(l1), 1)];
    r1 = [r1(:); zeros(len - length(r1), 1)];

    if (use_min_phase == 1)
        l0 = mps(l0);
        r0 = mps(r0);
        l1 = mps(l1);
        r1 = mps(r1);
    end
    
    ring_l(i,:) = l0;
    ring_r(i,:) = r0;
    ring_l(25+i,:) = l1;
    ring_r(25+i,:) = r1;
    
    del_l(i) = OnL(i,9);
    del_r(i) = OnR(i,9);
    del_l(25+i) = OnL(26-i,41);
    del_r(25+i) = OnR(26-i,41);
end

degs = zeros(360,1);
for i=1:length(azis)
    
    if (i == length(azis))
        for deg=azis(end):deg_step:(azis(1)+360)
            alpha = (deg - azis(end)) / ((azis(1)+360) - azis(end));
            
            deg0 = round(deg/deg_step);
            if (deg < 0)
                deg0 = deg0 + steps360;
            end
            
            del360_l(deg0+1) = (1-alpha)*del_l(end)+alpha*del_l(1);
            del360_r(deg0+1) = (1-alpha)*del_r(end)+alpha*del_r(1);
            
            if (use_morph == 1)
                ring360_l(deg0+1,:) = get_morph_between(ring_l(end,:),ring_l(1,:),alpha,mag_weighting);
                ring360_r(deg0+1,:) = get_morph_between(ring_r(end,:),ring_r(1,:),alpha,mag_weighting);
            else
                ring360_l(deg0+1,:) = (1-alpha)*ring_l(end,:)+alpha*ring_l(1,:);
                ring360_r(deg0+1,:) = (1-alpha)*ring_r(end,:)+alpha*ring_r(1,:);
            end
            
            degs(deg0+1) = alpha;
        end
    else
        for deg=azis(i):deg_step:azis(i+1)            
            alpha = (deg - azis(i)) / (azis(i+1) - azis(i));
            
            deg0 = round(deg/deg_step);
            if (deg < 0)
                deg0 = deg0 + steps360;
            end
            
            del360_l(deg0+1) = (1-alpha)*del_l(i)+alpha*del_l(i+1);
            del360_r(deg0+1) = (1-alpha)*del_r(i)+alpha*del_r(i+1);
            
            if (use_morph == 1)
                ring360_l(deg0+1,:) = get_morph_between(ring_l(i,:),ring_l(i+1,:),alpha,mag_weighting);
                ring360_r(deg0+1,:) = get_morph_between(ring_r(i,:),ring_r(i+1,:),alpha,mag_weighting);
            else
                ring360_l(deg0+1,:) = (1-alpha)*ring_l(i,:)+alpha*ring_l(i+1,:);
                ring360_r(deg0+1,:) = (1-alpha)*ring_r(i,:)+alpha*ring_r(i+1,:);
            end
            
            degs(deg0+1) = alpha;
        end
    end
    
end

% shfit and truncate and re-apply onset delay and convert to minimum-phase
for i=1:steps360
    
    l = ring360_l(i,:);
    r = ring360_r(i,:);
    
    if (truncate_hrtfs == 1)
        l = add_fractional_delay(l, del360_l(i), 1);
        r = add_fractional_delay(r, del360_r(i), 1);
    end
    
    out_l(i,:) = [l(1:min(out_len,length(l))); zeros(out_len - length(l),1)]';
    out_r(i,:) = [r(1:min(out_len,length(l))); zeros(out_len - length(r),1)]';
    
    if (use_min_phase_post == 1)
        out_l(i,:) = mps(out_l(i,:));
        out_r(i,:) = mps(out_r(i,:));
    end
end

fs = 44100;

if (use_morph == 1)
    morph_mode = 'morph';
else
    morph_mode = 'linear';
end
if (use_min_phase == 1)
    morph_mode = sprintf('%s_minphase',morph_mode);
end
if (truncate_hrtfs == 1)
    morph_mode = sprintf('%s_trunc',morph_mode);
end
if (use_min_phase_post == 1)
    morph_mode = sprintf('%s_minphasepost',morph_mode);
end

filename = sprintf('./output/cipic_%03d_%s',subject,morph_mode);

% generate HRIRs (1-degree each)
if (generate_hrirs == 1)
    normalize = 1/max(max(max(abs(out_l))),max(max(abs(out_r))));
    for i=1:steps360
        audiowrite(sprintf('%s_a%03d.wav',filename,(i-1)),normalize*[out_l(i,:)' out_r(i,:)'],fs);
    end
end

if (generate_sweep == 1)
    
    burst_len = round(fs*sweep_speed/steps360);

    conv_output_l = zeros(burst_len + out_len - 1, 1);
    conv_output_r = zeros(burst_len + out_len - 1, 1);

    audio_out_l = [];
    audio_out_r = [];
    for i=1:steps360
        burst = rand(burst_len,1)*2-1;
    %     burst = burst .* linspace(1,0,burst_len)';

        current_conv_output_l = conv(burst, out_l(i,:));
        current_conv_output_r = conv(burst, out_r(i,:));

        audio_out_l = [audio_out_l; current_conv_output_l(1:burst_len-1) + conv_output_l(end-(burst_len-1)+1:end)];
        audio_out_r = [audio_out_r; current_conv_output_r(1:burst_len-1) + conv_output_r(end-(burst_len-1)+1:end)];

        conv_output_l = current_conv_output_l;
        conv_output_r = current_conv_output_r;
    end
    audio_out = [audio_out_l audio_out_r];
    audio_out = audio_out ./ max(max(abs(audio_out)));

    audiowrite(sprintf('%s_sweep.wav',filename),audio_out,fs);
end

