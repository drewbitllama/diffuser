function [ p ] = interp_phase( h0, h1, alpha )

ang0 = unwrap(angle(h0));
ang1 = unwrap(angle(h1));

p = (1-alpha)*ang0 + alpha*ang1;

end

