% cleanup workspace
clearvars;
clc;

% load CIPIC database
subject = 27;
filename = sprintf('../CIPIC_hrtf_database/standard_hrir_database/subject_%03d/hrir_final.mat',subject);
load(filename);

% input parameters
sweep_spd = 10;
use_remove_delay = 1;
use_back_filter = 0;
degrees = 360;
weighting = 100;

% compute back filter
[avg_diff_l, avg_diff_r] = frontbackcompare();

% initializers
len = 200;
w = 1:len/2+1;
cipic_azi = [-80 -65 -55 -45:5:45 55 65 80];
azi = [cipic_azi (cipic_azi+180)];

% extract a horz ring from CIPIC
ring_l = zeros(length(azi),len);
ring_r = zeros(length(azi),len);
ring_l_min = ring_l;
ring_r_min = ring_r;
% ring_l_arc = ring_l;
% ring_r_arc = ring_r;
% ring_l_morph = ring_l;
% ring_r_morph = ring_r;
del_l = zeros(length(azi),1);
del_r = zeros(length(azi),1);
for i=1:25
    
    if (use_remove_delay == 1)
        rl0 = squeeze(hrir_l(i,9,floor(OnL(i,9)):end));
        rr0 = squeeze(hrir_r(i,9,floor(OnR(i,9)):end));
        rl1 = squeeze(hrir_l(26-i,41,floor(OnL(26-i,41)):end));
        rr1 = squeeze(hrir_r(26-i,41,floor(OnR(26-i,41)):end));
        rl0 = [rl0(:); zeros(len - length(rl0),1)];
        rr0 = [rr0(:); zeros(len - length(rr0),1)];
        rl1 = [rl1(:); zeros(len - length(rl1),1)];
        rr1 = [rr1(:); zeros(len - length(rr1),1)];
    else
        rl0 = squeeze(hrir_l(i,9,:));
        rr0 = squeeze(hrir_r(i,9,:));
        rl1 = squeeze(hrir_l(26-i,41,:));
        rr1 = squeeze(hrir_r(26-i,41,:));
    end
        
    ring_l_min(i,:) = mps(rl0);
    ring_r_min(i,:) = mps(rr0);
    ring_l_min(i+25,:) = mps(rl1);
    ring_r_min(i+25,:) = mps(rr1);
    
%     ring_l_arc(i,:) = squeeze(hrir_l(i,9,:));
%     ring_r_arc(i,:) = squeeze(hrir_r(i,9,:));
%     ring_l_arc(i+25,:) = squeeze(hrir_l(26-i,41,:));
%     ring_r_arc(i+25,:) = squeeze(hrir_r(26-i,41,:));

    ring_l(i,:) = rl0;
    ring_r(i,:) = rr0;
    ring_l(i+25,:) = rl1;
    ring_r(i+25,:) = rr1;
    
    del_l(i) = OnL(i,9);
    del_r(i) = OnR(i,9);
    del_l(i+length(cipic_azi)) = OnL(length(cipic_azi)+1-i,41);
    del_r(i+length(cipic_azi)) = OnR(length(cipic_azi)+1-i,41);
end
ring_l_arc = ring_l;
ring_r_arc = ring_r;
ring_l_morph = ring_l;
ring_r_morph = ring_r;

if (use_back_filter == 1)
    for i=1:length(cipic_azi)
        h_l = fft(ring_l(26-i,:));
        h_r = fft(ring_r(26-i,:));
        f_l = h_l(w) .* avg_diff_l(i,:);
        f_r = h_r(w) .* avg_diff_r(i,:);
        h_l = [f_l(:); f_l(end-1:-1:2)'];
        h_r = [f_r(:); f_r(end-1:-1:2)'];
        ring_l(i+length(cipic_azi),:) = ifft(h_l,'symmetric');
        ring_r(i+length(cipic_azi),:) = ifft(h_r,'symmetric');
    end
end

deg_step = 360 / degrees;

steps = zeros(length(azi),1);
ring360_l = zeros(degrees,len);
ring360_r = zeros(degrees,len);
ring360_l_min = zeros(degrees,len);
ring360_r_min = zeros(degrees,len);
ring360_l_arc = zeros(degrees,len);
ring360_r_arc = zeros(degrees,len);
ring360_l_morph = zeros(degrees,len);
ring360_r_morph = zeros(degrees,len);
del360_l = zeros(degrees,1);
del360_r = zeros(degrees,1);
idx = 1;
for i=1:50
    if (i < length(azi))
        i_n = i+1;
    else
        i_n = 1;
    end
    steps(i) = azi(i_n) - azi(i);
    if (steps(i) < 0)
        steps(i) = steps(i) + 360;
    end
    steps(i)
    
    for j=0:deg_step:steps(i)-deg_step
        alpha = j / steps(i);
        del360_l(idx) = floor(alpha * del_l(i_n) + (1 - alpha) * del_l(i));
        del360_r(idx) = floor(alpha * del_r(i_n) + (1 - alpha) * del_r(i));
        ring360_l(idx,:) = alpha * ring_l(i_n,:) + (1 - alpha) * ring_l(i,:);
        ring360_r(idx,:) = alpha * ring_r(i_n,:) + (1 - alpha) * ring_r(i,:);
        ring360_l_min(idx,:) = alpha * ring_l_min(i_n,:) + (1 - alpha) * ring_l_min(i,:);
        ring360_r_min(idx,:) = alpha * ring_r_min(i_n,:) + (1 - alpha) * ring_r_min(i,:);        
        ring360_l_morph(idx,:) = get_morph_between(ring_l_morph(i,:),ring_l_morph(i_n,:),alpha,weighting);
        ring360_r_morph(idx,:) = get_morph_between(ring_r_morph(i,:),ring_r_morph(i_n,:),alpha,weighting);
        
        % "arc" interpolation
        hl0 = fft(ring_l_arc(i,:));
        hr0 = fft(ring_r_arc(i,:));
        hl1 = fft(ring_l_arc(i_n,:));
        hr1 = fft(ring_r_arc(i_n,:));
        lml0 = 20*log10(hl0(w));
        lmr0 = 20*log10(hr0(w));
        lml1 = 20*log10(hl1(w));
        lmr1 = 20*log10(hr1(w));
        lmlN = (1-alpha)*lml0+alpha*lml1;
        lmrN = (1-alpha)*lmr0+alpha*lmr1;
        pl0 = angle(hl0(w));
        pr0 = angle(hr0(w));
        pl1 = angle(hl1(w));
        pr1 = angle(hr1(w));
        plN = pl0;
        prN = pr0;
        for k=2:len/2
            if (abs(pl0(k) - pl1(k)) <= pi)
                % do nothing
            elseif (abs((pl0(k) + 2*pi) - pl1(k)) <= pi)
                pl0(k) = pl0(k) + 2*pi;
            elseif (abs(pl0(k) - (pl1(k) + 2*pi)) <= pi)
                pl1(k) = pl1(k) + 2*pi;
            else
                error('phase problems!');
            end
            if (abs(pr0(k) - pr1(k)) <= pi)
                % do nothing
            elseif (abs((pr0(k) + 2*pi) - pr1(k)) <= pi)
                pr0(k) = pr0(k) + 2*pi;
            elseif (abs(pr0(k) - (pr1(k) + 2*pi)) <= pi)
                pr1(k) = pr1(k) + 2*pi;
            else
                error('phase problems!');
            end
            plN(k) = (1-alpha)*pl0(k)+alpha*pl1(k);
            prN(k) = (1-alpha)*pr0(k)+alpha*pr1(k);
        end
        flN = 10.^(lmlN./20).*(cos(plN)+1i.*sin(plN));
        frN = 10.^(lmrN./20).*(cos(prN)+1i.*sin(prN));
        hlN = [flN flN(end-1:-1:2)];
        hrN = [frN frN(end-1:-1:2)];
        ring360_l_arc(idx,:) = ifft(hlN,'symmetric');
        ring360_r_arc(idx,:) = ifft(hrN,'symmetric');
        
        if (use_remove_delay == 1)
            ring360_l(idx,:) = [zeros(1,del360_l(idx)) ring360_l(idx,1:end-del360_l(idx))];
            ring360_r(idx,:) = [zeros(1,del360_r(idx)) ring360_r(idx,1:end-del360_r(idx))];
            ring360_l_min(idx,:) = [zeros(1,del360_l(idx)) ring360_l_min(idx,1:end-del360_l(idx))];
            ring360_r_min(idx,:) = [zeros(1,del360_r(idx)) ring360_r_min(idx,1:end-del360_r(idx))];
            ring360_l_arc(idx,:) = [zeros(1,del360_l(idx)) ring360_l_arc(idx,1:end-del360_l(idx))];
            ring360_r_arc(idx,:) = [zeros(1,del360_r(idx)) ring360_r_arc(idx,1:end-del360_r(idx))];
            ring360_l_morph(idx,:) = [zeros(1,del360_l(idx)) ring360_l_morph(idx,1:end-del360_l(idx))];
            ring360_r_morph(idx,:) = [zeros(1,del360_r(idx)) ring360_r_morph(idx,1:end-del360_r(idx))];
        end
        
        idx = idx + 1;
    end
end

fs = 44100;
burst_len = round(fs * sweep_spd / degrees);

conv_length = burst_len + len - 1;
out = zeros(burst_len * degrees, 2);
out_min = out;
out_arc = out;
out_morph = out;
for i=0:degrees-1    
    
    burst = rand(burst_len,1)*2-1;
    burst_l = conv(burst,ring360_l(i+1,:));
    burst_r = conv(burst,ring360_r(i+1,:));
    burst_l_min = conv(burst,ring360_l_min(i+1,:));
    burst_r_min = conv(burst,ring360_r_min(i+1,:));
    burst_l_arc = conv(burst,ring360_l_arc(i+1,:));
    burst_r_arc = conv(burst,ring360_r_arc(i+1,:));
    burst_l_morph = conv(burst,ring360_l_morph(i+1,:));
    burst_r_morph = conv(burst,ring360_r_morph(i+1,:));

    idx_start = (i*burst_len)+1;
    idx_stop = idx_start+conv_length-1;
    if (idx_stop > size(out,1))
        idx_stop = size(out,1);
    end
    
    out(idx_start:idx_stop,1) = out(idx_start:idx_stop,1) + burst_l(1:idx_stop-idx_start+1);
    out(idx_start:idx_stop,2) = out(idx_start:idx_stop,2) + burst_r(1:idx_stop-idx_start+1);
    out_min(idx_start:idx_stop,1) = out_min(idx_start:idx_stop,1) + burst_l_min(1:idx_stop-idx_start+1);
    out_min(idx_start:idx_stop,2) = out_min(idx_start:idx_stop,2) + burst_r_min(1:idx_stop-idx_start+1);
    out_arc(idx_start:idx_stop,1) = out_arc(idx_start:idx_stop,1) + burst_l_arc(1:idx_stop-idx_start+1);
    out_arc(idx_start:idx_stop,2) = out_arc(idx_start:idx_stop,2) + burst_r_arc(1:idx_stop-idx_start+1);
    out_morph(idx_start:idx_stop,1) = out_morph(idx_start:idx_stop,1) + burst_l_morph(1:idx_stop-idx_start+1);
    out_morph(idx_start:idx_stop,2) = out_morph(idx_start:idx_stop,2) + burst_r_morph(1:idx_stop-idx_start+1);
    
end

audio_normalize = max([max(max(abs(out))) max(max(abs(out_min))) max(max(abs(out_arc))) max(max(abs(out_morph)))]);
audiowrite('sweep_lin.wav',out/audio_normalize,fs);
audiowrite('sweep_min.wav',out_min/audio_normalize,fs);
audiowrite('sweep_arc.wav',out_arc/audio_normalize,fs);
audiowrite('sweep_morph.wav',out_morph/audio_normalize,fs);

% plot linear interpolation of HRTFs
nfft = 8192;
db_w = 1:nfft/2+1;
db_l = zeros(size(ring360_l,1),nfft/2+1);
db_r = zeros(size(ring360_l,1),nfft/2+1);
db_l_min = db_l;
db_r_min = db_r;
db_l_arc = db_l;
db_r_arc = db_r;
db_l_morph = db_l;
db_r_morph = db_r;
for i=1:size(ring360_l,1)
    
    hl = fft([ring360_l(i,:) zeros(1,nfft-len)]);
    hr = fft([ring360_r(i,:) zeros(1,nfft-len)]);
    db_l(i,:) = 20*log10(abs(hl(db_w)));
    db_r(i,:) = 20*log10(abs(hr(db_w)));    

    hl_min = fft([ring360_l_min(i,:) zeros(1,nfft-len)]);
    hr_min = fft([ring360_r_min(i,:) zeros(1,nfft-len)]);
    db_l_min(i,:) = 20*log10(abs(hl_min(db_w)));
    db_r_min(i,:) = 20*log10(abs(hr_min(db_w)));    
    
    hl_arc = fft([ring360_l_arc(i,:) zeros(1,nfft-len)]);
    hr_arc = fft([ring360_r_arc(i,:) zeros(1,nfft-len)]);
    db_l_arc(i,:) = 20*log10(abs(hl_arc(db_w)));
    db_r_arc(i,:) = 20*log10(abs(hr_arc(db_w)));    
    
    hl_morph = fft([ring360_l_morph(i,:) zeros(1,nfft-len)]);
    hr_morph = fft([ring360_r_morph(i,:) zeros(1,nfft-len)]);
    db_l_morph(i,:) = 20*log10(abs(hl_morph(db_w)));
    db_r_morph(i,:) = 20*log10(abs(hr_morph(db_w)));  
    
end

db_l = db_l';
db_r = db_r';
db_l = db_l(end:-1:1,:);
db_r = db_r(end:-1:1,:);

db_l_min = db_l_min';
db_r_min = db_r_min';
db_l_min = db_l_min(end:-1:1,:);
db_r_min = db_r_min(end:-1:1,:);

db_l_arc = db_l_arc';
db_r_arc = db_r_arc';
db_l_arc = db_l_arc(end:-1:1,:);
db_r_arc = db_r_arc(end:-1:1,:);

db_l_morph = db_l_morph';
db_r_morph = db_r_morph';
db_l_morph = db_l_morph(end:-1:1,:);
db_r_morph = db_r_morph(end:-1:1,:);

fig = figure(1);
imagesc(db_l, [-60 20]);
title('Linear (dB) Left');
colormap gray;
% set(gcf,'Units','normal');
% set(gca,'Position',[0 0 1 1]);
% set(gca,'XTickLabel','');
% set(gca,'YTickLabel','');
% set(fig,'Position',[0, 0, degrees, nfft/2+1]);
% drawnow;
% print('db_lin_left.png','-dpng','-r1200');

fig = figure(2);
imagesc(db_r, [-60 20]);
title('Linear (dB) Right');
colormap gray;
% set(gcf,'Units','normal');
% set(gca,'Position',[0 0 1 1]);
% set(gca,'XTickLabel','');
% set(gca,'YTickLabel','');
% set(fig,'Position',[0, 0, degrees, nfft/2+1]);
% drawnow;
% print('db_lin_right.png','-dpng','-r1200');

fig = figure(3);
imagesc(db_l_min, [-60 20]);
title('Min.Phase (dB) Left');
colormap gray;
% set(gcf,'Units','normal');
% set(gca,'Position',[0 0 1 1]);
% set(gca,'XTickLabel','');
% set(gca,'YTickLabel','');
% set(fig,'Position',[0, 0, degrees, nfft/2+1]);
% drawnow;
% print('db_min_left.png','-dpng','-r1200');

fig = figure(4);
imagesc(db_r_min, [-60 20]);
title('Min.Phase (dB) Right');
colormap gray;
% set(gcf,'Units','normal');
% set(gca,'Position',[0 0 1 1]);
% set(gca,'XTickLabel','');
% set(gca,'YTickLabel','');
% set(fig,'Position',[0, 0, degrees, nfft/2+1]);
% drawnow;
% print('db_min_right.png','-dpng','-r1200');

fig = figure(5);
imagesc(db_l_arc, [-60 20]);
title('Arc (dB) Left');
colormap gray;
set(gcf,'Units','normal');
set(gca,'Position',[0 0 1 1]);
set(gca,'XTickLabel','');
set(gca,'YTickLabel','');
set(fig,'Position',[0, 0, degrees, nfft/2+1]);
drawnow;
print('db_arc_left.png','-dpng','-r1200');

fig = figure(6);
imagesc(db_r_arc, [-60 20]);
title('Arc (dB) Right');
colormap gray;
% set(gcf,'Units','normal');
% set(gca,'Position',[0 0 1 1]);
% set(gca,'XTickLabel','');
% set(gca,'YTickLabel','');
% set(fig,'Position',[0, 0, degrees, nfft/2+1]);
% drawnow;
% print('db_arc_right.png','-dpng','-r1200');

fig = figure(7);
imagesc(db_l_morph, [-60 20]);
title('Arc (dB) Left');
colormap gray;
% set(gcf,'Units','normal');
% set(gca,'Position',[0 0 1 1]);
% set(gca,'XTickLabel','');
% set(gca,'YTickLabel','');
% set(fig,'Position',[0, 0, degrees, nfft/2+1]);
% drawnow;
% print('db_morph_left.png','-dpng','-r1200');

fig = figure(8);
imagesc(db_r_morph, [-60 20]);
title('Arc (dB) Right');
colormap gray;
% set(gcf,'Units','normal');
% set(gca,'Position',[0 0 1 1]);
% set(gca,'XTickLabel','');
% set(gca,'YTickLabel','');
% set(fig,'Position',[0, 0, degrees, nfft/2+1]);
% drawnow;
% print('db_morph_right.png','-dpng','-r1200');

fig = figure(9);
db_ring_l = zeros(size(ring_l,1),len/2+1);
db_ring_r = db_ring_l;
db_ring_l_min = db_ring_l;
db_ring_r_min = db_ring_l;
for i=1:size(ring_l,1)
    hl = 20*log10(abs(fft(ring_l(i,:))));
    hr = 20*log10(abs(fft(ring_r(i,:))));
    hl_min = 20*log10(abs(fft(ring_l_min(i,:))));
    hr_min = 20*log10(abs(fft(ring_r_min(i,:))));
    db_ring_l(i,:) = hl(w);
    db_ring_r(i,:) = hr(w);
    db_ring_l_min(i,:) = hl_min(w);
    db_ring_r_min(i,:) = hr_min(w);
end

db_ring_l = db_ring_l';
db_ring_r = db_ring_r';
db_ring_l_min = db_ring_l_min';
db_ring_r_min = db_ring_r_min';
db_ring_l = db_ring_l(end:-1:1,:);
db_ring_r = db_ring_r(end:-1:1,:);
db_ring_l_min = db_ring_l_min(end:-1:1,:);
db_ring_r_min = db_ring_r_min(end:-1:1,:);

subplot(2,2,1);
imagesc(db_ring_l, [-60 20]);
colormap gray;
subplot(2,2,2);
imagesc(db_ring_r, [-60 20]);
colormap gray;
subplot(2,2,3);
imagesc(db_ring_l_min, [-60 20]);
colormap gray;
subplot(2,2,4);
imagesc(db_ring_r_min, [-60 20]);
colormap gray;

fig = figure(10);
subplot(1,2,1);
imagesc(db_ring_l - db_ring_l_min, [-60 20]);
colormap gray;
subplot(1,2,2);
imagesc(db_ring_r - db_ring_r_min, [-60 20]);
colormap gray;

fig = figure(11);
subplot(1,2,1);
imagesc(10.^(db_l/20) - 10.^(db_l_morph/20), [0 1]);
colormap jet;
subplot(1,2,2);
imagesc(10.^(db_r /20) - 10.^(db_r_morph/20), [0 1]);
colormap jet;

close all;
