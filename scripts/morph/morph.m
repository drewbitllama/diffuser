function [ y ] = morph( lin0, lin1, x0, x1, alpha )

% verify lin0 and lin1 are same length
if (length(lin0) ~= length(lin1))
    return;
end

% allocate output
y = zeros(length(x1),1);

% determine subsample access points for each sample in output
xn = 1;
while (xn < length(y))
    
    for lin_idx=1:length(lin0)
        if xn < lin0(lin_idx)
            break;
        end
    end
    
    xn_min = ((1 - alpha) * lin0(lin_idx-1) + alpha * lin1(lin_idx-1));
    xn_max = ((1 - alpha) * lin0(lin_idx) + alpha * lin1(lin_idx));
    
    for i=round(xn_min):round(xn_max)

        pos0 = (i - xn_min) / (xn_max - xn_min) * (lin0(lin_idx) - lin0(lin_idx-1)) + lin0(lin_idx-1);
        pos1 = (i - xn_min) / (xn_max - xn_min) * (lin1(lin_idx) - lin1(lin_idx-1)) + lin1(lin_idx-1);
        
        xi0 = lookup_subsample(x0, pos0);
        xi1 = lookup_subsample(x1, pos1);
        
        y(i) = (1 - alpha) * xi0 + alpha * xi1;

    end    

    xn = lin0(lin_idx);
    
end

end

function [ val ] = lookup_subsample( x, pos )

w = (1:length(x))';
% val = interp1(w,x,pos);
val = sum(sinc(pos - w) .* x(:));

end
