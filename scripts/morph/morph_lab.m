clearvars;
clc;

subject = 3;
filename = sprintf('../CIPIC_hrtf_database/standard_hrir_database/subject_%03d/hrir_final.mat',subject);
load(filename);

azi = 6;
ele = 13;

use_min_phase = 0;
interp_across_azi = 0; % set to 0 to interp across ele, 2 to interp across both

len = 8192;
mag_weighting = 10;
alpha = 0.5;

crop_len = 200;

if (interp_across_azi == 1)
    x0 = squeeze(hrir_l(azi-1,ele,:));
    x1 = squeeze(hrir_l(azi+1,ele,:));
    x2 = x0;
    x3 = x1;
elseif (interp_across_azi == 0)
    x0 = squeeze(hrir_l(azi,ele-1,:));
    x1 = squeeze(hrir_l(azi,ele+1,:));
    x2 = x0;
    x3 = x1;
elseif (interp_across_azi == 2)
    x0 = squeeze(hrir_l(azi-1,ele,:));
    x1 = squeeze(hrir_l(azi+1,ele,:));
    x2 = squeeze(hrir_l(azi,ele-1,:));
    x3 = squeeze(hrir_l(azi,ele+1,:));    
end 
xT = squeeze(hrir_l(azi,ele,:));

if (use_min_phase == 1)
    xT = mps(xT);
    x0 = mps(x0);
    x1 = mps(x1);
    x2 = mps(x2);
    x3 = mps(x3);
end

x0 = [x0(:); zeros(len - length(x0), 1)];
x1 = [x1(:); zeros(len - length(x1), 1)];
x2 = [x2(:); zeros(len - length(x2), 1)];
x3 = [x3(:); zeros(len - length(x3), 1)];

xT = [xT(:); zeros(len - length(xT), 1)];
xLin = (x0 + x1) / 2;

h0 = fft(x0);
h1 = fft(x1);
h2 = fft(x2);
h3 = fft(x3);

hT = fft(xT);
hLin = fft(xLin);

w = 1:(len/2+1);

m0 = abs(h0(w));
m1 = abs(h1(w));
m2 = abs(h2(w));
m3 = abs(h3(w));

mT = abs(hT(w));
mLin = abs(hLin(w));
mCplx = abs((m0 + m1) / 2);

% find local minima/maxima peaks on mag response
[pp0,lp0] = findpeaks(m0);
[pn0,ln0] = findpeaks(-m0);
[pp1,lp1] = findpeaks(m1);
[pn1,ln1] = findpeaks(-m1);
[pp2,lp2] = findpeaks(m2);
[pn2,ln2] = findpeaks(-m2);
[pp3,lp3] = findpeaks(m2);
[pn3,ln3] = findpeaks(-m2);

% collect local minima/maxima peaks into single lists
% (peaks & troughs)
p0 = [m0(1); pp0(:); -pn0(:); m0(end)];
l0 = [1; lp0(:); ln0(:); len/2+1];
p1 = [m1(1); pp1(:); -pn1(:); m1(end)];
l1 = [1; lp1(:); ln1(:); len/2+1];
p2 = [m2(1); pp2(:); -pn2(:); m2(end)];
l2 = [1; lp2(:); ln2(:); len/2+1];
p3 = [m3(1); pp3(:); -pn3(:); m3(end)];
l3 = [1; lp3(:); ln3(:); len/2+1];

% sort lists by index order
[l0,idx0] = sort(l0,'ascend');
[l1,idx1] = sort(l1,'ascend');
[l2,idx2] = sort(l2,'ascend');
[l3,idx3] = sort(l3,'ascend');
p0 = p0(idx0);
p1 = p1(idx1);
p2 = p2(idx2);
p3 = p3(idx3);

% compute alignment of lists via Needleman-Wunsch-ish algorithm
[lin0,lin1] = get_alignment(l0,p0,l1,p1,1,mag_weighting);
[lin2,lin3] = get_alignment(l2,p2,l3,p3,1,mag_weighting);
mMorph0 = morph(lin0,lin1,m0,m1,alpha);
mMorph1 = morph(lin2,lin3,m2,m3,alpha);
mMorph = (mMorph0 + mMorph1) / 2;

p0 = unwrap(angle(h0(w)));
p1 = unwrap(angle(h1(w)));
p2 = unwrap(angle(h2(w)));
p3 = unwrap(angle(h3(w)));

pCplx0 = (p0 + p1) / 2;
pCplx1 = (p2 + p3) / 2;
pMorph = (pCplx0 + pCplx1) / 2;

fMorph = mMorph.*(cos(pMorph)+1i*sin(pMorph));
hMorph = [fMorph(:); fMorph(end-1:-1:2)];
xMorph = ifft(hMorph,'symmetric');

xLin = (x0 + x1 + x2 + x3) / 4;

t_crop = 1:crop_len;
w_crop = 1:(crop_len/2+1);

xT = xT(:);
xMorph = xMorph(:);
xT_del = round(OnL(azi,ele));
if (interp_across_azi == 1)
    xApprox_del = round((OnL(azi-1,ele)+OnL(azi+1,ele))/2);
elseif (interp_across_azi == 0)
    xApprox_del = round((OnL(azi,ele-1)+OnL(azi,ele+1))/2);
elseif (interp_across_azi == 2)
    xApprox_del = round((OnL(azi-1,ele)+OnL(azi+1,ele)+OnL(azi,ele-1)+OnL(azi,ele+1))/4);
end

xT_crop = xT(1:crop_len);
xMorph_crop = xMorph(1:crop_len);
xLin_crop = xLin(1:crop_len);

% if (use_min_phase == 1)
%     xT_crop = [zeros(xT_del,1); xT(1:crop_len-xT_del)];
%     xMorph_crop = [zeros(xApprox_del,1); xMorph(1:crop_len-xApprox_del)];
%     xLin_crop = [zeros(xApprox_del,1); xLin(1:crop_len-xApprox_del)];
% else
%     xT_crop = [zeros(xT_del,1); xT(xT_del+1:crop_len)];
%     xMorph_crop = [zeros(xApprox_del,1); xMorph(xApprox_del+1:crop_len)];
%     xLin_crop = [zeros(xApprox_del,1); xLin(xApprox_del+1:crop_len)];
% end

hT_crop = fft(xT_crop);
dbT_crop = 20*log10(abs(hT_crop(w_crop)));
pT_crop = unwrap(angle(hT_crop(w_crop)));

hMorph_crop = fft(xMorph_crop);
dbMorph_crop = 20*log10(abs(hMorph_crop(w_crop)));
pMorph_crop = unwrap(angle(hMorph_crop(w_crop)));

hLin_crop = fft(xLin_crop);
dbLin_crop = 20*log10(abs(hLin_crop(w_crop)));
pLin_crop = unwrap(angle(hLin_crop(w_crop)));

lw = 1.5;
figure(1);
subplot(3,1,1);
plot(t_crop,xT_crop,'LineWidth',lw);
hold on;
plot(t_crop,xMorph_crop,t_crop,xLin_crop);
hold off;
xlim([1 crop_len]);
ylim([-1 1]);
subplot(3,1,2);
plot(w_crop,dbT_crop,'LineWidth',lw);
hold on;
plot(w_crop,dbMorph_crop,w_crop,dbLin_crop);
hold off;
xlim([1 crop_len/2+1]);
ylim([-60 20]);
subplot(3,1,3);
plot(w_crop,pT_crop,'LineWidth',lw);
hold on;
plot(w_crop,pMorph_crop,w_crop,pLin_crop);
hold off;
xlim([1 crop_len/2+1]);
pmin = min([pT_crop; pMorph_crop; pLin_crop]);
pmax = max([pT_crop; pMorph_crop; pLin_crop]);
ylim([pmin pmax]);
