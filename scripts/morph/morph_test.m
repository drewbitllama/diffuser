clearvars;
clc;

subject = 3;
filename = sprintf('../CIPIC_hrtf_database/standard_hrir_database/subject_%03d/hrir_final.mat',subject);
load(filename);

azi0 = 1;
ele0 = 1;

azi1 = 1;
ele1 = 41;

len = 1024;
t = 1:len;
w = 1:(len/2+1);
x0 = squeeze(hrir_l(azi0,ele0,round(OnL(azi0,ele0)):end));
x1 = squeeze(hrir_l(azi1,ele1,round(OnL(azi1,ele1)):end));
x0 = [x0(:); zeros(len - length(x0), 1)];
x1 = [x1(:); zeros(len - length(x1), 1)];

h0 = fft(x0);
h1 = fft(x1);

f0 = h0(w);
f1 = h1(w);

mag0 = abs(f0);
mag1 = abs(f1);
db0 = 20*log10(mag0);
db1 = 20*log10(mag1);

ang0 = unwrap(angle(f0));
ang1 = unwrap(angle(f1));

[pp0,lp0] = findpeaks(mag0);
[pn0,ln0] = findpeaks(-mag0);
[pp1,lp1] = findpeaks(mag1);
[pn1,ln1] = findpeaks(-mag1);

p0 = [x0(1); pp0(:); -pn0(:); x0(end)];
l0 = [1; lp0(:); ln0(:); len/2+1];
p1 = [x1(1); pp1(:); -pn1(:); x1(end)];
l1 = [1; lp1(:); ln1(:); len/2+1];

[l0,idx0] = sort(l0,'ascend');
[l1,idx1] = sort(l1,'ascend');
p0 = p0(idx0);
p1 = p1(idx1);

[z0,z1] = get_score(l0,p0,l1,p1,1,100);

% v = VideoWriter('morph.avi');
% v.FrameRate = 10;
% 
% open(v);

% y = morph(z0,z1,mag0,mag1,0);
% y_db = 20*log10(abs(y));
% 
% figure(3);
% plot(w,db0,'--','Color',[1 0.5 0.5]);
% hold on;
% plot(w,db1,'--','Color',[0.5 0.5 1]);
% plot(w,y,'k');
% hold off;
% xlim([1 len/2+1]);
% ylim([-20 15]);
% drawnow;

% for i=1:10
%     writeVideo(v,getframe);
% end

for alpha=[0:0.01:1]
    
    y = morph(z0,z1,mag0,mag1,alpha);
    y_db = 20*log10(abs(y));
    ang = interp_phase(f0, f1, alpha);
    
    mag_r = [y(:); y(end-1:-1:2)];
    ang_r = [ang(:); ang(end-1:-1:2)];
%     h_r = mag_r .* (cos(ang_r) + sqrt(-1) .* sin(ang_r));

    f_r = morph(z0,z1,f0,f1,alpha);
    h_r = [f_r(:); f_r(end-1:-1:2)];
    x_r = ifft(h_r,'symmetric');
    x_r = [x_r(1:200); zeros(len - 200, 1)];
    h_rr = fft(x_r);
    f_rr = h_rr(w);
    ang_rr = unwrap(angle(f_rr));
    y_r_db = 20*log10(abs(f_rr));
    
    figure(2);
    subplot(3,1,1);
    plot(t,x0,'--','Color',[1 0.5 0.5]);
    hold on;
    plot(t,x1,'--','Color',[0.5 0.5 1]);
    plot(t,x_r,'k');
    xlim([1 200]);
    ylim([-1 1]);
    hold off;

    subplot(3,1,2);
    plot(w,db0,'--','Color',[1 0.5 0.5]);
    hold on;
    plot(w,db1,'--','Color',[0.5 0.5 1]);
    plot(w,y_db,'b');
    plot(w,y_r_db,'r');
    hold off;
    xlim([1 len/2+1]);
    ylim([-20 15]);

    subplot(3,1,3);
    plot(w,ang0,'--','Color',[1 0.5 0.5]);
    hold on;
    plot(w,ang1,'--','Color',[0.5 0.5 1]);
    plot(w,ang,'b');
    plot(w,ang_rr,'r');
    hold off;
    xlim([1 len/2+1]);
    ylim([-20 15]);    
%     writeVideo(v,getframe);
    pause(0.1);
    
    alpha
    
end

% for i=1:10
%     writeVideo(v,getframe);
% end

% close(v);
% % figure(2);
% plot(w,db0,'b-');
% hold on;
% plot(w,db1,'r-');
% plot(z0,db0(z0),'o');
% plot(z1,db1(z1),'o');
% hold off;


