clearvars;
clc;

subject = 3;
filename = sprintf('../CIPIC_hrtf_database/standard_hrir_database/subject_%03d/hrir_final.mat',subject);
load(filename);

azi0 = 1;
azi1 = 2;

ele0 = 9;
ele1 = 9;

len = 200;
t = 1:len;
x0 = squeeze(hrir_l(azi0,ele0,round(OnL(azi0,ele0)):end));
x1 = squeeze(hrir_l(azi1,ele1,round(OnL(azi1,ele1)):end));
x0 = [x0(:); zeros(len - length(x0),1)];
x1 = [x1(:); zeros(len - length(x1),1)];

w = (1:(len/2+1))';
h0 = fft(x0);
h1 = fft(x1);
h0 = h0(w);
h1 = h1(w);

mag0 = abs(h0);
ang0 = unwrap(angle(h0));

mag1 = abs(h1);
ang1 = unwrap(angle(h1));

[pp0,lp0] = findpeaks(ang0);
[pn0,ln0] = findpeaks(-ang0);
[pp1,lp1] = findpeaks(ang1);
[pn1,ln1] = findpeaks(-ang1);

p0 = [ang0(1); pp0(:); -pn0(:); ang0(end)];
l0 = [1; lp0(:); ln0(:); len/2+1];
p1 = [ang1(1); pp1(:); -pn1(:); ang1(end)];
l1 = [1; lp1(:); ln1(:); len/2+1];

[l0,idx0] = sort(l0,'ascend');
[l1,idx1] = sort(l1,'ascend');
p0 = p0(idx0);
p1 = p1(idx1);

[z0,z1] = get_score(l0,p0,l1,p1,1,1);

for alpha=[0:0.01:1]

    y = morph(z0,z1,ang0,ang1,alpha);

    figure(1);
    plot(w,ang0,'--','Color',[1 0.5 0.5]);
    hold on;
    plot(w,ang1,'--','Color',[0.5 0.5 1]);
    plot(w,y,'k');
%     plot(z0,ang0(z0),'ro',z1,ang1(z1),'bo');
    hold off;
    pause(0.01);

end

% mag0_r = [mag0(:); mag0(end-1:-1:2)];
% ang0_r = [ang0(:); conj(ang0(end-1:-1:2))];
% h0_r = mag0_r .* (cos(ang0_r) + sqrt(-1).*sin(ang0_r));
% 
% x0_r = ifft(h0_r,'symmetric');
% 
% figure(3);
% plot(t,x0,t,x0_r);

% figure(1);
% plot(t,x0,t,x1);
% xlim([1 len]);
% ylim([-1 1]);

% figure(2);
% plot(w,unwrap(angle(h0)),w,unwrap(angle(h1)));
