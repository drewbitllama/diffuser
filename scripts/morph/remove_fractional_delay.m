function [ y ] = remove_fractional_delay( x, del )

x = x(:);
% x = x(end:-1:1);
% x = [zeros(floor(del),1); x(:)];

t = (1:length(x))';

y = zeros(length(x),1);

for i=1:length(y)
    y(i) = sum(sinc(t - (i + del)) .* x);
end
% y = y(end:-1:1);

end