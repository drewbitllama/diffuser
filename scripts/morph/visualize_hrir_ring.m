clearvars;
clc;

subject = 27;

len = 200;
deg = 360;

map_l0 = zeros(len/2+1,deg);
map_r0 = zeros(len/2+1,deg);

map_l1 = zeros(len/2+1,deg);
map_r1 = zeros(len/2+1,deg);

map_l_diff = zeros(len/2+1,deg);
map_r_diff = zeros(len/2+1,deg);

map_l0_minpre = zeros(len/2+1,deg);
map_r0_minpre = zeros(len/2+1,deg);

map_l0_minpost = zeros(len/2+1,deg);
map_r0_minpost = zeros(len/2+1,deg);

map_l0_min_diff = zeros(len/2+1,deg);
map_r0_min_diff = zeros(len/2+1,deg);

w = 1:len/2+1;
for i=1:deg
    
    x0 = audioread(sprintf('./output/cipic_%03d_linear_a%03d.wav',subject,i-1));
    x1 = audioread(sprintf('./output/cipic_%03d_morph_a%03d.wav',subject,i-1));
    
    x0_minpre = audioread(sprintf('./output/cipic_%03d_linear_minphase_a%03d.wav',subject,i-1));
    x0_minpost = audioread(sprintf('./output/cipic_%03d_linear_minphasepost_a%03d.wav',subject,i-1));
    
    l0 = fft(x0(:,1));
    r0 = fft(x0(:,2));
    
    l1 = fft(x1(:,1));
    r1 = fft(x1(:,2));
    
    l0_minpre = fft(x0_minpre(:,1));
    r0_minpre = fft(x0_minpre(:,2));
    
    l0_minpost = fft(x0_minpost(:,1));
    r0_minpost = fft(x0_minpost(:,2));
    
    map_l0(:,i) = 20*log10(abs(l0(w)))';
    map_r0(:,i) = 20*log10(abs(r0(w)))';
    
    map_l1(:,i) = 20*log10(abs(l1(w)))';
    map_r1(:,i) = 20*log10(abs(r1(w)))';
    
    map_l_diff(:,i) = 20*log10(min(abs(l0(w))./abs(l1(w)),abs(l1(w))./abs(l0(w))));
    map_r_diff(:,i) = 20*log10(min(abs(r0(w))./abs(r1(w)),abs(r1(w))./abs(r0(w))));
    
    map_l0_minpre(:,i) = 20*log10(abs(l0_minpre(w)))';
    map_r0_minpre(:,i) = 20*log10(abs(r0_minpre(w)))';
    
    map_l0_minpost(:,i) = 20*log10(abs(l0_minpost(w)))';
    map_r0_minpost(:,i) = 20*log10(abs(r0_minpost(w)))';
    
    map_l0_min_diff(:,i) = 20*log10(min(abs(l0_minpre(w))./abs(l0_minpost(w)),abs(l0_minpost(w))./abs(l0_minpre(w))));
    map_r0_min_diff(:,i) = 20*log10(min(abs(r0_minpre(w))./abs(r0_minpost(w)),abs(r0_minpost(w))./abs(r0_minpre(w))));
    
end

figure(1);
subplot(1,2,1);
imagesc(map_l0(end:-1:1,:),[-60 20]);
colormap hot;
title('Linear (L) dB');
subplot(1,2,2);
imagesc(map_l1(end:-1:1,:),[-60 20]);
colormap hot;
title('Morph (L) dB');

figure(2);
subplot(1,2,1);
imagesc(map_r0(end:-1:1,:),[-60 20]);
colormap hot;
title('Linear (R) dB');
subplot(1,2,2);
imagesc(map_r1(end:-1:1,:),[-60 20]);
colormap hot;
title('Morph (R) dB');

figure(3);
subplot(1,2,1);
imagesc(map_l0_minpre(end:-1:1,:),[-60 20]);
colormap hot;
title('Min.Phase(Pre) (L) dB');
subplot(1,2,2);
imagesc(map_l0_minpost(end:-1:1,:),[-60 20]);
colormap hot;
title('Min.Phase(Post) (L) dB');

figure(4);
subplot(1,2,1);
imagesc(map_r0_minpre(end:-1:1,:),[-60 20]);
colormap hot;
title('Min.Phase(Pre) (R) dB');
subplot(1,2,2);
imagesc(map_r0_minpost(end:-1:1,:),[-60 20]);
colormap hot;
title('Min.Phase(Post) (R) dB');

mindb_minphase = -6;
figure(5);
subplot(1,2,1);
imagesc(map_l0_min_diff(end:-1:1,:),[mindb_minphase 0]);
colormap hot;
title('Min.Phase Diff (L)');
subplot(1,2,2);
imagesc(map_r0_min_diff(end:-1:1,:),[mindb_minphase 0]);
colormap hot;
title('Min.Phase Diff (R)');

mindb_linmorph = -6;
figure(6);
subplot(1,2,1);
imagesc(map_l_diff(end:-1:1,:),[mindb_linmorph 0]);
colormap hot;
title('Diff (L)');
subplot(1,2,2);
imagesc(map_r_diff(end:-1:1,:),[mindb_linmorph 0]);
colormap hot;
title('Diff (R)');
