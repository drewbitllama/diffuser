clearvars;
clc;

fs = 44100;

figure(1);

x = -8:8;
v = 2.^x;

for d = v;

    c = 340;
%     d = 2.^v;
    
    T = d/c;

    num = [T 1];
    den = [T 0];

    [b,a] = bilinear(num,den,fs);

    % fc = 27.1;
    % k = tan(pi*fc/fs);
    % nfc_b = [1/(k+1) -1/(k+1)];
    % nfc_a = [1 (k-1)/(k+1)];
    % 
    % b = conv(b,nfc_b);
    % a = conv(a,nfc_a);

    [h,w] = freqz(b,a,2^16);
    
    plot(w./pi,unwrap(angle(h)));
%     plot(w,20*log10(abs(h)));
%     xlim([1/fs*2 20000/fs*2]);
%     ylim([-6 60]);
    hold on;
end
hold off;
