clearvars;
clc;

fc = 7000;
fs = 44100;

k = tan(pi*fc/fs);

b0_lf = k^2 / (k^2 + 2*k + 1);
b1_lf = 2*b0_lf;
b2_lf = b0_lf;

b0_hf = 1 / (k^2 + 2*k + 1);
b1_hf = -2*b0_hf;
b2_hf = b0_hf;

a1 = 2*(k^2 - 1) / (k^2 + 2*k + 1);
a2 = (k^2 - 2*k + 1) / (k^2 + 2*k + 1);

lf_b = [b0_lf b1_lf b2_lf];
hf_b = [b0_hf b1_hf b2_hf];
a = [1 a1 a2];

[h_lf,w_lf] = freqz(lf_b,a);
[h_hf,w_hf] = freqz(hf_b,a);

figure(1);
plot(w_lf,abs(h_lf),w_hf,abs(h_hf),w_lf,abs(h_lf-h_hf));
ylim([0 1]);

d = [1; zeros(100,1)];
t = 1:length(d);

d_lf = filter(lf_b,a,d);
d_hf = filter(hf_b,a,d);

figure(2);
plot(t,d,t,d_lf,t,d_hf,t,d_lf-d_hf);


