clearvars;
clc;

c = 340.29;
d = 1;

fs = 44100;
fc = c/(2*pi*d);
k = tan(pi*fc/fs);

a0 = 1 / (k + 1);
a1 = -1 / (k + 1);
b0 = 1;
b1 = (k - 1) / (k + 1);

b = [b0 b1] ./ a0;
a = [a0 a1] ./ a0;

a(2) = -0.999;

r = roots(a);
abs(r)

[h,w] = freqz(b,a,8192);

figure(1);
semilogx(w./pi,20*log10(abs(h)));
ylim([-60 20]);