clearvars;
clc;

source_theta = linspace(0,360,360);

az = 45;
el = -20;

for i=1%:length(source_theta)

% left/right down/up back/forward
source_radius = 1000;
source_azimuth_degrees = 5;%source_theta(i);
source_elevation_degrees = 10;
source_position = [source_radius*sin(source_azimuth_degrees/180*pi)*cos(source_elevation_degrees/180*pi) source_radius*sin(source_elevation_degrees/180*pi) source_radius*cos(source_azimuth_degrees/180*pi)*cos(source_elevation_degrees/180*pi)];

source_dir = source_position ./ norm(source_position);

hrtf_radius_meters = 1;
padding_meters = 0;
head_width_meters = 0.1449;

left_ear = [-head_width_meters/2 0 0];
right_ear = [head_width_meters/2 0 0];

left_dir = (left_ear - source_position) ./ norm(left_ear - source_position);
right_dir = (right_ear - source_position) ./ norm(right_ear - source_position);

tca_l = dot(-source_position, left_dir);
tca_r = dot(-source_position, right_dir);

if (tca_l < 0 || tca_r < 0)
    error('bad intersection (tca)');
end

d_l = sqrt(dot(-source_position,-source_position) - tca_l.^2);
d_r = sqrt(dot(-source_position,-source_position) - tca_r.^2);

if (d_l < 0 || d_r < 0)
    error('bad intersection (d)');
end

thc_l = sqrt(hrtf_radius_meters.^2 - d_l.^2);
thc_r = sqrt(hrtf_radius_meters.^2 - d_r.^2);

left_hrtf_point = source_position + (tca_l - thc_l) * left_dir;
right_hrtf_point = source_position + (tca_r - thc_r) * right_dir;

figure(1);

theta = linspace(0,2*pi,360);
cs = cos(theta)*hrtf_radius_meters;
sn = sin(theta)*hrtf_radius_meters;
zr = 0*theta;

ms = 16;

% plot hrtf outline
plot3(cs,sn,zr,'--','Color',[0.75 0.75 0.75]);
hold on;
plot3(cs,zr,sn,'--','Color',[0.75 0.75 0.75]);
plot3(zr,cs,sn,'--','Color',[0.75 0.75 0.75]);

% plot source & rays
plot3(source_position(1),source_position(3),source_position(2),'.','Color',[0 0 1],'MarkerSize',ms);
plot3([source_dir(1)*hrtf_radius_meters 0],[source_dir(3)*hrtf_radius_meters 0],[source_dir(2)*hrtf_radius_meters 0],'Color',[0 1 1]);
plot3(source_dir(1).*hrtf_radius_meters,source_dir(3).*hrtf_radius_meters,source_dir(2).*hrtf_radius_meters,'.','Color',[0 1 1],'MarkerSize',ms);
plot3([source_dir(1).*hrtf_radius_meters left_hrtf_point(1)],[source_dir(3).*hrtf_radius_meters left_hrtf_point(3)],[source_dir(2).*hrtf_radius_meters left_hrtf_point(2)],'--','Color',[0 0 1]);
plot3([source_dir(1).*hrtf_radius_meters right_hrtf_point(1)],[source_dir(3).*hrtf_radius_meters right_hrtf_point(3)],[source_dir(2).*hrtf_radius_meters right_hrtf_point(2)],'--','Color',[0 0 1]);
plot3([source_position(1) left_hrtf_point(1)],[source_position(3) left_hrtf_point(3)],[source_position(2) left_hrtf_point(2)],'-','Color',[0 1 1]);
plot3([source_position(1) right_hrtf_point(1)],[source_position(3) right_hrtf_point(3)],[source_position(2) right_hrtf_point(2)],'-','Color',[0 1 1]);

plot3([source_position(1) -head_width_meters/2],[source_position(3) 0],[source_position(2) 0],'Color',[0 0 1]);
plot3([source_position(1) head_width_meters/2],[source_position(3) 0],[source_position(2) 0],'Color',[0 0 1]);

% plot expected point
plot3(left_hrtf_point(1),left_hrtf_point(3),left_hrtf_point(2),'.','MarkerSize',ms,'Color',[0 0 1]);
plot3(right_hrtf_point(1),right_hrtf_point(3),right_hrtf_point(2),'.','MarkerSize',ms,'Color',[0 0 1]);

% plot head & direction
plot3([0 0],[0 hrtf_radius_meters],[0 0],'-','Color',[1 0 0]);
plot3(0,0,0,'.','Color',[1 0 0],'MarkerSize',ms);
plot3(-head_width_meters/2,0,0,'.','Color',[0 0 0],'MarkerSize',ms);
plot3(head_width_meters/2,0,0,'.','Color',[0 0 0],'MarkerSize',ms);
plot3(head_width_meters/2*cos(theta),head_width_meters/2*sin(theta),zr,'-','Color',[0 0 0]);

hold off;
ylim([-hrtf_radius_meters-padding_meters hrtf_radius_meters+padding_meters]);
xlim([-hrtf_radius_meters-padding_meters hrtf_radius_meters+padding_meters]);
zlim([-hrtf_radius_meters-padding_meters hrtf_radius_meters+padding_meters]);
axis('square');
% rotate3d on;
view([az,el]);
pause(0.01);

left_azimuth = atan2(left_hrtf_point(1),left_hrtf_point(3))/pi*180;
left_elevation = atan2(left_hrtf_point(2),sqrt(left_hrtf_point(1).^2 + left_hrtf_point(3).^2))/pi*180;

right_azimuth = atan2(right_hrtf_point(1),right_hrtf_point(3))/pi*180;
right_elevation = atan2(right_hrtf_point(2),sqrt(right_hrtf_point(1).^2 + right_hrtf_point(3).^2))/pi*180;

end


[source_azimuth_degrees source_elevation_degrees left_azimuth left_elevation right_azimuth right_elevation]

