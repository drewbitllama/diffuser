clearvars;
clc;

N = 9;

a = 3.6;
k = (1:N)';

hk = -1 + 2*(k - 1)./(N - 1);
theta = acos(hk);
delta_phi = a ./ sqrt(N) ./ sqrt(1 - hk.^2);
phi = zeros(N,1);
for i=2:N-1
    phi(i) = mod(phi(i-1) + delta_phi(i), 2*pi);
end

X = cos(theta);
Y = sin(theta) .* cos(phi);
Z = sin(theta) .* sin(phi);
V = [X Y Z];

azi0 = theta./pi.*180;
ele0 = phi./pi.*180;

azi = atan2(Z,X)./pi.*180;
% ele = atan2(Y,sqrt(X.^2+Z.^2))./pi.*180;
ele = asin(Y)./pi*180;
[theta phi azi ele]

figure(3);
subplot(2,1,1);
plot(azi);
subplot(2,1,2);
plot(ele);

figure(2);
subplot(1,3,1);
plot(X);
subplot(1,3,2);
plot(Y);
subplot(1,3,3);
plot(Z);

figure(1);
[sx,sy,sz] = sphere(20);
mesh(sx,sy,sz);
colormap gray;
hold on;
plot3(X(1:N-1),Y(1:N-1),Z(1:N-1),'.','MarkerSize',15);
hold off;


