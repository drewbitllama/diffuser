clearvars;
clc;
close all;

listen_to_hrtfs = 0;

order = 20;
iter = 100;
cipic_subject = 3;

max_db = 20;

positions = [...
   -1  1  1;...
    1  1  1;...
   -1  1 -1;...
    1  1 -1;...
   -1 -1  1;...
    1 -1  1;...
   -1 -1 -1;...
    1 -1 -1;...
   ];

% normalize coordinates
for i=1:size(positions,1)
    v = positions(i,:);
    v = v ./ sqrt(sum(v.^2));
    positions(i,:) = v;    
end

% generate FOA decoder matrix
K = ones(size(positions,1),4) ./ sqrt(2);
for i=1:size(positions,1)
    K(i,2:4) = [-positions(i,3) positions(i,1) positions(i,2)]; % front-back, left-right, up-down
end
M = pinv(K)';

decoder_code = sprintf('const real decoder_matrix[%d] = { %f', size(M,1)*size(M,2), M(1,1));
for i=1:size(M,1)
    for j=1:size(M,2)
        if i == 1 && j == 1
            % nothing
        else
            decoder_code = sprintf('%s, %f', decoder_code, M(i,j));
        end
    end
end
decoder_code = sprintf('%s };', decoder_code);

% convert positions to CIPIC locations
cipic_azimuths = [-80 -65 55 -45:5:45 55 65 80]';
cipic_elevations = -45 + 5.625.*(0:49)';

cipic_lookup_angles = zeros(size(positions,1),2);
cipic_lookup_indices = zeros(size(positions,1),2);
for i=1:size(positions,1)
    azi = atan2(positions(i,1),positions(i,3))/pi*180;
    ele = asin(positions(i,2))/pi*180;    
    
    % wrap azi's that extend beyond -90:90
    if azi < -90
        azi = azi + 90;
        ele = ele + 180;
    end
    if azi > 90
        azi = azi - 90;
        ele = ele + 180;
    end
    
    [~,azi_idx] = min(abs(azi - cipic_azimuths));
    [~,ele_idx] = min(abs(ele - cipic_elevations));
    nearest_azi = cipic_azimuths(azi_idx);
    nearest_ele = cipic_elevations(ele_idx);
    cipic_lookup_angles(i,:) = [nearest_azi nearest_ele];
    cipic_lookup_indices(i,:) = [azi_idx ele_idx];
end

load(sprintf('./CIPIC_hrtf_database/standard_hrir_database/subject_%03d/hrir_final.mat',cipic_subject));

% normalize amplitudes
total_max_magnitude = 0;
for i=1:25
    for j=1:50
        h_l = fft(squeeze(hrir_l(i,j,:)));
        h_r = fft(squeeze(hrir_r(i,j,:)));
        max_magnitude = max(max(abs(h_l)),max(abs(h_r)));
        if (max_magnitude > total_max_magnitude)
            total_max_magnitude = max_magnitude;
        end
    end
end
overall_gain_scale = 10.^(max_db/20) / total_max_magnitude;

left_biquads = [];
left_gains = [];
left_delays = [];
right_biquads = [];
right_gains = [];
right_delays = [];

dirac = [1;zeros(199,1)];
t = 1:200;
for i = 1:size(positions,1);
    left = overall_gain_scale .* squeeze(hrir_l(cipic_lookup_indices(i,1),cipic_lookup_indices(i,2),:));
    left_delay = floor(OnL(cipic_lookup_indices(i,1),cipic_lookup_indices(i,2)));
    
    right = overall_gain_scale .* squeeze(hrir_r(cipic_lookup_indices(i,1),cipic_lookup_indices(i,2),:));
    right_delay = floor(OnR(cipic_lookup_indices(i,1),cipic_lookup_indices(i,2)));
    
    [left_b,left_a] = brandhauen(left(left_delay+1:end),order,iter);
    left_approx = filter(left_b,left_a,circshift(dirac,left_delay));

    [right_b,right_a] = brandhauen(right(right_delay+1:end),order,iter);
    right_approx = filter(right_b,right_a,circshift(dirac,right_delay));
    
%     figure(i);
%     subplot(1,2,1);
%     plot(t,left,t,left_approx);
%     subplot(1,2,2);
%     plot(t,right,t,right_approx);
    
    [sos_l,k_l] = tf2sos(left_b,left_a);
    [sos_r,k_r] = tf2sos(right_b,right_a);
    
    biquad_l = [];
    biquad_r = [];
    for j=1:size(sos_l,1)
        biquad_l = [biquad_l; sos_l(j,5); sos_l(j,6); sos_l(j,2); sos_l(j,3)];
        biquad_r = [biquad_r; sos_r(j,5); sos_r(j,6); sos_r(j,2); sos_r(j,3)];
    end
    
    left_biquads = [left_biquads; biquad_l];
    left_gains = [left_gains; k_l];
    left_delays = [left_delays; left_delay];
    right_biquads = [right_biquads; biquad_r];
    right_gains = [right_gains; k_r];
    right_delays = [right_delays; right_delay];
end

c_code = '';

left_biquads_code = sprintf('const real left_biquads[%d] = { %f', length(left_biquads),left_biquads(1));
right_biquads_code = sprintf('const real right_biquads[%d] = { %f', length(right_biquads),right_biquads(1));
left_gains_code = sprintf('const real left_gains[%d] = { %f', length(left_gains),left_gains(1));
right_gains_code = sprintf('const real right_gains[%d] = { %f', length(right_gains),right_gains(1));
left_delays_code = sprintf('const real left_delays[%d] = { %d', length(left_delays),left_delays(1));
right_delays_code = sprintf('const real right_delays[%d] = { %d', length(right_delays),right_delays(1));

for i=2:length(left_biquads)
    left_biquads_code = sprintf('%s, %f',left_biquads_code,left_biquads(i));
    right_biquads_code = sprintf('%s, %f',right_biquads_code,right_biquads(i));
end

for i=2:length(left_gains)
    left_gains_code = sprintf('%s, %f',left_gains_code,left_gains(i));
    right_gains_code = sprintf('%s, %f',right_gains_code,right_gains(i));
    left_delays_code = sprintf('%s, %d',left_delays_code,left_delays(i));
    right_delays_code = sprintf('%s, %d',right_delays_code,right_delays(i));
end

left_biquads_code = strcat(left_biquads_code,' };');
right_biquads_code = strcat(right_biquads_code,' };');
left_gains_code = strcat(left_gains_code,' };');
right_gains_code = strcat(right_gains_code,' };');
left_delays_code = strcat(left_delays_code,' };');
right_delays_code = strcat(right_delays_code,' };');


decoder_code

left_biquads_code
left_gains_code
left_delays_code

right_biquads_code
right_gains_code
right_delays_code

% listen to hrtfs
if (listen_to_hrtfs == 1)
    [x,fs] = audioread('chickenleg.wav');
    x = x(1:fs*2);
    for i = 1:size(positions,1);
        out_l = conv(x,squeeze(hrir_l(cipic_lookup_indices(i,1),cipic_lookup_indices(i,2),:)));
        out_r = conv(x,squeeze(hrir_r(cipic_lookup_indices(i,1),cipic_lookup_indices(i,2),:)));
        out = [out_l(:) out_r(:)];
        playblocking(audioplayer(out,fs));
    end
end

