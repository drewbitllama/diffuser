clearvars;
clc;

fs = 44100;
t60 = 1;

len = round(fs * t60);

n = rand(len, 1) * 2 - 1;
n = n .* 10.^(linspace(0,-60,len)'/20);

figure(1);
plot(n);

x = audioread('chickenleg.wav');
x = x(1:fs);

% x = 1;

s = conv(x,n);

angle_t = (1:length(n))';

azi_hz = 20;
azi_angle = 0;

ele_hz = 0;
ele_angle = 0;

azi_p = [azi_hz/fs azi_angle/180*pi]; 
ele_p = [ele_hz/fs ele_angle/180*pi];

azi = polyval(azi_p,angle_t);
ele = polyval(ele_p,angle_t);

W = s / sqrt(2);
X = s .* conv(x,cos(azi));
Y = s .* conv(x,sin(azi));
Z = zeros(length(W),1);

out = [conv(W,x) conv(X,x) conv(Y,x) conv(Z,x)];
out = out ./ max(max(abs(out)));

figure(1);
subplot(2,2,1);
plot(W);
subplot(2,2,2);
plot(X);
subplot(2,2,3);
plot(Y);
subplot(2,2,4);
plot(Z);

figure(2);
subplot(2,2,2);
plot(cos(azi));
subplot(2,2,3);
plot(sin(azi));

figure(3);
subplot(2,2,1);
plot(out(:,1));
subplot(2,2,2);
plot(out(:,2));
subplot(2,2,3);
plot(out(:,3));
subplot(2,2,4);
plot(out(:,4));

audiowrite('ambi_noise.wav',out,fs);
