clearvars;
clc;

% tailspin test

fid = fopen('test.dat');
x = fread(fid,Inf,'double');
fclose(fid);

in = reshape(x,2,length(x)/2)';

[snd,fs] = audioread('chickenleg.wav');
snd = snd(1:fs*2);

outL = conv(in(:,1),snd);
outR = conv(in(:,2),snd);

figure(1);
subplot(1,2,1);
plot(in(:,1));
subplot(1,2,2);
plot(in(:,2));

out = [outL outR];

playblocking(audioplayer(out,fs));

