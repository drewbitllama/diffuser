clearvars;
clc;

[in,fs] = audioread('ambi44k.wav');

W = in(:,1);
X = in(:,2);
Y = in(:,3);
Z = in(:,4);

r = sqrt(X.^2 + Y.^2 + Z.^2);
rmag = zeros(length(r), 1);
for i = 1:length(r)
    if r(i) ~= 0
        rmag(i) = 1 ./ r(i);
    end
end

s = W .* sqrt(2);
rs = s;
for i=1:length(s)
    if (s(i) ~= 0)
        rs(i) = 1 ./ s(i);
    end
end

Xn = X .* rs;
Yn = Y .* rs;
Zn = Z .* rs;
rn = sqrt(Xn.^2 + Yn.^2 + Zn.^2);

azi = unwrap(atan2(Yn, Xn));
ele = unwrap(atan2(Zn, sqrt(Xn.^2 + Yn.^2)));

angle_t = (1:length(azi))';

polyorder = 1;

weight = s.^2;
azi_p = polyfit(angle_t,azi(:),polyorder);

% azi_Hz = azi_p(1) * fs;
% azi_Phase = mod(azi_p(2), pi) / pi * 180;
azi_Hz = 100;
azi_Phase = 0;

azi_p(1) = azi_Hz / fs;
azi_p(2) = azi_Phase / 180 * pi;

azi_est = polyval(azi_p,angle_t);



% smoothing = 1000;
% azi_smooth = [ones(smoothing*2,1) .* azi(1); azi(:); ones(smoothing*2,1) .* azi(end)];
% azi_est = smooth(azi_smooth, smoothing);
% azi_est = azi_est(smoothing*2+1:end-smoothing*2);

% azi_p = polyfit(angle_t,azi(:),polyorder);
% azi_est = polyval(azi_p,angle_t);

figure(1);
plot(angle_t,azi,angle_t,azi_est);
drawnow;

% azi_est = azi;
ele_est = ele;

azi_offset = 0;
ele_offset = 0;

theta_offset = azi_offset / 180 * pi;
phi_offset = ele_offset / 180 * pi;

azi_cos = cos(azi_est + theta_offset);
azi_sin = sin(azi_est + theta_offset);

Xs = s .* azi_cos .* cos(ele_est + phi_offset);
Ys = s .* azi_sin .* cos(ele_est + phi_offset);
Zs = s .* sin(ele_est + phi_offset);

% Xs_n = Xs .* cos(theta_offset) - Ys .* sin(theta_offset);
% Ys_n = Xs .* sin(theta_offset) + Ys .* cos(theta_offset);

% Xs = Xs_n;
% Ys = Ys_n;

x = audioread('chickenleg.wav');

out1 = [conv(W,x) conv(Xs,x) conv(Ys,x) conv(Zs,x)];
out1 = out1 ./ max(max(abs(out1)));

% out2 = [conv(W,x) conv(Xp,x) conv(Yp,x) conv(Zp,x)];
% out2 = out2 ./ max(max(abs(out2)));

audiowrite('ambi44k_recon_s_manual_0.wav',out1,fs);
% audiowrite('ambi44k_approx_conv.wav',out2,fs);
