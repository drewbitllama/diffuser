clearvars;
clc;

load taps.mat
% n = 8;
% 
% fid = fopen('test.dat');
% x = fread(fid,Inf,'double');
% fclose(fid);
% 
% x = reshape(x,n,length(x)/(n))';
% 
% [in,fs] = audioread('chickenleg.wav');
% % in = in(1:fs*2);
% 
% v = [];
% 
% for i=1:n
%     y = conv(in,x(:,i));
%     v(:,i) = y;
% end

% for i=1:n
%     playblocking(audioplayer(v(:,i),fs));
% end

azi = linspace(0,360,n+1);
azi = azi(1:n);
theta = azi / 180 * pi;

W = [];
X = [];
Y = [];
Z = [];

mono = sum(v,2);
alpha = 0.5;

mono = [mono; mono];

target = linspace(0, 360, length(mono))';

    
Xn1 = zeros(length(mono),1);
Yn1 = zeros(length(mono),1);
for i=1:n
    Xn1 = Xn1 + mono .* cos(theta(i));
    Yn1 = Yn1 + mono .* sin(theta(i));
end

Xn2 = mono .* cos(target./180.*pi);
Yn2 = mono .* sin(target./180.*pi);
    
W = [W; mono / sqrt(2)];
X = [X; alpha .* Xn1 + (1 - alpha) .* Xn2];
Y = [Y; alpha .* Yn1 + (1 - alpha) .* Yn2];
Z = [Z; zeros(length(mono),1)];

out = [W X Y Z];
out = out ./ max(max(abs(out)));

audiowrite('taps.wav',out,fs);

% mono = sum(v,2);
% mono = mono ./ max(abs(mono));

% audiowrite('taps_mono.wav',mono,fs);
