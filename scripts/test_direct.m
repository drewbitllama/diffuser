clearvars;
clc;

fs = 44100;

prime_min = 100;
prime_max = 1000;


dels = primes(prime_max);
for i=1:length(dels)
    if dels(i) > prime_min
        dels = dels(i:end);
        break;
    end
end
dels = dels(1:1:end);
dels = [101 229 359 487 613 743 877 997];

% dels = [101 229];

norm_fc = 0.1;

t60_dc = 1;
t60_ny = norm_fc * t60_dc;

del = max(dels);
Kp = -60 * del / fs / t60_dc;
Gp = -60 * del / fs / t60_ny;

kp = 10^(Kp/20);
gp = 10^(Gp/20);

bp = (kp - gp) / (kp + gp);

b = kp * (1 - bp);
a = [1 -bp];

if (b == 0) || (abs(bp) >= 1)
    error('unstable!');
end

[h1,w1] = freqz(b,a);
[h2,w2] = freqz(a,b);
figure(10);
plot(w1,20*log10(abs(h1)),w2,20*log10(abs(h2)),w1,20*log10(abs(h1.*h2)));


len = round(t60_dc * fs * 1.5);
x = zeros(len, length(dels));
parfor i=1:length(dels)
    
    x(:,i) = make_modcomb(len,t60_dc,t60_ny,dels(i),fs);
    
end
x = sum(x,2);

figure(1);
subplot(1,2,1);
[h,w] = freqz(x,1);
plot(w,20*log10(abs(h)));
subplot(1,2,2);
plot(1:length(x),max(-120,20*log10(abs(x))),1:length(x),-60*ones(length(x),1));
ylim([-120 0]);

blocksize = 1024;
overlap = 4;
X = make_mag_blocks(x,blocksize,blocksize/overlap);
X = X(end:-1:2,:);

figure(2);
imagesc(20*log10(abs(X)),[-60 0]);
colormap jet;

fid = fopen('test.dat');
y = fread(fid,Inf,'double');
fclose(fid);

figure(3);
subplot(1,2,1);
[h,w] = freqz(y,1);
plot(w,20*log10(abs(h)));
subplot(1,2,2);
plot(1:length(y),max(-120,20*log10(abs(y))),1:length(y),-60*ones(length(y),1));
ylim([-120 0]);

Y = make_mag_blocks(y,blocksize,blocksize/overlap);
Y = Y(end:-1:2,:);

figure(4);
imagesc(20*log10(abs(Y)),[-60 0]);
colormap jet;


playblocking(audioplayer(x,fs));
playblocking(audioplayer(y,fs));
