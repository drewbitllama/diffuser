clearvars;
clc;

len = 128; 
n = cos(2*pi*linspace(0,len/4,len)');
b = cos(2*pi*linspace(0,len/16,len)');
c = cos(2*pi*linspace(0,len/64,len)');
g = n .* b;
h = g .* c;
t = 1:len;

w = (0:(len-1))*pi/len;
N = fft(n);
B = fft(b);
C = fft(c);
G = fft(g);
H = fft(h);

figure(1);
subplot(1,2,1);
plot(t,n,t,b,t,c,t,g,t,h);
subplot(1,2,2);
plot(t,20*log10(abs(N)),t,20*log10(abs(B)),t,20*log10(abs(C)),t,20*log10(abs(G)),t,20*log10(abs(H)));
ylim([-60 60]);
