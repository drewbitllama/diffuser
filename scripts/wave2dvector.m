clearvars;
clc;

m = 200;

lambda = 1 / sqrt(2);

p = zeros(m,m);
vx = zeros(m,m);
vy = zeros(m,m);

g = exp(-linspace(-3,3,30).^2);
% g = 1;
% g = cos(linspace(0,1,100).*2*pi*20)*0.1;
g = sinc(-10:0.1:10).*0.1;

max_t = 1000;
for t=1:max_t
    
    for i=2:m-1
        for j=2:m-1
            vx(i,j) = vx(i,j) - lambda * (p(i,j) - p(i-1,j));
            vy(i,j) = vy(i,j) - lambda * (p(i,j) - p(i,j-1));
        end
    end
    
    for i=2:m-1
        for j=2:m-1
            p(i,j) = p(i,j) - lambda * (vx(i+1,j) - vx(i,j) + vy(i,j+1) - vy(i,j));
        end
    end
    
    if t <= length(g)
%         p(50,50) = p(50,50) + g(t);
        p(50,50) = g(t);
    else
        p(50,50) = 0;
    end
    
    figure(1);
    db = (max(-60,min(0,20*log10(abs(p)))) + 60) .* sign(p);
    dbx = (max(-60,min(0,20*log10(abs(vx)))) + 60) .* sign(vx);
    dby = (max(-60,min(0,20*log10(abs(vy)))) + 60) .* sign(vy);
    imagesc(db,[-60 60]);
%     imagesc(p,[-1 1]);
    colormap jet;
    pause(0.01);
end