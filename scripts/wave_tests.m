clearvars;
clc;

% simple 2D box room tests... test t60 times for various freqs

max_duration = 1;

width = 20;
depth = 30;

emitter_x = 0.475;
emitter_y = 0.475;

listener_x = 0.5;
listener_y = 0.5;

fs = 4000;
fc = fs * 0.1;
dt = 1 / fs;
c = 340;
dx = c / fs * sqrt(2); % CFL + SRL stability conditions
lambda = c * dt / dx;

db_min = -60;

m = round(width / dx);
n = round(depth / dx);

dc_fc = fs * 0.01;
R = exp(-2*pi*dc_fc/fs);
dc = impz([1 -1],[1 -R]);

g_bw = 10;
g_fc = fc / fs * 2;
g = sinc(-g_bw:g_fc:g_bw)' * g_fc;
g = g .* blackman(length(g));
g = g ./ dx .* 10.^(12/20);
g = conv(g,dc);

gx = round((m-1) * emitter_x + 1);
gy = round((n-1) * emitter_y + 1);

pn = zeros(m,n);
pc = zeros(m,n);
pp = zeros(m,n);

bc = zeros(m,n);
bc(1,:) = 1;
bc(m,:) = 1;
bc(:,1) = 1;
bc(:,n) = 1;
% bc(40:100,50) = 1;
R0 = 0.9;

mu = zeros(m,n);
mu(1,:) = lambda * (1 - R0) / (1 + R0);
mu(m,:) = lambda * (1 - R0) / (1 + R0);
mu(:,1) = lambda * (1 - R0) / (1 + R0);
mu(:,n) = lambda * (1 - R0) / (1 + R0);
mu(:,:) = lambda * (1 - R0) / (1 + R0);

max_t = max_duration * fs;
output = zeros(max_t, 1);
ox = round((m-1) * listener_x + 1);
oy = round((n-1) * listener_y + 1);
for t=1:max_t
    
    for i=2:m-1
        for j=2:n-1
            bc_losses = 0;
            L = pc(i-1,j);
            R = pc(i+1,j);
            U = pc(i,j-1);
            D = pc(i,j+1);
            
            if bc(i-1,j) == 1
                L = R;
                bc_losses = bc_losses + mu(i-1,j);
            end
            if bc(i+1,j) == 1
                R = L;
                bc_losses = bc_losses + mu(i+1,j);
            end
            if bc(i,j-1) == 1
                U = D;
                bc_losses = bc_losses + mu(i,j-1);
            end
            if bc(i,j+1) == 1
                D = U;
                bc_losses = bc_losses + mu(i,j+1);
            end
            
            pn(i,j) = ((L + R + U + D) / 2 + (bc_losses - 1) * pp(i,j)) / (bc_losses + 1);
            if bc(i,j) == 1
                pn(i,j) = 0;
            end
            
        end
    end
    
    if t <= length(g)
        pn(gx,gy) = pn(gx,gy) + g(t);
    end
    
    output(t) = pn(ox,oy);
    
    pp = pc;
    pc = pn;
    
    figure(1);
    db = (max(db_min,min(0,20*log10(abs(pn)))) - db_min) .* sign(pn);
    imagesc(db,[db_min -db_min]);
    colormap jet;
    pause(0.01);
    t
end

[h,w] = freqz(output,1,8192);
mag = smooth(abs(h),512);

figure(1);
plot(output);
xlim([1 length(output)]);
ylim([-1 1]);

output = output ./ max(abs(output));
playblocking(audioplayer(output,fs));

