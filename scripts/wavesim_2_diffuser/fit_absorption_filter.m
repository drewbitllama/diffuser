function [ bp ] = fit_absorption_filter( t60_dc, t60_f, f, iter )

z_neg1 = cos(-2*pi*f) + sqrt(-1) * sin(-2*pi*f);
Kp = -60 / t60_dc * 0.001;
kp = 10^(Kp / 20);

stepsize = 0.01;

t60_ny = sum(t60_f) / length(t60_f);

for i=1:iter
    
    Gp = -60 / t60_ny * 0.001;
    gp = 10^(Gp / 20);
    bp = (gp - kp) / (-gp - kp);

    hz = kp .* (1 - bp) ./ (1 - bp .* z_neg1);
    
    t60_evals = -60 ./ (20*log10(abs(hz))) .* 0.001;
    
    t60_ny = t60_ny - stepsize * sum(t60_f .* (t60_evals - t60_f)) / length(t60_f);
    
end

end

