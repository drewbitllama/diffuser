function [ b ] = fit_lowpass_filter( mag_dc, mag_f, f, iter )

z_neg1 = cos(-2*pi*f) + sqrt(-1) * sin(-2*pi*f);

stepsize = 0.01;

mag_ny = sum(mag_f) / length(mag_f)

for i=1:iter
    
    b = (mag_ny - mag_dc) / (-mag_ny - mag_dc);

    hz = mag_dc .* (1 - b) ./ (1 - b .* z_neg1);
    
    mag_evals = abs(hz);
    
    mag_ny = mag_ny - stepsize * sum(mag_f .* (mag_evals - mag_f)) / length(mag_f);
    
end

end

