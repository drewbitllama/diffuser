function [ mixing_time ] = get_mixing_time( x, gauss_blocksize, gauss_hopsize, xft_hopsize )

% gauss_blocksize = 128;
% gauss_hopsize = 16;
% xft_hopsize = 2;

gauss_size = floor((length(x) - (gauss_blocksize)) / gauss_hopsize);
errGauss = zeros(gauss_size, 1);
gaussIdx = zeros(gauss_size, 1);
for i=1:gauss_size
    
    idx1 = (i - 1) * gauss_hopsize + 1;
    idx2 = idx1 + gauss_blocksize - 1;
    
    xT = x(idx1:idx2);
    
    errGauss(i) = kurtosis(xT);
   
    gaussIdx(i) = round((idx1 + idx2) / 2);
    
end
errGauss = errGauss ./ max(abs(errGauss));

phase_size = floor(length(x) / xft_hopsize);
errPhase = zeros(phase_size, 1);
phaseIdx = zeros(phase_size, 1);
for i=1:phase_size
    
    idx = i*xft_hopsize;
    xT = x(1:idx);
    hT = fft(xT);
%     hT = freqz(xT,1,xft_blocksize);
    
    wT = linspace(0, 1, length(hT))';
    pT = -unwrap(angle(hT));    
    pT_range = max(pT) - min(pT);    
    npT = (pT - min(pT)) ./ pT_range;
    
    X = [ones(length(wT), 1) wT];
    b = X\npT;
    pTideal = wT.*b(1) + b(2);
    errPhase(i) = abs(sum(npT - pTideal)) ./ length(pT);
    
    phaseIdx(i) = round(idx/2);
    
end
errPhase = errPhase ./ max(abs(errPhase));

errX = 1:length(x);
linPhase = interp1(phaseIdx, errPhase, errX);
linGauss = interp1(gaussIdx, errGauss, errX);

% figure(3);
% plot(errX, linPhase, errX, linGauss);

for mixing_time=2:length(x)
    if isnan(linPhase(mixing_time))
    elseif isnan(linGauss(mixing_time))
    else
        dx1 = linPhase(mixing_time) - linGauss(mixing_time);
        dx2 = linPhase(mixing_time - 1) - linGauss(mixing_time - 1);
        if (sign(dx1) == -sign(dx2))
            break;
        end
    end
end
if (mixing_time == length(x))
    mixing_time = 0;
end
if (mixing_time < 0)
    mixing_time = 0;
end

end