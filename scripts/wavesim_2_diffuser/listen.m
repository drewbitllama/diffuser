clearvars;
clc;

fs = 44100;

fid = fopen('diffuser_output.dat');
x = fread(fid,Inf,'double');
fclose(fid);
 
x = x ./ max(abs(x));

blocksize = 1024;
overlap = 4;
X = make_mag_blocks(x,blocksize,blocksize/overlap);
X = X(end:-1:2,:);

figure(3);
imagesc(20*log10(abs(X)),[-60 0]);
colormap jet;

[in,fs] = audioread('chickenleg.wav');
% in = in(1:fs*2);

y = conv(in,x);
y = y ./ max(abs(y));

playblocking(audioplayer(y,fs));
audiowrite('ir_approx.wav',y,fs);
