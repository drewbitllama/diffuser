function [ y ] = mysmooth( x, v )

a = [x(1) .* ones(v,1); x(:); x(end) .* ones(v,1)];
y = smooth(a,v);
y = y(v+1:end-v);

end

