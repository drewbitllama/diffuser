function [x, offset] = remove_delay( x )

[~, locs_a, ~, prom_a] = findpeaks(x);
[~, locs_b, ~, prom_b] = findpeaks(-x);

locs = [locs_a; locs_b];
prom = [prom_a; prom_b];

[~, idxs] = sort(prom, 'descend');
offset = locs(idxs(1)) - 1;
x = x(offset+1:end);

end