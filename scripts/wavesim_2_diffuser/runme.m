clearvars;
clc;

%% initialization
fmax = 0.15;
fmin = 0.01;

blocksize = 256;
hopsize = 64;
desired_fs = 44100;

%% load audio file and resample
[ir,ir_fs] = audioread(sprintf('./ir.wav'));
ir_fs_fmax = round(ir_fs*fmax*2);

ir_w = resample(ir(:,1),ir_fs_fmax,ir_fs,200);
ir_x = resample(ir(:,2),ir_fs_fmax,ir_fs,200);
ir_y = resample(ir(:,3),ir_fs_fmax,ir_fs,200);
ir_z = resample(ir(:,4),ir_fs_fmax,ir_fs,200);
ir = [ir_w(:) ir_x(:) ir_y(:) ir_z(:)];

%% remove predelay and mixing time elements
[w,offset] = remove_delay(ir(:,1));
mixing_time = get_mixing_time(w, 32, 2, 2);
late = w(mixing_time:end);

%% add some whitespace at end of late to ensure T60s are positive
late = [late(:); zeros(ir_fs_fmax, 1)];

%% convert late response into spectrogram representation
count = round((length(late) + (blocksize - hopsize)) / hopsize);
blocked_length = count * hopsize + (blocksize - hopsize);
late = [late(:); zeros(blocked_length - length(late), 1)];
blocks = zeros(blocksize / 2 + 1, count);
for i=1:count
    index_start = (i - 1) * hopsize;
    h = fft(late(index_start+1:index_start+blocksize));
    blocks(:,i) = abs(h(1:blocksize/2+1));
end

%% remove DC component, reverse order and normalize
block_min_block = round(fmin/fmax*2*(size(blocks,1)-1))+1;
blocks = blocks(end:-1:block_min_block,:);
blocks = blocks ./ max(max(abs(blocks)));
eq_slice = blocks(:,1);

%% smooth-out response
for i=1:size(blocks,1)
    blocks(i,:) = mysmooth(blocks(i,:),8);
end
for i=1:size(blocks,2)
    blocks(:,i) = mysmooth(blocks(:,i),8);
end

%% determine linear t60 shape for each freq band
db_blocks_min = -60;
db_blocks = max(-120,20*log10(abs(blocks)));
t60_coefs = zeros(size(blocks,1),2);
for i=1:size(blocks,1);
    for j=1:size(blocks,2)
        if db_blocks(i,j) < db_blocks_min
            p = polyfit(1:j,db_blocks(i,1:j),1);
            t60_coefs(i,:) = p;
            break;
        end
    end
end

%% reconstruct response using approximated t60 values
reblocks = zeros(size(blocks,1),size(blocks,2));
reblocks_t = 1:size(blocks,2);
for i=1:size(reblocks,1)
    reblocks(i,:) = polyval(t60_coefs(i,:),reblocks_t);
end

%% determine normalized freqs of supplied t60 values and EQs
t_f = (block_min_block:(block_min_block+length(t60_coefs)-1))';
f = t_f./(blocksize/2+1)/2*ir_fs_fmax/desired_fs;

t60_f = -60./(t60_coefs(end:-1:1,1)./hopsize)/ir_fs_fmax;
mag_f = abs(10.^(t60_coefs(end:-1:1,2)/20));

%% fit t60 absorption filter
t60_dc = t60_f(1);
kp = 10^(-60 / t60_dc * 0.001 / 20);
bp = fit_absorption_filter(t60_dc, t60_f, f, 10000);
if (bp < 0)
    bp = 0;
end
t60_ny = -60 / (20*log10(abs(kp * (1 - bp) / (1 + bp)))) * 0.001;
t60_ratio = t60_ny / t60_dc;

%% fit EQ filter
mag_f = smooth(mag_f, 200);
mag_idx = round(length(mag_f)*0.1):round(length(mag_f)*0.4);
mag_dc = mag_f(mag_idx(1));
b = fit_lowpass_filter(mag_dc, mag_f(mag_idx), f(mag_idx), 10000);
if (b < 0)
    b = 0;
end
if (b == 0)
    lowpass_norm_fc = 1;
else
    lowpass_norm_fc = -log(b) / pi;
end

figure(1);
subplot(1,2,1);
imagesc(db_blocks,[-60 0]);
colormap parula;
title('Spectrogram (Input)');
subplot(1,2,2);
imagesc(reblocks,[-60 0]);
colormap parula;
title('Spectrogram (Approx)');

figure(2);
subplot(1,2,1);
[h,wh] = freqz(kp * (1 - bp), [1 -bp], 128);
semilogx(wh./(2*pi), -60 ./ (20*log10(abs(h))) .* 0.001, f, t60_f, 'o');
title('T60 Fit');
subplot(1,2,2);
[h,wh] = freqz(mag_dc * (1 - b), [1 -b], 128);
semilogx(wh./(2*pi), abs(h), f(mag_idx), mag_f(mag_idx), 'o');
title('EQ Fit');

[t60_dc t60_ratio lowpass_norm_fc]
