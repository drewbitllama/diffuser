clearvars;
clc;

%% Initialization
% SLF method (fc_max = 0.196, usable ~= 0.15)

N = 3;
fs = 4000;
fc = 0.1;
lp = 0.005;

fps = 1;

max_t = round(fs*0.5);

c = 340;
dx = c / fs / sqrt(N);

w = round(5 / dx);
h = round(2.5 / dx);
d = round(6 / dx);

gx = round(2.25 / dx);
gy = round(1.25 / dx);
gz = round(4 / dx);

lx = round(0.75 / dx);
ly = round(0.5 / dx);
lz = round(0.75 / dx);

if N == 2
    d = 1;
    gz = 1;
    lz = 1;
end

pn = zeros(w,h,d);
pc = zeros(w,h,d);
pp = zeros(w,h,d);

ghfc = fc;
glfc = lp;

gAmplitude = 10^(12/20);
gbw = 10;

g = sinc(-gbw:ghfc*2:gbw) .* ghfc*2;
g = hamming(length(g))' .* g;

glp = -sinc(-gbw:glfc*2:gbw) .* glfc*2;
glp((length(glp)-1)/2+1) = glp((length(glp)-1)/2+1) + 1;
glp = glp((length(glp)-1)/2 - (length(g)-1)/2:(length(glp)-1)/2 + (length(g)-1)/2);
g = conv(g,glp);

gk = tan(pi*glfc);
gb = [1/(gk+1) -1/(gk+1)];
ga = [1 (gk-1)/(gk+1)];
g = filter(conv(gb,gb),conv(ga,ga),g) .* gAmplitude;


[gh,gw] = freqz(g,1);
figure(1);
plot(gw,20*log10(abs(gh)));
xlim([0 pi]);
ylim([-60 6]);

g = g ./ dx;

%% Wall stuff (lots of preprocessing to make the script run faster)

walls = zeros(w,h,d);
R0 = zeros(w,h,d);

walls(1,:,:) = 1;
R0(1,:,:) = 0.9;

walls(end,:,:) = 1;
R0(end,:,:) = 0.9;

walls(:,1,:) = 1;
R0(:,1,:) = 0.99;

walls(:,end,:) = 1;
R0(:,end,:) = 0.8;

if N == 2
    % nothing
else
    walls(:,:,1) = 1;
    R0(:,:,1) = 0.9;

    walls(:,:,end) = 1;
    R0(:,:,end) = 0.9;
end

walls(round(2/dx),:,round(0.1/dx):round(1.25/dx)) = 1;
walls(round(2/dx),:,round(1.9/dx):round(3/dx)) = 1;
walls(1:round(2.5/dx),:,round(3/dx)) = 1;
walls(round(3.5/dx):end,:,round(3/dx)) = 1;

R0(round(2/dx),:,round(0.1/dx):round(1.25/dx)) = 0.9;
R0(round(2/dx),:,round(1.9/dx):round(3/dx)) = 0.9;
R0(1:round(2.5/dx),:,round(3/dx)) = 0.9;
R0(round(3.5/dx):end,:,round(3/dx)) = 0.9;

% figure(1);
% imagesc(1 - squeeze(walls(:,gy,:)),[0 1]);
% colormap gray;
% error;

Z = (walls == 1) .* (1 - R0) ./ (1 + R0);

Lw = ones(w,h,d);
Rw = ones(w,h,d);
Dw = ones(w,h,d);
Uw = ones(w,h,d);
Bw = ones(w,h,d);
Fw = ones(w,h,d);
Zw = zeros(w,h,d);
for i=1:w
    for j=1:h
        for k=1:d
            if walls(i,j,k) == 1
                Lw(i,j,k) = 0;
                Rw(i,j,k) = 0;
                Dw(i,j,k) = 0;
                Uw(i,j,k) = 0;
                Bw(i,j,k) = 0;
                Fw(i,j,k) = 0;
            else
                if walls(i-1,j,k) == 1
                    Lw(i,j,k) = 0;
                    Rw(i,j,k) = 2;
                    Zw(i,j,k) = Zw(i,j,k) + Z(i-1,j,k);
                end
                if walls(i+1,j,k) == 1
                    Lw(i,j,k) = 2;
                    Rw(i,j,k) = 0;
                    Zw(i,j,k) = Zw(i,j,k) + Z(i+1,j,k);
                end
                if walls(i,j-1,k) == 1
                    Dw(i,j,k) = 0;
                    Uw(i,j,k) = 2;
                    Zw(i,j,k) = Zw(i,j,k) + Z(i,j-1,k);
                end
                if walls(i,j+1,k) == 1
                    Dw(i,j,k) = 2;
                    Uw(i,j,k) = 0;
                    Zw(i,j,k) = Zw(i,j,k) + Z(i,j+1,k);
                end
                if N == 3 && k > 1 && walls(i,j,k-1) == 1
                    Bw(i,j,k) = 0;
                    Fw(i,j,k) = 2;
                    Zw(i,j,k) = Zw(i,j,k) + Z(i,j,k-1);
                end
                if N == 3 && k < d && walls(i,j,k+1) == 1
                    Bw(i,j,k) = 2;
                    Fw(i,j,k) = 0;
                    Zw(i,j,k) = Zw(i,j,k) + Z(i,j,k+1);
                end
            end
        end
    end
    sprintf('precomputing values... %03d%% complete', round(i/w*100))
end
Lw = Lw ./ (N .* (1 + Zw));
Rw = Rw ./ (N .* (1 + Zw));
Dw = Dw ./ (N .* (1 + Zw));
Uw = Uw ./ (N .* (1 + Zw));
Bw = Bw ./ (N .* (1 + Zw));
Fw = Fw ./ (N .* (1 + Zw));
Pw = (Zw - 1) ./ (Zw + 1);

%% Runtime 

g = [g(:); zeros(max_t - length(g), 1)];


v = VideoWriter('waves.avi');
v.FrameRate = 60;
v.Quality = 100;

open(v);

tic
last_percent = 0;
o = zeros(max_t,4);
for t=1:max_t
    
    if N == 3    
        pn(2:w-1,2:h-1,2:d-1) = ...
            Lw(2:w-1,2:h-1,2:d-1) .* pc(1:w-2,2:h-1,2:d-1) + ...
            Rw(2:w-1,2:h-1,2:d-1) .* pc(3:w,2:h-1,2:d-1) + ...
            Dw(2:w-1,2:h-1,2:d-1) .* pc(2:w-1,1:h-2,2:d-1) + ...
            Uw(2:w-1,2:h-1,2:d-1) .* pc(2:w-1,3:h,2:d-1) + ...
            Bw(2:w-1,2:h-1,2:d-1) .* pc(2:w-1,2:h-1,1:d-2) + ...
            Fw(2:w-1,2:h-1,2:d-1) .* pc(2:w-1,2:h-1,3:d) + ...
            Pw(2:w-1,2:h-1,2:d-1) .* pp(2:w-1,2:h-1,2:d-1);
    elseif N == 2
        pn(2:w-1,2:h-1,1) = ...
            Lw(2:w-1,2:h-1,1) .* pc(1:w-2,2:h-1,1) + ...
            Rw(2:w-1,2:h-1,1) .* pc(3:w,2:h-1,1) + ...
            Dw(2:w-1,2:h-1,1) .* pc(2:w-1,1:h-2,1) + ...
            Uw(2:w-1,2:h-1,1) .* pc(2:w-1,3:h,1) + ...
            Pw(2:w-1,2:h-1,1) .* pp(2:w-1,2:h-1,1);        
    end
    % source term
    pn(gx,gy,gz) = pn(gx,gy,gz) + g(t);
    
    pp = pc;
    pc = pn;

    if (mod(t,fps) == 0)
        figure(1);
        s = squeeze(pn(:,gy,:));
        ws = squeeze(walls(:,gy,:));
        db = (max(-60,min(0,20*log10(abs(s)))) + 60) .* sign(s);
        db = (ws == 0) .* db + (ws == 1) .* 60;
        imagesc(db,[-60 60]);
        axis equal;
        colormap jet;
        writeVideo(v,getframe);
        pause(0.01);
    end
    
    o(t,1) = pn(lx,ly,lz);
    o(t,2) = (pn(lx-1,ly,lz) - pn(lx+1,ly,lz)) / 2;
    o(t,3) = (pn(lx,ly-1,lz) - pn(lx,ly+1,lz)) / 2;
    if N == 2
        o(t,4) = 0;
    else
        o(t,4) = (pn(lx,ly,lz-1) - pn(lx,ly,lz+1)) / 2;
    end

    percent = round(t/max_t*100);
    if (last_percent ~= percent)
        sprintf('running simulation... %03.0f%% complete', percent)
        last_percent = percent;
    end
end
toc
close(v);
audiowrite('ir.wav',o,fs);

