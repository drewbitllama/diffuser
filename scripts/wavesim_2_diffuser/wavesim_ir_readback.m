clearvars;
clc;

[x,fs] = audioread('ir.wav');

fc = 0.195;
fs_new = round(fs*fc*2);

y = resample(x,fs_new,fs);

[in,in_fs] = audioread('chickenleg.wav');
in = resample(in,fs_new,in_fs);

o = conv(y(:,1),in);

figure(4);
plot(y(:,1));

o = o ./ max(abs(o));
playblocking(audioplayer(o,fs_new));

audiowrite('ir_input.wav',o,fs_new);


% gfc = 0.196;
% go = 0.95;
% gbw = 50;
% g = sinc(-gbw:gfc*2*go:gbw) .* gfc*2*go;
% g = blackman(length(g))' .* g;
% 
% gi = sinc(-100000:gfc*2:100000) .* gfc*2;
% 
% [h,w] = freqz(g,1);
% hi = freqz(gi,1);
% 
% figure(2);
% plot(w./pi/2,20*log10(abs(h)),w./pi/2,20*log10(abs(hi)));
% xlim([0 0.5]);
% ylim([-60 6]);
