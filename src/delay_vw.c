#include <stdlib.h>
#include <assert.h>
#include <string.h>

#include "delay_vw.h"

//=============================================================================

typedef struct _delay_vw
{
  double* buffer;
  int buffer_size;
  int write_ptr;
  int read_ptr;
  int int_delay_size;
  double frac_delay_size;
  double x_prev[3]; // x[n-1] x[n-2] x[n-3]

} delay_vw_t;

//=============================================================================

delay_vw_t* delay_vw_create(int max_size_in_samples)
{
  delay_vw_t* ptr = NULL;

  ptr = (delay_vw_t*)malloc(sizeof(delay_vw_t));
  assert(NULL != ptr);

  ptr->buffer = (double*)malloc(sizeof(double) * max_size_in_samples);
  assert(NULL != ptr->buffer);

  memset(ptr->buffer, 0, sizeof(double) * max_size_in_samples);

  ptr->buffer_size = max_size_in_samples;
  ptr->write_ptr = 0;
  ptr->read_ptr = 0;
  ptr->int_delay_size = 0;
  ptr->frac_delay_size = 0.0;
  ptr->x_prev[0] = 0.0;
  ptr->x_prev[1] = 0.0;
  ptr->x_prev[2] = 0.0;

  return ptr;
}

//=============================================================================

void delay_vw_destroy(delay_vw_t* ptr)
{
  free(ptr->buffer);
  free(ptr);
}

//=============================================================================

void delay_vw_set_delay_time(delay_vw_t* ptr,
  double delay_in_samples)
{
  int i, int_delay_size, idx;
  double prev_write;

  // clamp delay size
  if (delay_in_samples >= ptr->buffer_size)
  {
    delay_in_samples = ptr->buffer_size - 1;
  }
  else if (delay_in_samples < 1.0f)
  {
    delay_in_samples = 1.0f;
  }

  int_delay_size = (int)(delay_in_samples - 1);
  ptr->frac_delay_size = delay_in_samples - (double)((int)delay_in_samples);

  prev_write = ptr->x_prev[1];
  for (i = ptr->int_delay_size; i < int_delay_size; i++)
  {
    idx = (ptr->read_ptr + i) % ptr->buffer_size;
    ptr->buffer[idx] = prev_write;
  }
  ptr->write_ptr = (ptr->read_ptr + int_delay_size) % ptr->buffer_size;
  ptr->int_delay_size = int_delay_size;
}

//=============================================================================

double delay_vw_tick(delay_vw_t* ptr, double in)
{
  double out, xm1, x0, x1, x2, a, b, c, y;

  xm1 = in;
  x0 = ptr->x_prev[0];
  x1 = ptr->x_prev[1];
  x2 = ptr->x_prev[2];

  a = (3.0 * (x0 - x1) - xm1 + x2) / 2.0;
  b = 2.0 * x1 + xm1 - (5.0 * x0 + x2) / 2.0;
  c = (x1 - xm1) / 2.0;

  y =
    (((a * ptr->frac_delay_size) + b) *
      ptr->frac_delay_size + c) * ptr->frac_delay_size + x0;
  ptr->buffer[ptr->write_ptr] = y;

  // shift previous input values
  ptr->x_prev[2] = ptr->x_prev[1];
  ptr->x_prev[1] = ptr->x_prev[0];
  ptr->x_prev[0] = in;

  // read output
  out = ptr->buffer[ptr->read_ptr];
  ptr->buffer[ptr->read_ptr] = 0.0;

  // increment and wrap pointers
  ptr->write_ptr = (ptr->write_ptr + 1) % ptr->buffer_size;
  ptr->read_ptr = (ptr->read_ptr + 1) % ptr->buffer_size;

  return out;
}

//=============================================================================
