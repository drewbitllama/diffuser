#ifndef DELAY_VW_H__
#define DELAY_VW_H__

//=============================================================================
// variable-write delayline
//=============================================================================

typedef struct _delay_vw delay_vw_t;

delay_vw_t* delay_vw_create(int max_size_in_samples);

void delay_vw_destroy(delay_vw_t* ptr);

void delay_vw_set_delay_time(delay_vw_t* ptr,
  double delay_in_samples);

double delay_vw_tick(delay_vw_t* ptr, double in);

//=============================================================================

#endif /* DELAY_VW_H__ */