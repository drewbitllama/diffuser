#include <stdlib.h>
#include <assert.h>
#include <string.h>
#include <math.h>
#include <float.h>

#include "diffuser.h"

//=============================================================================
// vectors & matrices
//=============================================================================

void _diffuser_sqrmat_by_vec(real* vec_out, real* mat_in, real* vec_in, int n)
{
  // note: mat is row-major order
  int i = 0;
  int j = 0;
  int row = 0;

  memset(vec_out, 0, sizeof(real) * n);

  for (; i < n; i++)
  {
    row = i * n;
    for (j = 0; j < n; j++)
    {
      vec_out[i] += mat_in[row + j] * vec_in[j];
    }
  }
}

//=============================================================================
// one-zero filter
//=============================================================================

struct _diffuser_1z_t
{
  real b0, b1, xm1; // one-zero filter
};

//=============================================================================

void _diffuser_1z_init(struct _diffuser_1z_t* ptr)
{
  ptr->xm1 = 0;
}

//=============================================================================

void _diffuser_1z_make_tone(struct _diffuser_1z_t* ptr, 
  real recip_t60_secs_dc, real recip_t60_secs_nyquist, real t60_delay_secs)
{
  real Kp = t60_delay_secs * recip_t60_secs_dc;
  real Gp = t60_delay_secs * recip_t60_secs_nyquist;

  real kp = pow(10, Kp * 0.05);
  real gp = pow(10, Gp * 0.05);

  real bp = (kp - gp) / (kp + gp);

  ptr->b0 = 1 / (kp * (1 - bp));
  ptr->b1 = -bp * ptr->b0;
}

//=============================================================================

real _diffuser_1z_tick(struct _diffuser_1z_t* ptr, real in_sample)
{
  real out_sample = ptr->b0 * in_sample + ptr->b1 * ptr->xm1;
  ptr->xm1 = in_sample;

  return out_sample;
}

//=============================================================================
// one-pole filter
//=============================================================================

struct _diffuser_1p_t
{
  real b0, a1, ym1; // one-pole filter
};

//=============================================================================

void _diffuser_1p_init(struct _diffuser_1p_t* ptr)
{
  ptr->ym1 = 0;
}

//=============================================================================

void _diffuser_1p_make_absorb(struct _diffuser_1p_t* ptr, 
  real recip_t60_secs_dc, real recip_t60_secs_nyquist, real t60_delay_secs)
{
  real Kp = t60_delay_secs * recip_t60_secs_dc;
  real Gp = t60_delay_secs * recip_t60_secs_nyquist;

  real kp = pow(10, Kp * 0.05);
  real gp = pow(10, Gp * 0.05);

  real bp = (kp - gp) / (kp + gp);

  ptr->b0 = kp * (1 - bp);
  ptr->a1 = -bp;
}

//=============================================================================

void _diffuser_1p_make_smoothing(struct _diffuser_1p_t* ptr, real norm_fc)
{
  const real pi = 4 * atan(1);

  ptr->a1 = -exp(-pi * norm_fc);
  ptr->b0 = 1 + ptr->a1;
}

//=============================================================================

real _diffuser_1p_tick(struct _diffuser_1p_t* ptr, real in_sample)
{
  ptr->ym1 = ptr->b0 * in_sample - ptr->a1 * ptr->ym1;

  return ptr->ym1;
}

//=============================================================================
// dc-blocker
//=============================================================================

struct _diffuser_dcblocker_t
{
  real b0, b1, a1, vm1;
};

void _diffuser_dcblocker_init(struct _diffuser_dcblocker_t* ptr, 
  real norm_fc)
{
  ptr->vm1 = 0;
  ptr->a1 = -exp(-4.0 * atan(1.0) * norm_fc);
}

real _diffuser_dcblocker_tick(struct _diffuser_dcblocker_t* ptr, 
  real in_sample)
{
  real v = in_sample - ptr->a1 * ptr->vm1;
  real out_sample = v - ptr->vm1;
  ptr->vm1 = v;
  return out_sample;
}

//=============================================================================
// delay
//=============================================================================

struct _diffuser_delay_t
{
  real* buffer;
  int size; // needs to be pow(2)
  int write_location;
  int read_location;
};

//=============================================================================

void _diffuser_delay_init(struct _diffuser_delay_t* ptr, int m)
{
  ptr->size = (int)pow(2, (double)((int)log2((double)m + 1)) + 1);
  
  ptr->buffer = (real*)malloc(sizeof(real) * ptr->size);
  assert(ptr->buffer);
  memset(ptr->buffer, 0, sizeof(real) * ptr->size);

  ptr->write_location = 0;
  ptr->read_location = (ptr->size - (m - 1)) & (ptr->size - 1);
}

//=============================================================================

void _diffuser_delay_cleanup(struct _diffuser_delay_t* ptr)
{
  free(ptr->buffer);
}

//=============================================================================

real _diffuser_delay_tick(struct _diffuser_delay_t* ptr, real in_sample)
{
  ptr->buffer[ptr->write_location] = in_sample;
  real out_sample = ptr->buffer[ptr->read_location];

  ptr->write_location = (ptr->write_location + 1) & (ptr->size - 1);
  ptr->read_location = (ptr->read_location + 1) & (ptr->size - 1);

  return out_sample;
}

//=============================================================================
// diffuser (main code)
//=============================================================================

typedef struct _diffuser_t
{
  // initial and current gain values (RMS)
  real gain_initial_rms;

  // # of filter lines, n^2 is the matrix size
  int n; 

  // delay values (in samples)
  real* delay_values; // const

  // vector and matrix buffers
  real* t60_delay_secs; // const
  real* mixing_matrix;  // const
  real* y_previous; // non-const
  real* y_current; // non-const

  // tone_corrector
  struct _diffuser_1z_t tone_filter;
  real tone_filter_t60_delay_secs;

  // EQ filter(s)
  struct _diffuser_1p_t low_pass;

  // DC blocker
  struct _diffuser_dcblocker_t dc_blocker;

  // delay-depedent absorption
  struct _diffuser_1p_t* absorbs;

  // input smoothing filters and pre-filtered values
  struct _diffuser_1p_t* inputs;
  real t60_secs_in;
  real rolloff_percent_in;
  real lowpass_cutoff_in;
  real gain_offset_rms_in;

  // n delay lines, simple & cheap
  struct _diffuser_delay_t* delays;

} diffuser_t;

//=============================================================================

diffuser_t* diffuser_create_with_values(
  real t60_secs, real rolloff_percent, real lowpass_cutoff,
  real initial_gain_dB, real fs,
  int n, const real* const delay_values, const real* const matrix_values)
{
  const real dc_fc = 20.0 / fs;
  const real smoothing_fc = 0.001;

  int i = 0;
  diffuser_t* ptr = NULL;

  // main structure alloc
  ptr = (diffuser_t*)malloc(sizeof(diffuser_t));
  assert(NULL != ptr);

  // initial and current gain values (RMS)
  ptr->gain_initial_rms = pow(10.0, initial_gain_dB / 20.0);

  // # of filter lines, n^2 is the matrix size
  ptr->n = n;

  // delay values (in samples)
  ptr->delay_values = (real*)malloc(sizeof(real) * n);
  assert(NULL != ptr->delay_values);
  for (i = 0; i < n; i++)
  {
    ptr->delay_values[i] = (real)((int)delay_values[i]);
  }

  // vector and matrix buffers
  ptr->t60_delay_secs = (real*)malloc(sizeof(real) * n);
  assert(NULL != ptr->t60_delay_secs);
  for (i = 0; i < n; i++)
  {
    ptr->t60_delay_secs[i] = 
      -60.0 * ptr->delay_values[i] / fs;
  }

  ptr->mixing_matrix = (real*)malloc(sizeof(real) * n * n);
  assert(NULL != ptr->mixing_matrix);
  memcpy(ptr->mixing_matrix, matrix_values, sizeof(real) * n * n);

  ptr->y_previous = (real*)malloc(sizeof(real) * n);
  assert(NULL != ptr->y_previous);
  memset(ptr->y_previous, 0, sizeof(real) * n);

  ptr->y_current = (real*)malloc(sizeof(real) * n);
  assert(NULL != ptr->y_current);
  memset(ptr->y_current, 0, sizeof(real) * n);

  // tone_corrector
  _diffuser_1z_init(&ptr->tone_filter);
  ptr->tone_filter_t60_delay_secs = 
    -60.0 * ptr->delay_values[n - 1] / fs;

  // EQ filter(s)
  _diffuser_1p_init(&ptr->low_pass);

  // DC blocker
  _diffuser_dcblocker_init(&ptr->dc_blocker, dc_fc);

  // delay-depedent absorption
  ptr->absorbs = (struct _diffuser_1p_t*)malloc(
    sizeof(struct _diffuser_1p_t) * n);
  assert(NULL != ptr->absorbs);
  for (i = 0; i < n; i++)
  {
    _diffuser_1p_init(&ptr->absorbs[i]);
  }

  // input smoothing filters and pre-filtered values
  // total inputs (4):
  //   t60_secs
  //   rolloff_percent
  //   lowpass_cutoff
  //   gain_offset_dB

  ptr->inputs = (struct _diffuser_1p_t*)malloc(
    sizeof(struct _diffuser_1p_t) * 4);
  assert(NULL != ptr->inputs);
  for (i = 0; i < 4; i++)
  {
    _diffuser_1p_init(&ptr->inputs[i]);
    _diffuser_1p_make_smoothing(&ptr->inputs[i], smoothing_fc);
  }
  ptr->inputs[0].ym1 = t60_secs;
  ptr->inputs[1].ym1 = rolloff_percent;
  ptr->inputs[2].ym1 = lowpass_cutoff;
  ptr->inputs[3].ym1 = 0;

  // n delay lines, simple & cheap
  ptr->delays = (struct _diffuser_delay_t*)malloc(
    sizeof(struct _diffuser_delay_t) * n);
  assert(NULL != ptr->delays);
  for (i = 0; i < n; i++)
  {
    _diffuser_delay_init(&ptr->delays[i], (int)ptr->delay_values[i]);
  }

  // initialize absorbs and tone corrector using initial settings
  diffuser_set_samplerate(ptr, fs);
  diffuser_set_params(ptr,
    t60_secs, rolloff_percent, lowpass_cutoff, 0);

  return ptr;
}

//=============================================================================

diffuser_t* diffuser_create_with_quality(
  real t60_secs, real rolloff_percent, real lowpass_cutoff,
  real initial_gain_dB, real fs, diffuser_quality_t quality)
{
  const int low_n = 2;
  const real low_delay_values[2] = { 1009,1499 };
  const real low_matrix_values[4] = {0.70711,-0.70711,0.70711,0.70711};

  const int med_n = 4;
  const real med_delay_values[4] = { 751,1063,1429,1747 };
  const real med_matrix_values[16] = { 0.35355,-0.35355,-0.5,-0.70711,
    -0.14645,0.85355,-1.1102e-16,-0.5,0.35355,-0.14645,0.85355,-0.35355,
    0.85355,0.35355,-0.14645,0.35355 };

  const int high_n = 8;
  const real high_delay_values[8] = { 503,701,907,1103,1307,1549,1753,1999 };
  const real high_matrix_values[64] = { 0.088388,-0.088388,-0.125,-0.17678,
    -0.25,-0.35355,-0.5,-0.70711,-0.28661,0.46339,0.35355,0.375,0.35355,0.25,
    -1.1102e-16,-0.5,0.28033,-0.72855,0.088388,0.088388,0.25,0.35355,0.25,
    -0.35355,0.051777,0.28033,-0.81694,-0.036612,1.9429e-16,0.25,0.35355,
    -0.25,-0.0075825,0.18436,0.34283,-0.81694,-0.036612,0.088388,0.375,
    -0.17678,0.10669,0.054917,0.22855,0.34283,-0.81694,0.088388,0.35355,
    -0.125,0.34597,0.10669,0.054917,0.18436,0.28033,-0.72855,0.46339,
    -0.088388,0.83525,0.34597,0.10669,-0.0075825,0.051777,0.28033,-0.28661,
    0.088388 };

  const int ultra_n = 16;
  real ultra_delay_values[16] = { 503,601,683,773,877,977,1063,1171,1277,1373,
    1481,1571,1667,1777,1879,1999 };
  const real ultra_matrix_values[256] = { 0.0055243,-0.0055243,-0.0078125,
    -0.011049,-0.015625,-0.022097,-0.03125,-0.044194,-0.0625,-0.088388,
    -0.125,-0.17678,-0.25,-0.35355,-0.5,-0.70711,-0.049163,0.060212,
    0.066291,0.085938,0.11049,0.14063,0.17678,0.21875,0.26517,0.3125,
    0.35355,0.375,0.35355,0.25,-1.1102e-16,-0.5,0.1741,-0.26461,-0.1976,
    -0.23754,-0.26562,-0.28726,-0.29687,-0.28726,-0.25,-0.17678,-0.0625,
    0.088388,0.25,0.35355,0.25,-0.35355,-0.29063,0.58035,0.16076,0.23209,
    0.17678,0.10937,0.022097,-0.078125,-0.17678,-0.25,-0.26517,-0.1875,
    1.9429e-16,0.25,0.35355,-0.25,0.17671,-0.59447,0.34597,0.011604,0.1774,
    0.21545,0.22656,0.18783,0.09375,-0.044194,-0.1875,-0.26517,-0.1875,
    0.088388,0.375,-0.17678,0.082864,0.090772,-0.74362,0.15847,-0.18175,
    0.013337,0.11049,0.19531,0.22097,0.15625,-3.0531e-16,-0.1875,-0.26517,
    -0.0625,0.35355,-0.125,-0.087211,0.24859,0.20015,-0.70495,0.12722,
    -0.26461,-0.088226,0.027621,0.15625,0.22097,0.15625,-0.044194,-0.25,
    -0.17678,0.3125,-0.088388,-0.06992,0.0065394,0.37013,0.31733,-0.62209,
    0.15847,-0.28118,-0.12729,1.6653e-16,0.15625,0.22097,0.09375,-0.17678,
    -0.25,0.26517,-0.0625,0.016952,-0.12309,0.0094691,0.41846,0.38569,
    -0.56408,0.18581,-0.28118,-0.12729,0.027621,0.19531,0.18783,-0.078125,
    -0.28726,0.21875,-0.044194,0.052645,-0.069962,-0.18869,-0.017875,0.42813,
    0.41304,-0.54475,0.18581,-0.28118,-0.088226,0.11049,0.22656,0.022097,
    -0.29688,0.17678,-0.03125,0.035254,0.021571,-0.11977,-0.2322,-0.038382,
    0.42813,0.41304,-0.56408,0.15847,-0.26461,0.013337,0.21545,0.10937,
    -0.28726,0.14063,-0.022097,0.017729,0.058692,0.015356,-0.14027,-0.2467,
    -0.038382,0.42813,0.38569,-0.62209,0.12722,-0.18175,0.1774,0.17678,
    -0.26563,0.11049,-0.015625,0.041765,0.05191,0.074805,0.015356,-0.14027,
    -0.2322,-0.017875,0.41846,0.31733,-0.70495,0.15847,0.011604,0.23209,
    -0.23754,0.085938,-0.011049,0.13306,0.057879,0.063304,0.074805,0.015356,
    -0.11977,-0.18869,0.0094691,0.37013,0.20015,-0.74362,0.34597,0.16076,
    -0.1976,0.066291,-0.0078125,0.34402,0.13306,0.057879,0.05191,0.058692,
    0.021571,-0.069962,-0.12309,0.0065394,0.24859,0.090772,-0.59447,0.58035,
    -0.26461,0.060212,-0.0055243,0.83053,0.34402,0.13306,0.041765,0.017729,
    0.035254,0.052645,0.016952,-0.06992,-0.087211,0.082864,0.17671,-0.29063,
    0.1741,-0.049163,0.0055243 };

  const int miller_n = 4;
  //const real miller_delay_values[4] = { 293,347,397,439 };
  const real miller_delay_values[4] = { 751,1063,1429,1747 };
  //const real miller_delay_values[4] = { 100, 100, 100, 100 };
  const real miller_matrix_values[16] = { 0,0.70711,0.70711,0,-0.70711,
    0,0,-0.70711,0.70711,0,0,-0.70711,0,0.70711,-0.70711,0 };
  //const real miller_matrix_values[16] = { 1,0,0,0,0,1,0,0,0,0,1,0,0,0,0,1 };

  diffuser_t* ptr = NULL;
  const real* g_delay_values = NULL;
  const real* g_matrix_values = NULL;
  int n = 0;

  switch (quality)
  {
  case DIFFUSER_MILLER:
  {
    n = miller_n;
    g_delay_values = miller_delay_values;
    g_matrix_values = miller_matrix_values;
  }
  break;
  case DIFFUSER_ULTRA_QUALITY:
  {
    n = ultra_n;
    g_delay_values = ultra_delay_values;
    g_matrix_values = ultra_matrix_values;
  }
  break;
  case DIFFUSER_HIGH_QUALITY:
  {
    n = high_n;
    g_delay_values = high_delay_values;
    g_matrix_values = high_matrix_values;
  }
  break;
  case DIFFUSER_MED_QUALITY:
  {
    n = med_n;
    g_delay_values = med_delay_values;
    g_matrix_values = med_matrix_values;
  }
  break;
  case DIFFUSER_LOW_QUALITY:
  default:
  {
    n = low_n;
    g_delay_values = low_delay_values;
    g_matrix_values = low_matrix_values;
  }
  break;
  }

  ptr = diffuser_create_with_values(
    t60_secs, rolloff_percent, lowpass_cutoff, 
    initial_gain_dB, fs,
    n, g_delay_values, g_matrix_values);

  return ptr;
}

//=============================================================================

void diffuser_destroy(diffuser_t* const ptr)
{
  int i = 0;
  if (NULL != ptr)
  {
    for (; i < ptr->n; i++)
    {
      _diffuser_delay_cleanup(&ptr->delays[i]);
    }

	  free(ptr->delays);
    free(ptr->inputs);
    free(ptr->absorbs);

	  free(ptr->y_current);
    free(ptr->y_previous);
    free(ptr->mixing_matrix);
    free(ptr->t60_delay_secs);

    free(ptr);
  }
}

//=============================================================================

void diffuser_set_samplerate(diffuser_t* const ptr, real fs)
{
  int i;
  const real dt = 1.0 / fs;
  const real dc_fc = 20.0 * dt;

  _diffuser_dcblocker_init(&ptr->dc_blocker, dc_fc);

  for (i = 0; i < ptr->n; i++)
  {
    ptr->t60_delay_secs[i] = 
      -60.0 * ptr->delay_values[i] / fs;
  }

  ptr->tone_filter_t60_delay_secs =
    -60.0 * ptr->delay_values[ptr->n - 1] / fs;
}

//=============================================================================

void diffuser_set_params(diffuser_t* const ptr,
  real t60_secs, real rolloff_percent, 
  real lowpass_cutoff, real gain_offset_dB)
{
  if (t60_secs != 0.0) 
    ptr->t60_secs_in = t60_secs;

  ptr->rolloff_percent_in = rolloff_percent;
  ptr->lowpass_cutoff_in = lowpass_cutoff;
  ptr->gain_offset_rms_in = pow(10, gain_offset_dB / 20);
}

//=============================================================================

real diffuser_tick(diffuser_t* const ptr, real in_sample)
{
  const real rolloff_percent_min = 0.1;

  real t60_secs, rolloff_percent, lowpass_cutoff, gain_offset;
  real out_gain;
  real out_sample = 0;
  real y;
  int i = 0;

  t60_secs =
    _diffuser_1p_tick(&ptr->inputs[0], ptr->t60_secs_in);
  rolloff_percent =
    _diffuser_1p_tick(&ptr->inputs[1], ptr->rolloff_percent_in);
  lowpass_cutoff =
    _diffuser_1p_tick(&ptr->inputs[2], ptr->lowpass_cutoff_in);
  gain_offset =
    _diffuser_1p_tick(&ptr->inputs[3], ptr->gain_offset_rms_in);

  // set output gain
  out_gain = ptr->gain_initial_rms * gain_offset;

  // compute t60 ratio
  const real t60_ratio =
    (rolloff_percent > 1 ? 1 :
      rolloff_percent < rolloff_percent_min ?
      rolloff_percent_min : rolloff_percent);

  const real eq_ratio = pow(2, ((lowpass_cutoff * 8) - 8));
  const real recip_t60_secs_dc = 1 / (t60_secs);
  const real recip_t60_secs_nyquist = 1 / (t60_secs * t60_ratio);

  _diffuser_1z_make_tone(&ptr->tone_filter, recip_t60_secs_dc,
    recip_t60_secs_nyquist, ptr->tone_filter_t60_delay_secs);
  _diffuser_1p_make_smoothing(&ptr->low_pass, eq_ratio);

  for (i = 0; i < ptr->n; i++)
  {
    _diffuser_1p_make_absorb(&ptr->absorbs[i], recip_t60_secs_dc,
      recip_t60_secs_nyquist, ptr->t60_delay_secs[i]);
  }

  for (i = 0; i < ptr->n; i++) // we can vectorize this easily
  {
    y = _diffuser_delay_tick(&ptr->delays[i],
      in_sample + ptr->y_previous[i]);
    y = _diffuser_1p_tick(&ptr->absorbs[i], y);
    out_sample += y;
    ptr->y_current[i] = y;
  }

  // matrix multiply against vector, get feedback contribution for next tick
  _diffuser_sqrmat_by_vec(ptr->y_previous, ptr->mixing_matrix,
    ptr->y_current, ptr->n);

  // tone corrector
  out_sample = _diffuser_1z_tick(&ptr->tone_filter, out_sample);

  // EQ
  out_sample = _diffuser_1p_tick(&ptr->low_pass, out_sample);

  // DC blocker
  out_sample = _diffuser_dcblocker_tick(&ptr->dc_blocker, out_sample);

  // output gain-staged sample
  for (i = 0; i < ptr->n; i++)
  {
    ptr->y_current[i] *= out_gain;
  }
  return out_sample * out_gain;
}

//=============================================================================

int diffuser_get_num_of_taps(const diffuser_t* const ptr)
{
  return ptr->n;
}

//=============================================================================

const real* diffuser_get_taps(const diffuser_t* const ptr)
{
  return ptr->y_current;
}

//=============================================================================
