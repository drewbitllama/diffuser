#ifndef DIFFUSER_H__
#define DIFFUSER_H__

typedef double real;
typedef struct _diffuser_t diffuser_t;

//=============================================================================
// quality levels for using preset-configured diffusers

typedef enum _diffuser_quality_t
{
  DIFFUSER_LOW_QUALITY,
  DIFFUSER_MED_QUALITY,
  DIFFUSER_HIGH_QUALITY,
  DIFFUSER_ULTRA_QUALITY,
  DIFFUSER_MILLER,
}
diffuser_quality_t;

//=============================================================================
// create a new diffuser ptr based on a given "quality" setting

// t60_secs: duration of reverberation
// rolloff_percent: [0 - 1] ratio of duration rolloff
//	 (1 is flat, -> 0 rolls off durations of highs, 0 is maximum rolloff)
// lowpass_cutoff: [0 - 1] normalized cutoff freq of lowpass filter
// initial_gain_dB: initial gain in dB (0 is max amp)
// fs: samplerate (Hz)

diffuser_t* diffuser_create_with_quality(
  real t60_secs, real rolloff_percent, real lowpass_cutoff, 
  real initial_gain_dB, real fs, diffuser_quality_t quality);

//=============================================================================
// create a new diffuser ptr with explicit control over static internals

// t60_secs: duration of reverberation
// rolloff_percent: [0 - 1] ratio of duration rolloff
//	 (1 is flat, -> 0 rolls off durations of highs, 0 is maximum rolloff)
// lowpass_cutoff: [0 - 1] normalized cutoff freq of lowpass filter
//   (0 is max lowpass effect, 1 disables lowpass)
// initial_gain_dB: initial gain in dB (0 is max amp)
// fs: samplerate (Hz)
// n: the number of filter lines, the mixing matrix is n^2.
// delay_values: pointer to list of delay line values
// matrix_values: pointer to n^2 (row-major order) mixing matrix values

diffuser_t* diffuser_create_with_values(
  real t60_secs, real rolloff_percent, real lowpass_cutoff, 
  real initial_gain_dB, real fs,
  int n, const real* const delay_values, const real* const matrix_values);

//=============================================================================
// cleanup diffuser instance

void diffuser_destroy(diffuser_t* const ptr);

//=============================================================================
// set samplerate (requires recomputing diffuser values)

// fs: samplerate (Hz)

void diffuser_set_samplerate(diffuser_t* const ptr, real fs);

//=============================================================================
// set t60 in seconds, t60 rolloff, lowpass and gain offset
// (only need to call when values are changed)

// t60_secs: duration of reverberation
// rolloff_percent: [0 - 1] ratio of duration rolloff
//	 (1 is flat, -> 0 rolls off durations of highs, 0 is maximum rolloff)
// lowpass_cutoff: [0 - 1] normalized cutoff freq of lowpass filter
//   (0 is max lowpass effect, 1 disables lowpass)
// gain_offset_dB: gain in dB (0 is unity)

void diffuser_set_params(diffuser_t* const ptr,
  real t60_secs, real rolloff_percent, 
  real lowpass_cutoff, real gain_offset_dB);

//=============================================================================
// tick function (per-sample filter operation)

real diffuser_tick(diffuser_t* const ptr, real in_sample);

//=============================================================================
// return number of internal taps

int diffuser_get_num_of_taps(const diffuser_t* const ptr);

//=============================================================================
// get const pointer to internal taps

const real* diffuser_get_taps(const diffuser_t* const ptr);

#endif /* DIFFUSER_H__ */
