#include <stdlib.h>
#include <assert.h>
#include <string.h>
#include <math.h>
#include <stdio.h>

#include "tailspin.h"

//=============================================================================
// vectors & matrices
//=============================================================================

void _tailspin_mat_by_vec(
  real* vec_out, real* mat_in, real* vec_in, int n, int m)
{
  // note: mat is row-major order
  int i = 0;
  int j = 0;
  int row = 0;

  // zero-set vec_out
  memset(vec_out, 0, sizeof(real) * m);

  // dot the rows and cols
  for (i = 0; i < n; i++)
  {
    row = i * m;
    for (j = 0; j < m; j++)
    {
      vec_out[i] += mat_in[row + j] * vec_in[j];
    }
  }
}

//=============================================================================
// near-field compensation filter (4-channel)
//=============================================================================

struct _tailspin_nfc_t
{
  real b0, b1, a1, vm1[4]; // one-zero, one-pole filter
};

//=============================================================================

void _tailspin_nfc_init(struct _tailspin_nfc_t* ptr, real fc, real fs)
{
  real k = tan(4.0 * atan(1.0) * fc / fs);

  ptr->b0 = 1 / (k + 1);
  ptr->b1 = -ptr->b0;
  ptr->a1 = (k - 1) * ptr->b0;
  ptr->vm1[0] = 0;
  ptr->vm1[1] = 0;
  ptr->vm1[2] = 0;
  ptr->vm1[3] = 0;
}

//=============================================================================

void _tailspin_nfc_tick(struct _tailspin_nfc_t* ptr, real* in_samples,
  real* out_samples)
{
  real v;
  int i = 0;
  for (; i < 4; i++)
  {
    v = in_samples[i] - ptr->a1 * ptr->vm1[i];
    out_samples[i] = ptr->b0 * v + ptr->b1 * ptr->vm1[i];
    ptr->vm1[i] = v;
  }
}

//=============================================================================
// crossover filter (4-channel)
//=============================================================================

struct _tailspin_crossovers_t
{
  // two-zero, two-pole filters
  real b02_lp, b1_lp, vm1_lp[4], vm2_lp[4]; 
  real b02_hp, b1_hp, vm1_hp[4], vm2_hp[4];
  real a1, a2;
};

//=============================================================================

void _tailspin_crossovers_init(struct _tailspin_crossovers_t* ptr,
  real fc, real fs)
{
  int i = 0;
  real k = tan(4.0 * atan(1.0) * fc / fs);

  ptr->b02_lp = (k * k) / (k * k + 2 * k + 1);
  ptr->b1_lp = 2 * ptr->b02_lp;
  
  ptr->b02_hp = 1 / (k * k + 2 * k + 1);
  ptr->b1_hp = -2 * ptr->b02_hp;

  ptr->a1 = 2 * (k * k - 1) / (k * k + 2 * k + 1);
  ptr->a2 = (k * k - 2 * k + 1) / (k * k + 2 * k + 1);

  for (; i < 4; i++)
  {
    ptr->vm1_lp[i] = 0;
    ptr->vm2_lp[i] = 0;
    ptr->vm1_hp[i] = 0;
    ptr->vm2_hp[i] = 0;
  }
}

//=============================================================================

void _tailspin_crossovers_tick(struct _tailspin_crossovers_t* ptr,
  real* in_samples, real *lp_out_samples, real *hp_out_samples)
{
  real v_lp, v_hp;
  int i = 0;

  for (i = 0; i < 4; i++)
  {
    v_lp = in_samples[i] - ptr->a1 * ptr->vm1_lp[i] 
      - ptr->a2 * ptr->vm2_lp[i];
    v_hp = in_samples[i] - ptr->a1 * ptr->vm1_hp[i] 
      - ptr->a2 * ptr->vm2_hp[i];

    lp_out_samples[i] = ptr->b02_lp * v_lp + ptr->b1_lp * ptr->vm1_lp[i]
      + ptr->b02_lp * ptr->vm2_lp[i];

    hp_out_samples[i] = ptr->b02_hp * v_hp + ptr->b1_hp * ptr->vm1_hp[i]
      + ptr->b02_hp * ptr->vm2_hp[i];

    ptr->vm2_lp[i] = ptr->vm1_lp[i];
    ptr->vm1_lp[i] = v_lp;

    ptr->vm2_hp[i] = ptr->vm1_hp[i];
    ptr->vm1_hp[i] = v_hp;
  }
}

//=============================================================================
// SOS (second-order system)
//=============================================================================

struct _tailspin_sos_t
{
  real gain;
  real* coefs; // group of 4 (a1,a2,b1,b2) per biquad
  real* vm; // group of 2 (vm1,vm2) per biquad
  int count; // biquad count
};

//=============================================================================

struct _tailspin_sos_t* _tailspin_sos_create(
  const real* const coefs, 
  real gain,
  int biquad_count)
{
  struct _tailspin_sos_t* ptr = NULL;
  ptr = (struct _tailspin_sos_t*)malloc(sizeof(struct _tailspin_sos_t));
  assert(NULL != ptr);

  ptr->gain = gain;
  ptr->count = biquad_count;

  // a1, a2, b1, b2 [repeat]
  ptr->coefs = (real*)malloc(sizeof(real) * biquad_count * 4);
  assert(NULL != ptr->coefs);

  // vm1, vm2
  ptr->vm = (real*)malloc(sizeof(real) * biquad_count * 2);
  assert(NULL != ptr->vm);

  // copy coefs
  memcpy(ptr->coefs, coefs, sizeof(real) * biquad_count * 4);

  // zero-set vm's
  memset(ptr->vm, 0, sizeof(real) * biquad_count * 2);

  return ptr;
}

//=============================================================================

void _tailspin_sos_destroy(struct _tailspin_sos_t* const ptr)
{
  if (NULL != ptr)
  {
    free(ptr->coefs);
    free(ptr->vm);

    free(ptr);
  }
}

//=============================================================================

real _tailspin_sos_tick(struct _tailspin_sos_t* ptr, real in_sample)
{
  int i = 0;
  int coef_idx = 0;
  int vm_idx = 0;

  real out = in_sample;
  for (i = 0; i < ptr->count; i++)
  {
    real v = out - ptr->coefs[coef_idx] * ptr->vm[vm_idx]
      - ptr->coefs[coef_idx + 1] * ptr->vm[vm_idx + 1];
    
    out = v + ptr->coefs[coef_idx + 2] * ptr->vm[vm_idx] +
      ptr->coefs[coef_idx + 3] * ptr->vm[vm_idx + 1];

    ptr->vm[vm_idx + 1] = ptr->vm[vm_idx];
    ptr->vm[vm_idx] = v;

    coef_idx += 4;
    vm_idx += 2;
  }

  return out * ptr->gain;
}

//=============================================================================
// delay
//=============================================================================

struct _tailspin_delay_t
{
  real* buffer;
  int size; // needs to be pow(2)
  int write_location;
  int read_location;
};

//=============================================================================

void _tailspin_delay_init(struct _tailspin_delay_t* ptr, int m)
{
  ptr->size = (int)pow(2, (double)((int)log2((double)m + 1)) + 1);

  ptr->buffer = (real*)malloc(sizeof(real) * ptr->size);
  assert(ptr->buffer);
  memset(ptr->buffer, 0, sizeof(real) * ptr->size);

  ptr->write_location = 0;
  ptr->read_location = (ptr->size - (m - 1)) & (ptr->size - 1);
}

//=============================================================================

void _tailspin_delay_cleanup(struct _tailspin_delay_t* ptr)
{
  free(ptr->buffer);
}

//=============================================================================

real _tailspin_delay_tick(struct _tailspin_delay_t* ptr, real in_sample)
{
  ptr->buffer[ptr->write_location] = in_sample;
  real out_sample = ptr->buffer[ptr->read_location];

  ptr->write_location = (ptr->write_location + 1) & (ptr->size - 1);
  ptr->read_location = (ptr->read_location + 1) & (ptr->size - 1);

  return out_sample;
}

//=============================================================================
// b-format
//=============================================================================

struct _tailspin_b_t
{
  real w, x, y, z;
};

//=============================================================================
// degree-lookup cos and sin tables
//=============================================================================

struct _tailspin_trig_table_t
{
  real* cos_vals;
  real* sin_vals;
  real multiplier;
  int n;
};

//=============================================================================

struct _tailspin_trig_table_t* _tailspin_trig_table_create(int pow2_mult)
{
  int i = 0;
  real delta_angle = 0;
  real theta = 0;

  struct _tailspin_trig_table_t* ptr = NULL;
  ptr = (struct _tailspin_trig_table_t*)
    malloc(sizeof(struct _tailspin_trig_table_t));
  assert(NULL != ptr);

  ptr->multiplier = (real)(1 << pow2_mult);
  ptr->n = (360 << pow2_mult);

  ptr->cos_vals = (real*)malloc(sizeof(real) * ptr->n);
  assert(NULL != ptr->cos_vals);

  ptr->sin_vals = (real*)malloc(sizeof(real) * ptr->n);
  assert(NULL != ptr->sin_vals);

  if (ptr->n > 1)
    delta_angle = 8 * atan(1) / (real)(ptr->n - 1);
  for (i = 0; i < ptr->n; i++)
  {
    ptr->cos_vals[i] = cos(theta);
    ptr->sin_vals[i] = sin(theta);
    theta += delta_angle;
  }

  return ptr;
}

//=============================================================================

void _tailspin_trig_table_destroy(struct _tailspin_trig_table_t* const ptr)
{
  if (NULL != ptr)
  {
    free(ptr->sin_vals);
    free(ptr->cos_vals);
    free(ptr);
  }
}

//=============================================================================

real _tailspin_trig_table_lookup_cos(
  struct _tailspin_trig_table_t* const ptr, real degrees)
{
  int idx;
  idx = (int)round(degrees * ptr->multiplier);
  idx %= ptr->n;
  idx += ptr->n;
  idx %= ptr->n;
  return ptr->cos_vals[idx];
}

//=============================================================================

real _tailspin_trig_table_lookup_sin(
struct _tailspin_trig_table_t* const ptr, real degrees)
{
  int idx;
  idx = (int)round(degrees * ptr->multiplier);
  idx %= ptr->n;
  idx += ptr->n;
  idx %= ptr->n;
  return ptr->sin_vals[idx];
}

//=============================================================================
// points on a sphere (algorithm by Rakhmanov, Saff and Zhou)
//=============================================================================

void _tailspin_get_cart_pts_on_sphere(int n, real* azimuth, real* elevation)
{
  int i = 0;
  real alpha = 3.6;
  real hk, theta, phi, phi_prev = 0, x, y, z;

  for (i = 0; i < n; i++)
  {
    hk = -1 + 2 * i / ((real)n - 1);
    theta = acos(hk);
    if (i == 0 || i == n - 1)
    {
      phi = 0;
    }
    else
    {
      phi = fmod(phi_prev + 
        alpha / (sqrt(n) * sqrt(1 - hk * hk)), 8 * atan(1));
    }

    phi_prev = phi;

    x = cos(theta);
    y = sin(theta) * cos(phi);
    z = sin(theta) * sin(phi);

    azimuth[i] = atan2(z, x) / 4 / atan(1) * 180;
    elevation[i] = atan2(y, sqrt(x * x + z * z)) / 4 / atan(1) * 180;
  }
}

//=============================================================================
// tailspin (main code)
//=============================================================================

typedef struct _tailspin_t
{
  int tap_count;  // n from diffuser + 1 (for the sum of all taps)

  // hard-coded values used for taps' surround encoding
  real* azimuth_taps; 
  real* elevation_taps;
  struct _tailspin_trig_table_t* trig_table;
  real recip_sqrt2;

  // desired direction of the overall effect
  real azimuth_angle;
  real elevation_angle;
  real direction_mix;

  struct _tailspin_nfc_t nfc;
  struct _tailspin_crossovers_t xovers;
  
  real* decoder_matrix;
  real* loudspeaker_in_samples;
  int num_speakers;

  struct _tailspin_delay_t* hrtf_l_delay;
  struct _tailspin_delay_t* hrtf_r_delay;
  struct _tailspin_sos_t** hrtf_l_sos;
  struct _tailspin_sos_t** hrtf_r_sos;
  int biquad_count;

} tailspin_t;

//=============================================================================

tailspin_t* tailspin_create_with_values(
  int num_of_taps,
  real fs,
  const real* const decoder_matrix,
  int num_speakers,
  const real* const hrtf_l_coefs,
  const real* const hrtf_l_delay_samples,
  const real* const hrtf_l_gain,
  const real* const hrtf_r_coefs,
  const real* const hrtf_r_delay_samples,
  const real* const hrtf_r_gain,
  int biquad_count)
{
  int i = 0;
  int hrtf_idx = 0;

  tailspin_t* ptr = NULL;
  ptr = (tailspin_t*)malloc(sizeof(tailspin_t));
  assert(NULL != ptr);

  ptr->tap_count = num_of_taps + 1;

  ptr->azimuth_taps = (real*)malloc(sizeof(real) * ptr->tap_count);
  assert(NULL != ptr->azimuth_taps);

  ptr->elevation_taps = (real*)malloc(sizeof(real) * ptr->tap_count);
  assert(NULL != ptr->elevation_taps);

  ptr->trig_table = _tailspin_trig_table_create(1);

  ptr->recip_sqrt2 = 1 / sqrt(2);

  _tailspin_get_cart_pts_on_sphere(
    ptr->tap_count, ptr->azimuth_taps, ptr->elevation_taps);

  ptr->azimuth_angle = 0;
  ptr->elevation_angle = 0;
  ptr->direction_mix = 0;

  _tailspin_nfc_init(&ptr->nfc, 27.1, fs);
  _tailspin_crossovers_init(&ptr->xovers, 700, fs);

  ptr->decoder_matrix = (real*)malloc(sizeof(real) * num_speakers * 4);
  assert(NULL != ptr->decoder_matrix);

  memcpy(ptr->decoder_matrix, 
    decoder_matrix, sizeof(real) * num_speakers * 4);

  ptr->loudspeaker_in_samples = (real*)malloc(sizeof(real) * num_speakers);
  assert(NULL != ptr->loudspeaker_in_samples);

  ptr->num_speakers = num_speakers;

  ptr->hrtf_l_delay = (struct _tailspin_delay_t*)malloc(
    sizeof(struct _tailspin_delay_t) * num_speakers);
  assert(NULL != ptr->hrtf_l_delay);

  ptr->hrtf_r_delay = (struct _tailspin_delay_t*)malloc(
    sizeof(struct _tailspin_delay_t) * num_speakers);
  assert(NULL != ptr->hrtf_r_delay);

  ptr->hrtf_l_sos = (struct _tailspin_sos_t**)
    malloc(sizeof(struct _tailspin_sos_t*) * num_speakers);
  assert(NULL != ptr->hrtf_l_sos);

  ptr->hrtf_r_sos = (struct _tailspin_sos_t**)
    malloc(sizeof(struct _tailspin_sos_t*) * num_speakers);
  assert(NULL != ptr->hrtf_r_sos);

  for (i = 0; i < num_speakers; i++)
  {
    _tailspin_delay_init(&ptr->hrtf_l_delay[i], (int)round(hrtf_l_delay_samples[i]));
    _tailspin_delay_init(&ptr->hrtf_r_delay[i], (int)round(hrtf_r_delay_samples[i]));

    ptr->hrtf_l_sos[i] = 
      _tailspin_sos_create(&hrtf_l_coefs[hrtf_idx], 
        hrtf_l_gain[i], biquad_count);

    ptr->hrtf_r_sos[i] =
      _tailspin_sos_create(&hrtf_r_coefs[hrtf_idx],
        hrtf_r_gain[i], biquad_count);

    hrtf_idx += biquad_count * 4;
  }

  return ptr;
}

//=============================================================================

tailspin_t* tailspin_create_with_quality(
  int num_of_taps, real fs, tailspin_quality_t quality)
{
  tailspin_t* ptr = NULL;

  if (quality == TAILSPIN_CIPIC3_O20_QUAD_44K)
  {
    const int num_speakers = 4;
    const int biquad_count = 10;

    const real decoder_matrix[16] = { 0.353553, -0.353553, -0.353553, 0.000000, 0.353553, -0.353553, 0.353553, 0.000000, 0.353553, 0.353553, -0.353553, 0.000000, 0.353553, 0.353553, 0.353553, 0.000000 };

    const real left_biquads[160] = { 0.301125, 0.719991, 18.286115, -20.044889, -0.226445, 0.743536, -0.818391, 0.000000, 1.570568, 0.797376, 2.395003, 1.320038, 0.675899, 0.801857, 0.514426, 0.541078, -1.321291, 0.809206, 0.024497, 1.178795, -1.662760, 0.841716, -0.676676, 0.362418, -0.851766, 0.842042, -0.987129, 0.993353, -1.897389, 0.904679, -1.484009, 0.569665, -1.527328, 0.914004, -1.514609, 0.914085, -1.784761, 0.939692, -1.798376, 0.955316, -0.786304, 0.711342, 0.373314, -0.000000, -1.424456, 0.768101, -1.848026, 0.839044, 0.059086, 0.777268, 12.956544, 42.752467, -0.340561, 0.814436, -0.402586, 1.014877, -1.894223, 0.899929, -1.032749, 1.190732, -1.853451, 0.906352, -1.943092, 0.978838, 1.094878, 0.924205, 0.846349, 0.769139, -1.787476, 0.974474, -1.792203, 0.983065, -1.834157, 0.975654, -1.843970, 0.985993, -1.649843, 0.978175, -1.639581, 0.971828, -1.300077, 0.707639, -57.154176, -91.491909, -1.680950, 0.708496, -1.039417, 0.000000, -0.092391, 0.721117, -1.584265, 0.957666, -0.738560, 0.787006, -0.744626, 1.028026, 1.561857, 0.794878, 1.764562, 0.906848, -1.611468, 0.828038, -1.692114, 0.794575, 1.018103, 0.879403, 0.845905, 1.096706, 0.630870, 0.905458, 0.761397, 0.849007, 0.321546, 0.929788, 0.331602, 0.897373, -1.190502, 0.958974, -1.178085, 0.948012, 0.415658, 0.562498, 27.841315, 28.662778, -1.582031, 0.642324, -1.432346, 0.400570, 1.017758, 0.718412, 0.911761, 0.000000, -0.159178, 0.736936, 0.828934, 1.052959, -1.143653, 0.792464, -0.852390, 0.859601, -1.832524, 0.841325, -1.861677, 0.887148, -1.574906, 0.874746, -1.268885, 0.711085, -1.795269, 0.908394, -1.812800, 0.934079, -1.792775, 0.967874, -1.792072, 0.968679, -1.605271, 0.974214, -1.608790, 0.976162 };
    const real left_gains[4] = { 0.056562, 0.004645, -0.010018, 0.006118 };
    const real left_delays[4] = { 28, 44, 29, 46 };

    const real right_biquads[160] = { -1.133448, 0.699462, 20.355902, 41.822977, 0.750303, 0.827376, -1.038638, 0.000000, 0.018860, 0.840568, 0.577987, 0.875299, -0.455122, 0.859491, -0.384518, 1.045654, -1.857778, 0.865884, -1.059526, 1.095395, -1.739378, 0.915317, -1.724262, 0.872347, -1.866422, 0.930723, -1.940664, 0.979478, -1.855828, 0.947701, -1.875569, 0.959280, -1.711667, 0.955144, -1.719979, 0.951086, -1.650301, 0.962351, -1.660326, 0.963679, 0.098866, -0.476834, 18.267056, -19.927153, 1.308899, 0.525134, 2.428283, 1.434229, -0.108900, 0.680433, 0.568093, 0.000000, -1.664365, 0.693327, -0.456347, 0.293105, 0.543672, 0.701430, -0.029587, 1.243598, -1.483904, 0.739854, -1.755518, 0.807511, -0.821354, 0.757923, -0.947335, 0.964955, 0.885713, 0.832358, 0.976353, 0.935227, 1.521018, 0.838112, 1.520724, 0.840020, -1.384122, 0.894836, -1.359171, 0.846220, 0.582968, 0.536837, 15.989731, 23.465242, 0.059865, 0.669040, -1.039287, 0.000000, -0.852788, 0.671545, 0.487014, 0.982160, 0.992020, 0.675757, 1.326513, 0.610632, -1.824764, 0.833805, -0.971044, 1.003116, -1.540434, 0.837583, -1.483160, 0.673820, -0.187640, 0.862585, -0.151938, 0.831422, -1.815948, 0.879091, -1.872614, 0.916939, -1.341909, 0.892232, -1.367806, 0.919095, -1.946722, 0.964159, -1.964055, 0.980948, -1.644747, 0.680577, 15.903452, 14.621296, -0.725141, 0.693587, -1.034235, -0.000000, -1.540492, 0.724310, -1.756503, 0.846076, -0.029460, 0.739094, -0.727653, 1.002866, 1.677508, 0.751989, 1.508931, 0.711369, -1.378084, 0.769974, -1.562191, 0.968904, 0.979071, 0.833129, 0.889483, 1.059835, 0.564503, 0.875572, 0.518293, 0.792019, 0.248423, 0.885911, 0.204939, 0.869587, -1.409017, 0.979035, -1.412586, 0.983229 };
    const real right_gains[4] = { 0.005014, 0.062773, 0.009374, 0.066637 };
    const real right_delays[4] = { 45, 28, 47, 29 };

    ptr = tailspin_create_with_values(num_of_taps, fs,
      decoder_matrix, num_speakers,
      left_biquads, left_delays, left_gains,
      right_biquads, right_delays, right_gains,
      biquad_count);
  }
  else if (quality == TAILSPIN_CIPIC3_O20_CUBE_44K)
  {
    const int num_speakers = 8;
    const int biquad_count = 10;

    const real decoder_matrix[32] = { 0.176777, -0.216506, -0.216506, 0.216506, 0.176777, -0.216506, 0.216506, 0.216506, 0.176777, 0.216506, -0.216506, 0.216506, 0.176777, 0.216506, 0.216506, 0.216506, 0.176777, -0.216506, -0.216506, -0.216506, 0.176777, -0.216506, 0.216506, -0.216506, 0.176777, 0.216506, -0.216506, -0.216506, 0.176777, 0.216506, 0.216506, -0.216506 };

    const real left_biquads[320] = { 0.666696, 0.629316, 17.777426, 35.206607, 0.256032, 0.672075, -1.036153, -0.000000, 1.471740, 0.750690, 1.728746, 0.702017, -1.529871, 0.785900, -1.585528, 0.620798, -1.820980, 0.829521, 0.626705, 1.658657, 1.015485, 0.871659, 1.112972, 0.958577, -1.371280, 0.886098, -1.342081, 0.830317, -0.784580, 0.897221, -0.699741, 0.923414, -1.144777, 0.917406, -1.087714, 0.892069, -1.758557, 0.926378, -1.780511, 0.940952, -1.155258, 0.216693, -11.098194, -13.738820, -1.310563, 0.610307, -1.047443, -0.000000, -0.522104, 0.718100, -0.706555, 1.135187, 0.352727, 0.723339, 0.603889, 1.450826, 1.063628, 0.822144, 0.838551, 1.083148, -1.315314, 0.858227, -1.337942, 0.864813, -1.746457, 0.872151, -1.806080, 0.918658, -1.895343, 0.910879, -1.965333, 0.984139, -1.602229, 0.972360, -1.609640, 0.974059, -1.656783, 0.972694, -1.654834, 0.965745, -1.605612, 0.639351, 18.163535, 24.227780, -0.948180, 0.765374, 0.033903, -1.115433, -0.523408, 0.818451, -0.436929, 0.000000, 1.647725, 0.819009, -1.036766, 1.101432, -0.316552, 0.879550, -0.362809, 0.853861, 0.952469, 0.886906, 0.996306, 0.804447, -1.424788, 0.915566, -1.432806, 0.892737, -1.157073, 0.939240, -1.137126, 0.935662, 0.437697, 0.940114, 0.439414, 0.953156, 0.183390, 0.944702, 0.193494, 0.957110, -1.875091, 0.878209, 3.177941, -0.000000, 1.213794, 0.670097, 1.213622, 0.255341, -1.434726, 0.720346, -1.991242, 0.988224, 0.097468, 0.722241, 0.062032, 0.887432, -0.607197, 0.773869, -1.166088, 0.951095, 0.780552, 0.777413, 1.044513, 0.818556, -1.886366, 0.900439, -1.929590, 0.946386, -1.616597, 0.947054, -1.612698, 0.952413, -1.778442, 0.974103, -1.785433, 0.979788, -1.878765, 0.978775, -1.878237, 0.980588, -1.586469, 0.627779, 5.353949, 4.222652, -0.233383, 0.388696, -1.037220, 0.000000, -0.841279, 0.680029, -1.238131, 1.017114, -1.497673, 0.748673, -1.742268, 0.827922, 0.302210, 0.789396, -0.157706, 1.158562, 0.592368, 0.824531, 0.472275, 0.838142, 1.001262, 0.845764, 1.006321, 1.016118, 1.613674, 0.870812, 1.697236, 0.846122, -1.687922, 0.944175, -1.694825, 0.953923, -1.634237, 0.977210, -1.636930, 0.978103, -1.850506, 0.855987, 5.529244, 5.647264, 0.710100, 0.714507, 0.044426, -0.000000, -1.743291, 0.791525, -1.946292, 0.941595, -1.289868, 0.816696, -1.436999, 0.959236, -1.606652, 0.824187, -1.837755, 0.902704, -0.113599, 0.861338, -0.377032, 0.738518, 1.019646, 0.871957, 0.936636, 0.929570, -0.732038, 0.907721, -0.818093, 1.036356, -1.830178, 0.968387, -1.833585, 0.978200, -1.643817, 0.978376, -1.638492, 0.978128, 0.174996, 0.735178, -40.267489, -51.139881, -1.676954, 0.784934, -1.039854, -0.000000, -0.731839, 0.797020, -0.570240, 0.830332, -1.536827, 0.816552, -1.672208, 0.870848, -1.428783, 0.826370, -1.534512, 0.962364, -1.822751, 0.837090, -1.821603, 0.855722, 1.064072, 0.852377, 1.363974, 1.693442, 1.651703, 0.853258, 1.994451, 1.183967, -0.010233, 0.928554, 0.003147, 0.940918, -1.206181, 0.960598, -1.204430, 0.946831, -0.800237, -0.132495, 4.006392, 2.923601, -1.271825, 0.729312, -1.046687, 0.000000, 0.897352, 0.771324, 1.448557, 1.577189, -0.226445, 0.776537, -0.460813, 1.038539, -0.857348, 0.833996, -0.994744, 0.738968, -0.758259, 0.884821, -0.810292, 0.928192, -1.761498, 0.926765, -1.733386, 0.900953, -1.922419, 0.932167, -1.923023, 0.936807, -1.622879, 0.947037, -1.597341, 0.943068, -1.884765, 0.979468, -1.881271, 0.977744 };
    const real left_gains[8] = { 0.026088, -0.007534, 0.049022, 0.072105, 0.292295, 0.035631, -0.012288, 0.037000 };
    const real left_delays[8] = { 28, 44, 29, 46, 28, 45, 29, 47 };

    const real right_biquads[320] = { 0.620776, 0.578386, 4.970693, -6.250405, 0.735310, 0.578627, 0.788176, -0.000000, -1.261969, 0.668381, 0.988527, 1.709499, -0.774720, 0.687485, -0.747819, 1.091107, -1.659340, 0.713181, -0.459764, 0.087958, -1.006055, 0.795906, -1.064769, 0.880613, -1.867746, 0.877165, -1.954680, 0.979022, -1.828283, 0.952806, -1.837979, 0.964632, -1.685740, 0.959235, -1.676831, 0.952334, -1.919483, 0.965011, -1.908057, 0.957709, -1.637759, 0.665149, 39.668100, 51.030439, 0.422005, 0.670365, -0.027104, 0.000000, -1.253474, 0.713216, -1.811675, 0.805742, 1.082423, 0.760980, 0.225293, 1.551085, 0.763195, 0.794368, 0.974615, 0.850896, -0.996841, 0.851071, -0.970281, 0.881032, -0.712197, 0.892693, -0.628840, 0.937112, -1.636837, 0.892899, -1.550241, 0.791962, 1.792397, 0.910438, 1.955477, 1.076310, -1.512986, 0.947690, -1.522311, 0.957818, -0.820824, -0.117805, 8.692129, 10.205065, -0.424608, 0.431280, -0.486074, -0.580193, -0.512612, 0.673269, -0.431672, 0.000000, 0.545060, 0.719438, -0.037820, 0.857344, -1.542484, 0.768556, -1.258645, 0.977875, 0.948091, 0.909469, 0.964281, 0.963752, -1.803347, 0.949931, -1.802385, 0.954896, -1.843584, 0.951206, -1.845985, 0.957958, -1.943725, 0.952967, -1.960388, 0.972363, -1.943278, 0.970842, -1.956244, 0.984425, -0.277539, 0.572373, 3.748439, 0.000000, 1.188600, 0.655619, 1.613563, 0.635080, -1.697087, 0.722816, -1.761320, 0.752902, -0.904428, 0.723737, -1.049152, 1.095801, 0.442953, 0.757847, 0.310864, 0.951949, -0.062807, 0.769692, 0.059543, 0.712263, -1.629205, 0.811442, -1.635353, 0.839132, 1.733877, 0.873661, 1.589147, 0.858718, -1.371202, 0.876884, -1.356440, 0.818361, 1.106205, 0.959047, 1.106211, 0.951143, 0.008194, 0.738498, 4.490587, 4.346617, -1.352093, 0.781953, -1.040256, -0.000000, -1.572100, 0.795683, -1.475983, 0.957580, -0.613373, 0.824485, -0.650156, 0.965573, 0.673977, 0.828831, 0.324375, 0.777616, -0.299555, 0.860700, -0.416386, 0.998194, 1.051984, 0.900063, 0.909697, 0.834164, -1.903228, 0.907695, -1.565006, 0.626368, -1.780414, 0.935547, -1.805603, 0.961862, -1.938903, 0.962967, -1.915920, 0.941173, -1.180661, 0.346619, 10.795956, -12.225528, -0.102198, -0.718626, 0.480398, -0.991120, 0.885559, 0.621782, 0.952410, -0.000000, 0.023701, 0.674051, -0.114581, 1.154007, 0.763419, 0.744663, 0.980566, 1.026357, -1.314597, 0.780283, -1.235061, 0.930598, -1.659886, 0.787382, -1.781872, 0.875792, 1.723714, 0.829178, 1.787762, 0.937344, -0.866261, 0.847940, -0.888364, 0.699246, -1.645326, 0.987479, -1.644154, 0.985750, 0.600545, 0.376766, 8.076481, 8.814382, -0.094444, 0.678582, -0.212686, -0.859316, -0.760105, 0.718328, -0.184718, 0.000000, -1.113220, 0.790206, -0.526838, 1.061143, 0.917325, 0.817914, 1.130192, 1.307063, -1.850033, 0.858757, -1.090997, 0.863600, -1.579382, 0.884345, -1.543916, 0.822241, -1.825226, 0.891601, -1.866202, 0.916436, -1.734621, 0.903801, -1.709829, 0.858560, -1.935541, 0.950353, -1.960722, 0.975527, -0.301275, -0.443440, 13.995596, 14.858052, 0.074879, 0.620207, -1.032564, -0.000000, -1.682714, 0.724220, -0.671632, 0.654381, 0.823015, 0.742457, 1.298730, 1.694698, -1.654118, 0.804109, -1.841300, 0.879025, -1.512354, 0.804480, -1.737864, 0.919687, -1.355450, 0.824336, -1.525607, 0.959435, -1.203587, 0.866520, -1.257896, 0.889716, 1.742026, 0.880994, 1.903292, 1.022718, -0.813292, 0.908040, -0.678449, 0.855325 };
    const real right_gains[8] = { 0.015777, 0.018770, 0.017573, 0.335654, 0.047837, 0.101916, 0.012951, 0.051959 };
    const real right_delays[8] = { 46, 28, 46, 29, 45, 28, 48, 29 };

    ptr = tailspin_create_with_values(num_of_taps, fs,
      decoder_matrix, num_speakers,
      left_biquads, left_delays, left_gains,
      right_biquads, right_delays, right_gains,
      biquad_count);
  }

  return ptr;
}

//=============================================================================

void tailspin_destroy(tailspin_t* const ptr)
{
  int i = 0;

  if (NULL != ptr)
  {
    for (i = 0; i < ptr->num_speakers; i++)
    {
      _tailspin_sos_destroy(ptr->hrtf_l_sos[i]);
      _tailspin_sos_destroy(ptr->hrtf_r_sos[i]);

      _tailspin_delay_cleanup(&ptr->hrtf_l_delay[i]);
      _tailspin_delay_cleanup(&ptr->hrtf_r_delay[i]);
    }

    free(ptr->hrtf_l_sos);
    free(ptr->hrtf_r_sos);

    free(ptr->hrtf_l_delay);
    free(ptr->hrtf_r_delay);

    free(ptr->loudspeaker_in_samples);
    free(ptr->decoder_matrix);

    free(ptr->azimuth_taps);
    free(ptr->elevation_taps);

    free(ptr);
  }
}

//=============================================================================

void tailspin_set_parameters(tailspin_t* const ptr,
  real azimuth_degrees, real elevation_degrees, real direction_mix)
{
  ptr->azimuth_angle = azimuth_degrees;
  ptr->elevation_angle = elevation_degrees;
  ptr->direction_mix = direction_mix;
}

//=============================================================================

void tailspin_tick(tailspin_t* const ptr, real diffuser_direct,
  const real* const diffuser_taps, real* const binaural_out)
{
  const real hp_w_weight = sqrt(2.0);
  const real hp_xyz_weight = sqrt(2.0 / 3.0);

  int i = 0;
  real foa_taps[4] = { 0, 0, 0, 0 };
  real foa_direct[4] = { 0, 0, 0, 0 };
  real foa[4] = { 0, 0, 0, 0 };
  real foa_lp[4] = { 0, 0, 0, 0 };
  real foa_hp[4] = { 0, 0, 0, 0 };
  real azi, ele, cos_azi, sin_azi, cos_ele, sin_ele, s_cos_ele;
  real inv_direction_mix;
  real left, right;

  // FOA-encode taps
  for (i = 0; i < ptr->tap_count - 1; i++)
  {
    azi = ptr->azimuth_taps[i];
    ele = ptr->elevation_taps[i];

    cos_azi = _tailspin_trig_table_lookup_cos(ptr->trig_table, azi);
    sin_azi = _tailspin_trig_table_lookup_sin(ptr->trig_table, azi);

    cos_ele = _tailspin_trig_table_lookup_cos(ptr->trig_table, ele);
    sin_ele = _tailspin_trig_table_lookup_sin(ptr->trig_table, ele);

    s_cos_ele = diffuser_taps[i] * cos_ele;

    foa_taps[0] += diffuser_taps[i] * ptr->recip_sqrt2;
    foa_taps[1] += s_cos_ele * cos_azi;
    foa_taps[2] += s_cos_ele * sin_azi;
    foa_taps[3] += diffuser_taps[i] * sin_ele;
  }

  // FOA-encode direct
  azi = ptr->azimuth_angle;
  ele = ptr->elevation_angle;

  _tailspin_trig_table_lookup_cos(ptr->trig_table, azi);

  cos_azi = _tailspin_trig_table_lookup_cos(ptr->trig_table, azi);
  sin_azi = _tailspin_trig_table_lookup_sin(ptr->trig_table, azi);

  cos_ele = _tailspin_trig_table_lookup_cos(ptr->trig_table, ele);
  sin_ele = _tailspin_trig_table_lookup_sin(ptr->trig_table, ele);

  s_cos_ele = diffuser_direct * cos_ele;

  foa_direct[0] = ptr->direction_mix * diffuser_direct * ptr->recip_sqrt2;
  foa_direct[1] = s_cos_ele * cos_azi;
  foa_direct[2] = s_cos_ele * sin_azi;
  foa_direct[3] = diffuser_direct * sin_ele;

  // mix taps with direct
  inv_direction_mix = (1 - ptr->direction_mix);
  for (i = 0; i < 4; i++)
  {
    foa[i] = ptr->direction_mix * foa_direct[i] + inv_direction_mix * foa_taps[i];
  }

  // near-field compensation filter (don't need)
  //_tailspin_nfc_tick(&ptr->nfc, foa, foa);

  // phase-matched crossover filters to balance high-freq localization
  _tailspin_crossovers_tick(&ptr->xovers, foa, foa_lp, foa_hp);

  // weight foa_lp and foa_hp different
  foa[0] = foa_lp[0] - hp_w_weight * foa_hp[0];
  foa[1] = foa_lp[1] - hp_xyz_weight * foa_hp[1];
  foa[2] = foa_lp[2] - hp_xyz_weight * foa_hp[2];
  foa[3] = foa_lp[3] - hp_xyz_weight * foa_hp[3];

  // decode b-format to loudspeakers
  _tailspin_mat_by_vec(ptr->loudspeaker_in_samples,
    ptr->decoder_matrix, foa, ptr->num_speakers, 4);

  // run binaural delays & filters and compute final stereo out
  if (NULL != binaural_out)
  {
    // zero-set output samples
    memset(binaural_out, 0, sizeof(real) * 2);

    for (i = 0; i < ptr->num_speakers; i++)
    {
      left = _tailspin_delay_tick(
        &ptr->hrtf_l_delay[i], ptr->loudspeaker_in_samples[i]);

      right = _tailspin_delay_tick(
        &ptr->hrtf_r_delay[i], ptr->loudspeaker_in_samples[i]);

      binaural_out[0] += _tailspin_sos_tick(ptr->hrtf_l_sos[i], left);
      binaural_out[1] += _tailspin_sos_tick(ptr->hrtf_r_sos[i], right);
    }
  }
}

//=============================================================================

const real* tailspin_get_loudspeaker_outputs(
  const tailspin_t* const ptr)
{
  return ptr->loudspeaker_in_samples;
}

//=============================================================================
