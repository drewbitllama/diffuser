#ifndef TAILSPIN_H__
#define TAILSPIN_H__

#include "diffuser.h"

typedef struct _tailspin_t tailspin_t;

//=============================================================================
// settings for decoder matrices and HRTF sets
// use associated MATLAB file to generate new sets

typedef enum _tailspin_quality_t
{
  TAILSPIN_CIPIC3_O20_QUAD_44K,
  TAILSPIN_CIPIC3_O20_CUBE_44K,
}
tailspin_quality_t;

//=============================================================================
// create a new tailspin ptr based on a given "quality" setting

// num_of_taps: the number of internal taps from an associated diffuser
// fs: samplerate (Hz)

tailspin_t* tailspin_create_with_quality(
  int num_of_taps, real fs, tailspin_quality_t quality);

//=============================================================================
// create a new tailspin ptr with explicit control over initialization

// num_of_taps: the number of internal taps from an associated diffuser
// fs: samplerate (Hz)
// decoder_matrix: row-major decoder matrix values
// num_speakers: the number of loudspeakers to project to
// hrtf_l_coefs: [a1 a2 b1 b2] x num_of_biquads coeffs for left HRTF sos
// hrtf_l_delay_samples: delay in samples for each left HRTF sos
// hrtf_l_gain: gain for each left HRTF sos
// hrtf_r_coefs: [a1 a2 b1 b2] x num_of_biquads coeffs for right HRTF sos
// hrtf_r_delay_samples: delay in samples for each right HRTF sos
// hrtf_r_gain: gain for each right HRTF sos
// biquad_count: num of biquads in a single sos

tailspin_t* tailspin_create_with_values(
  int num_of_taps,
  real fs, 
  const real* const decoder_matrix, 
  int num_speakers,
  const real* const hrtf_l_coefs, 
  const real* const hrtf_l_delay_samples, 
  const real* const hrtf_l_gain,
  const real* const hrtf_r_coefs, 
  const real* const hrtf_r_delay_samples, 
  const real* const hrtf_r_gain,
  int biquad_count);

//=============================================================================
// cleanup tailspin instance

void tailspin_destroy(tailspin_t* const ptr);

//=============================================================================
// set parameters

// azimuth_degrees: horizontal angle of directional sound
//   (0 deg is forward, -90 deg is left, 90 deg is right, 180 is backward)
// elevation_degrees: vertical angle of directional sound
//   (0 deg is ear-level, -90 deg is directly below, 90 is directly above)
// direction_mix: [0 - 1] amount of directional vs surround mix
//   (0 is no directional, 1 is only directional)

void tailspin_set_parameters(tailspin_t* const ptr,
  real azimuth_degrees, real elevation_degrees, real direction_mix);

//=============================================================================
// tick function (per-sample filter operation)

// diffuser_direct: output from diffuser_tick
// diffuser_taps: const pointer of diffueser's internal taps
// binaural_out: (optional) stereo array for binaural output
//   (set to NULL to disable binaural processing)

void tailspin_tick(tailspin_t* const ptr, real diffuser_direct,
  const real* const diffuser_taps, real* const binaural_out);

//=============================================================================
// get const pointer to loudspeaker output channels

const real* tailspin_get_loudspeaker_outputs(
  const tailspin_t* const ptr);

//=============================================================================

#endif /* TAILSPIN_H__ */