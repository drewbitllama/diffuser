#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include "diffuser.h"
#include "tailspin.h"
#include "delay_vw.h"

void test_diffuser(void)
{
  int i = 0;

  real t60_secs = 1.5540;
  real rolloff_percent = 0.7691;
  real lowpass_cutoff = 1.0;
  real initial_gain_dB = -6;
  real fs = 44100;

  diffuser_t* d = diffuser_create_with_quality(
    t60_secs, rolloff_percent, lowpass_cutoff, initial_gain_dB, fs,
    DIFFUSER_HIGH_QUALITY);

  //tailspin_t* ts = tailspin_create_with_quality(
  //  diffuser_get_num_of_taps(d), fs,
  //  TAILSPIN_CIPIC3_O20_QUAD_44K);

  //tailspin_set_parameters(ts, 90, 0, 1);

  FILE* fp;
  fopen_s(&fp, "../../scripts/wavesim_2_diffuser/diffuser_output.dat", "wb");
  if (NULL == fp)
  {
    printf("ERROR!\n");
    exit(0);
  }

  //real binaural_out[2];
  int burst_len = (int)(fs);
  for (i = 0; i < burst_len; i++)
  {
    real in = 0;
    if (i == 0)
      in = 1;
    real out = diffuser_tick(d, in);
    //tailspin_tick(ts, out, diffuser_get_taps(d), binaural_out);
    fwrite(&out, sizeof(real), 1, fp);
  }
  fclose(fp);

  //tailspin_destroy(ts);
  diffuser_destroy(d);
}

void test_delay_vw(void)
{
  const int max_delay = 10000000;
  delay_vw_t* del = delay_vw_create(max_delay);

  double delay_time = 10000.0;
  double delay_v = 0.0;
  double delay_a = -0.00001;

  int fs = 44100;

  FILE* fp;
  fopen_s(&fp, "../../scripts/delay_output.dat", "wb");

  int max_t = (int)((double)fs);
  int i = 0;
  for (i = 0; i < max_t; i++)
  {
    double t = (double)i / (double)fs;
    double in = cos(8.0 * atan(1.0) * 440.0 * t);

    delay_vw_set_delay_time(del, delay_time);
    delay_time += delay_v;
    delay_v += delay_a;
    double out = delay_vw_tick(del, in);

    fwrite(&out, sizeof(double), 1, fp);
  }
  printf("%f\n", delay_time);

  fclose(fp);

  delay_vw_destroy(del);
}

int main(void)
{
  test_delay_vw();
  return 0;
}